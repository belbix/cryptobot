from time import sleep

from entity.charge import Charge
from utils.ordercreator import OrderCreator


class AmountTest:
    engine = None

    def __init__(self, engine):
        self.engine = engine

    def start(self):
        self.bitmex()

    def bitmex(self):
        oc = OrderCreator(self.engine)
        balance = 0.94381458
        last_price = 3759.5
        charge = Charge
        charge.close_position = 0
        server = "bitmex"
        amount = oc.get_amount(balance, last_price, charge, server)
        print(amount)

