from time import sleep

from entity.charge import Charge
from utils.ordercreator import OrderCreator


class OrderCreatorTest:
    engine = None

    def __init__(self, engine):
        self.engine = engine

    def start(self):
        self.bitmex()

    def bitfinex(self):
        oc = OrderCreator(self.engine)
        init_short_charge = Charge("bitfinex",  # server
                                   "BTCUSD",  # currency
                                   "2019-01-23 14:04:09",  # date
                                   1,  # type
                                   0,  # price
                                   12864,  # candle_id
                                   -1,  # strategy_id
                                   None,  # init_charge
                                   0)  # close_position

        close_short_charge = Charge("bitfinex",  # server
                                    "BTCUSD",  # currency
                                    "2019-01-23 14:04:09",  # date
                                    0,  # type
                                    0,  # price
                                    12876,  # candle_id
                                    -1,  # strategy_id
                                    62,  # init_charge
                                    1)  # close_position
        oc.create_order(init_short_charge)
        sleep(10)
        oc.create_order(close_short_charge)

    def bitmex(self):
        oc = OrderCreator(self.engine)
        init_short_charge = Charge("bitmextest",  # server
                                   "XBT",  # currency
                                   "2019-01-23 14:04:09",  # date
                                   1,  # type
                                   0,  # price
                                   12864,  # candle_id
                                   -1,  # strategy_id
                                   None,  # init_charge
                                   0)  # close_position

        close_short_charge = Charge("bitmextest",  # server
                                    "XBT",  # currency
                                    "2019-01-23 14:04:09",  # date
                                    0,  # type
                                    0,  # price
                                    12876,  # candle_id
                                    -1,  # strategy_id
                                    62,  # init_charge
                                    1)  # close_position
        oc.create_order(init_short_charge)
        sleep(10)
        oc.create_order(close_short_charge)
