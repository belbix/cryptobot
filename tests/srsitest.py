import logging
from time import sleep

from entity.candle import Candle
from entity.charge import Charge
from utils.indicators import stochastic_rsi
from utils.ordercreator import OrderCreator
import logging
from datetime import datetime

import numpy
import pandas as pd
from matplotlib import pyplot

log = logging.getLogger(__name__)


class SrsiTest:
    engine = None

    def __init__(self, engine):
        self.engine = engine

    def start(self):
        closes = [3577.7,
                  3653,
                  3586.1,
                  3588.1,
                  3587.6,
                  3585,
                  3569,
                  3566.11,
                  3568.2,
                  3571.5,
                  3573.1,
                  3577.7,
                  3579.27,
                  3585.8,
                  3587.34,
                  3584.79,
                  3589.9,
                  3588.7,
                  3587,
                  3590.3,
                  3579.15,
                  3579.9,
                  3574.1,
                  3653,
                  3717,
                  3768.9,
                  3769.5,
                  3769.8,
                  3767.93,
                  3767.2,
                  3763.5,
                  3766.2,
                  3769.7,
                  3766.8,
                  3770.3,
                  3767.1,
                  3769.87,
                  3772.1,
                  3773.3,
                  3775,
                  3777.1,
                  3776.2,
                  3780.44,
                  3782.1,
                  3785.1,
                  3768.1,
                  3766.1,
                  3762.3,
                  3766.2,
                  3773.1,
                  3779,
                  3777.8,
                  3776.1,
                  3780,
                  3777.14,
                  3781.5,
                  3782.6,
                  3778.8,
                  3774.8,
                  3796.2,
                  3793.2,
                  3793.1,
                  3792.1,
                  3789.3,
                  3791.9,
                  3788.8,
                  3791,
                  3796.1,
                  3797.3,
                  3808.7,
                  3808.31,
                  3808.9,
                  3819.9,
                  3825.6,
                  3830.6,
                  3711.9,
                  3709,
                  3707.4,
                  3706.98,
                  3702.7,
                  3703.7,
                  3697,
                  3704.8,
                  3703.8,
                  3712.9,
                  3706.58,
                  3707.2,
                  3706.12,
                  3707.2,
                  3708,
                  3699.7,
                  3690.8,
                  3681.1,
                  3679.2,
                  3680.9,
                  3675.7,
                  3681.5,
                  3676.4,
                  3678.4,
                  3678.8]
        in_candles: pd.DataFrame = pd.DataFrame(closes, columns=['close'])
        start = datetime.utcnow()
        in_candles = stochastic_rsi(in_candles)
        print("stochastic_rsi time: %s" % (datetime.utcnow() - start).microseconds)
        # for c in in_candles['SLOWD'].values:
        #     print(c)
