import logging
from datetime import datetime

import pandas

from bitfinex.client import Client, TradeClient
from entity.book import Book
from entity.candle import Candle
from entity.order import Order
from entity.tick import Tick
from mappers.mapper import Mapper

log = logging.getLogger(__name__)


def get_time(period):
    time = 0
    if "m" in str(period).lower():
        n = int(period.replace("m", ""))
        time = n
    elif "h" in str(period).lower():
        n = int(period.replace("h", ""))
        time = n * 60
    elif "d" in str(period).lower():
        period = period.replace("D", "").replace("d", "")
        n = int(period)
        time = n * 60 * 24
    return time


class BitfinexMapper(Mapper):
    SERVER_NAME = "bitfinex"
    tick = None
    candles = None
    books = None
    ticks = None
    client = None
    trade_client = None
    test = False

    def __init__(self, api_key, api_secret):
        self.client = Client()
        self.trade_client = TradeClient(api_key, api_secret)

    def validate_symbol(self, symbol: str):
        if len(symbol) == 3:
            symbol += "USD"
        return symbol.replace("XBT", "BTC")

    def get_candles(self, period, name, hist=False, limit=100, start="", end=""):
        time = get_time(period)

        data = self.client.candles(period, name, hist, limit, start, end)
        try:
            if data is None \
                    or str(data) == "None" \
                    or len(data) == 0 \
                    or data[0] == "error":
                log.error("Error get candles! | %s | %s| %s| %s| %s| %s| %s"
                          % (data, period, name, hist, limit, start, end))
                return None
        except:
            log.exception(str(data))
            return None
        # print(data)
        if not hist:
            date = datetime.fromtimestamp(int(data[0]) / 1000)
            self.candles = Candle(self.SERVER_NAME,
                                  name,
                                  time,
                                  date,
                                  0,  # amount
                                  data[2],  # close
                                  data[3],  # high
                                  data[4],  # low
                                  data[1],  # open
                                  data[5],  # volume
                                  )
            return self.candles
        else:
            self.candles = pandas.DataFrame(data,
                                            columns=["date", "open", "close", "high", "low", "volume"])
            self.candles["date"] = pandas.to_datetime(self.candles["date"], unit='ms')
            return self.candles.sort_values(by="date")

    def create_order(self, amount, price, type, symbol='btcusd', instant=False):
        if type == 0:
            side = "buy"
        else:
            side = "sell"
        data = self.trade_client.place_order(amount, price, side, "market", symbol)
        log.info(data)
        uuid = data["order_id"]
        order = Order(self.SERVER_NAME, symbol, datetime.utcnow(), type, amount, price, uuid, 0)
        return order

    def delete_order(self, order):
        log.info("delete order %s" % order)
        try:
            result = self.trade_client.delete_order(order.market_id)
        except:
            self.trade_client.delete_all_orders()
            return True
        log.info("delete order %s" % result)
        return True

    def get_balance(self, currency: str):
        log.info(currency)
        data = self.trade_client.balances()

        for balance in data:
            log.info(balance)
            if balance["type"] == "trading" and balance["currency"] == currency.lower():
                return float(balance["available"])
        return 0

    def get_position_amount(self, symbol):
        positions = self.trade_client.active_positions()
        log.info("positions %s" % positions)
        for postion in positions:
            if postion["symbol"] == str(symbol).lower():
                return abs(float(postion["amount"]))
        return 0

    def isolate_margin(self, symbol, leverage):
        return True

    def get_tick(self, symbol):
        symbol = "t" + symbol
        try:
            data = self.client.ticker(symbol)
            if data[0] == "error":
                log.error("Error get tick! %s" % data)
                return None
        except:
            log.exception("Error get tick!")
            return None

        self.tick = Tick()
        self.tick.server = self.SERVER_NAME
        self.tick.symbol = symbol
        self.tick.bid = data[0]
        self.tick.ask = data[2]
        self.tick.last = data[6]
        self.tick.date = datetime.utcnow()
        return self.tick

    def get_history_tick(self, symbol, limit, start, end):
        self.ticks = None
        data = ""
        try:
            data = self.client.trades(symbol, limit, start, end)
            if data is None \
                    or str(data) == "None" \
                    or len(data) == 0 \
                    or data[0] == "error":
                log.error("Error get tick! " + str(data))
                return None
        except:
            try:
                log.exception("Error get tick! %s" % data)
                return None
            except:
                log.exception("UNDEFINED ERROR!!!")
                return None

        self.ticks = pandas.DataFrame(data,
                                      columns=["id", "date", "amount", "price"])
        self.ticks["date"] = pandas.to_datetime(self.ticks["date"], unit='ms')

        return self.ticks.sort_values(by="date")

    def get_books(self, symbol, precision, length):
        self.books = []
        try:
            data = self.client.books(symbol, precision, length)
            if data is None \
                    or str(data) == "None" \
                    or len(data) == 0 \
                    or data[0] == "error":
                log.error("Error get books!")
                return None
        except:
            log.exception("Error get books!")
            return None

        for el in data:
            b = Book()
            b.date = datetime.utcnow()
            b.server = self.SERVER_NAME
            b.symbol = symbol
            b.price = el[0]
            b.count = el[1]
            b.amount = el[2]
            self.books.append(b)

        return self.books

    def get_symbols(self):
        return self.client.symbols()

# bm = BitfinexMapper()
# print(bm.get_candles("1m", "BTCUSD", True, 3))
