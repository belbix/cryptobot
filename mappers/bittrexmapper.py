import logging
from datetime import datetime

from bittrex.bittrex import Bittrex
from entity.order import Order
from entity.tick import Tick
from mappers.mapper import Mapper

log = logging.getLogger(__name__)


class BittrexMapper(Mapper):
    def get_candles(self, period, name, hist=False, limit=100, start="", end=""):
        pass

    tick = None
    order = None
    SERVER_NAME = "bittrex"
    b = None
    test = False

    def __init__(self, api_key, api_secret):
        self.b = Bittrex(api_key, api_secret)

    def validate_symbol(self, symbol: str):
        return symbol.replace("XBT", "BTC")

    def get_tick(self, name):
        name = symbol_normalize(name)
        data = self.b.get_ticker(name)
        if not data["success"]:
            log.error("Error get tick: " + str(data))
            return None
        self.tick = Tick()
        self.tick.server = self.SERVER_NAME
        self.tick.symbol = name
        self.tick.bid = data["result"]["Bid"]
        self.tick.ask = data["result"]["Ask"]
        self.tick.last = data["result"]["Last"]
        self.tick.date = datetime.utcnow()
        return self.tick

    # USDT-BTC
    def create_order(self, amount, price, type, symbol):
        if type == 0:
            amount = amount / price
        amount = round(amount, 8)

        symbol = symbol_normalize(symbol)

        data = None
        if self.test:
            self.order = Order(self.SERVER_NAME, symbol, datetime.utcnow(), type, amount, price, 0, 0)
            return self.order

        if type == 0:  # buy
            data = self.b.buy_limit(symbol, amount, price)
        elif type == 1:  # sell
            data = self.b.sell_limit(symbol, amount, price)
        if data is None or not data["success"]:
            log.error("Error get create_order: " + str(data))
            return None
        uuid = data["result"]["uuid"]

        self.order = Order(self.SERVER_NAME, symbol, datetime.utcnow(), type, amount, price, uuid, 0)

        return self.order

    def get_balance(self, currency: str):
        # if self.test:
        #     return 5000
        if currency == "USD":
            currency = "USDT"
        data = self.b.get_balance(currency)
        if data is None or not data["success"]:
            log.error("Error get tick: " + str(data))
            return None
        return data["result"]["Available"]

    def check_open_order(self, order):
        data = self.b.get_open_orders(order.symbol)

        if not data["success"]:
            log.error("Error get order: " + str(data))
            return False
        open_orders = data["result"]
        for o in open_orders:
            log.info(o)
            if o["OrderUuid"] == order.market_id:
                return True
        return False

    def cancel_order(self, order):
        data = self.b.cancel(order.market_id)
        if not data["success"]:
            log.error("Error cancel order: " + str(data))
            return False
        return True


def symbol_normalize(symbol):
    if "-" not in symbol:
        symbol = symbol[3] + symbol[4] + symbol[5] + "-" + symbol[0] + symbol[1] + symbol[2]
        symbol = symbol.replace("USD", "USDT")
    return symbol
