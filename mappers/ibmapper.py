import logging
from collections import OrderedDict
from datetime import datetime
from threading import Thread
from time import sleep

import pandas

from entity.order import Order
from entity.tick import Tick
from ibapi.client import EClient
from ibapi.common import OrderId, TickerId
from ibapi.contract import Contract
from ibapi.iborder import IBOrder
from ibapi.order_state import OrderState
from ibapi.ticktype import TickType
from ibapi.wrapper import EWrapper
from mappers.mapper import Mapper
from strategies.strat import Strat

log = logging.getLogger(__name__)


def singleton(class_):
    instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return get_instance


@singleton
class IBMapper(Mapper, EClient, EWrapper):
    SERVER_NAME = "ib"
    currency = "USD"
    restart_errors = [1100]
    last_message_id = 1000

    def __init__(self, api_key, api_secret, start=True):
        EClient.__init__(self, self)
        self.nextValidOrderId = 1000
        self.permId2ord = {}
        self.mapper_ip = "127.0.0.1"
        self.mapper_port = 4002  # 7497
        self.clientId = 0
        if start:
            self.start()

    def start(self):
        thread = Thread(target=self.run)
        while True:
            try:
                self.clientId = int(datetime.utcnow().timestamp()) - 1525000000
                log.info("Start IB connection %s" % self.clientId)
                self.connect(self.mapper_ip, self.mapper_port, self.clientId)

                timeout = 0
                while timeout < 30:  # wait 30 second
                    if self.isConnected():
                        break
                    else:
                        sleep(1)
                    timeout += 1
                if not self.isConnected():
                    raise Exception("error connect")

                thread.start()
                self.reqAccountSummary(reqId=self.next_id(), groupName="All", tags="NetLiquidation")
                break
            except Exception as e:
                log.error("Cant connect to ibg: %s" % e)
                self.done = True
                sleep(10)
                self.done = False

    def get_history_tick(self, symbol, batch, start: str, end: str):
        contract = Contract()
        contract.symbol = self.currency
        # contract.secType = "FUT"
        contract.secType = "CASH"
        contract.currency = symbol
        contract.exchange = "IDEALPRO"
        # contract.primaryExchange = "ISLAND"
        # contract.lastTradeDateOrContractMonth = "201606"

        number_of_ticks = batch
        what_to_show = "TRADES"
        useRth = 0
        ignoreSize = True
        miscOptions = None

        if miscOptions is None:
            miscOptions = []
        log.debug("start: %s" % start)
        start_time_str = None
        if start and start != "":
            start_time_str = datetime.fromtimestamp(int(start[:-3])).strftime("%Y%m%d %H:%M:%S")
            if ":59:" in start_time_str:
                start_time_str = datetime.fromtimestamp(int(start[:-3]) + 60).strftime("%Y%m%d %H:%M:%S")
        if end and end != "" and (start_time_str is None or start_time_str == ""):
            end_time_str = datetime.fromtimestamp(int(end[:-3])).strftime("%Y%m%d %H:%M:%S")
        else:
            end_time_str = ""
        id_num = self.next_id()
        if not miscOptions:
            miscOptions = []
        self.reqHistoricalTicks(id_num, contract, start_time_str, end_time_str,
                                number_of_ticks, what_to_show, useRth, ignoreSize, miscOptions)
        self.history_messages[id_num] = None
        timeout = 0
        while timeout < 30:  # wait 30 second
            err_code = self.check_errors(self.errors)
            if err_code == "restart":
                continue

            if self.history_messages[id_num] is not None:
                h = self.history_messages.pop(id_num)
                if not h["done"] or len(h["ticks"]) == 0:
                    log.info("Wrong or empty answer %s! %s" % (id_num, h))
                    return None

                ticks = pandas.DataFrame.from_records(t.to_dict() for t in h["ticks"])
                ticks["date"] = pandas.to_datetime(ticks["date"], unit='s')
                return ticks
            else:
                sleep(1)
            timeout += 1

        log.error("History tick timeout expired! %s" % id_num)
        return None

    def create_order(self, amount, price, type, symbol):
        old_price = price
        contract = Contract()
        contract.symbol = symbol
        contract.secType = "CASH"
        contract.currency = self.currency
        contract.exchange = "IDEALPRO"

        order = IBOrder()
        if Strat.TYPE_BUY:
            order.action = "BUY"
        else:
            order.action = "SELL"
        order.orderType = "MKT"
        order.totalQuantity = amount
        order_id = self.nextOrderId()

        o = Order(self.SERVER_NAME, symbol, datetime.utcnow(), type, amount, old_price, order_id, 0)
        self.placeOrder(order_id, contract, order)

        timeout = 0
        while timeout < 30:  # wait 30 second
            err_code = self.check_errors(self.errors)
            if err_code == "restart":
                continue
            if self.orders[order_id] is not None:
                result_order = self.orders.pop(order_id)
                log.info("result_order: %s" % result_order)
                return o
            else:
                sleep(1)
            timeout += 1

        log.error("Create order timeout expired! %s" % o)
        return None

    def get_candles(self, period, name, hist=False, limit=100, start="", end=""):
        return None

    def get_balance(self, currency: str):
        for summary in self.account_summary_messages.values():
            if summary.currency == self.currency:
                return summary.value

        return 0

    def get_tick(self, symbol):
        symbol = "t" + symbol
        try:
            data = self.client.ticker(symbol)
            if data[0] == "error":
                log.error("Error get tick! %s" % data)
                return None
        except:
            log.exception("Error get tick!")
            return None

        self.tick = Tick()
        self.tick.server = self.SERVER_NAME
        self.tick.symbol = symbol
        self.tick.bid = 0
        self.tick.ask = 0
        self.tick.last = 0
        self.tick.date = datetime.utcnow()
        return self.tick

    def validate_symbol(self, symbol: str):
        return symbol

    def check_errors(self, errors: OrderedDict):
        for errCode in errors.keys():
            if int(errCode) in self.restart_errors:
                log.warning("Restart thread!")
                self.done = True
                sleep(3)
                self.disconnect()
                self.start()
                errors.clear()
                return "restart"
        return ""

    def next_id(self):
        self.last_message_id += 1
        return self.last_message_id

    def nextValidId(self, orderId: int):
        super().nextValidId(orderId)
        logging.debug("setting nextValidOrderId: %d", orderId)
        self.nextValidOrderId = orderId

    def nextOrderId(self):
        id = self.nextValidOrderId
        self.nextValidOrderId += 1
        return id

    def error(self, *args):
        super().error(*args)
        # print(current_fn_name(), vars())

    def winError(self, *args):
        super().error(*args)
        # print(current_fn_name(), vars())

    def openOrder(self, orderId: OrderId, contract: Contract, order: IBOrder,
                  orderState: OrderState):
        super().openOrder(orderId, contract, order, orderState)
        # print(current_fn_name(), vars())

        order.contract = contract
        self.permId2ord[order.permId] = order

    def openOrderEnd(self, *args):
        super().openOrderEnd()
        logging.debug("Received %d openOrders", len(self.permId2ord))

    def orderStatus(self, orderId: OrderId, status: str, filled: float,
                    remaining: float, avgFillPrice: float, permId: int,
                    parentId: int, lastFillPrice: float, clientId: int,
                    whyHeld: str):
        super().orderStatus(orderId, status, filled, remaining,
                            avgFillPrice, permId, parentId, lastFillPrice, clientId, whyHeld)

    def tickPrice(self, tickerId: TickerId, tickType: TickType, price: float, attrib):
        super().tickPrice(tickerId, tickType, price, attrib)
        # print(current_fn_name(), tickerId, TickTypeEnum.to_str(tickType), price, attrib, file=sys.stderr)

    def tickSize(self, tickerId: TickerId, tickType: TickType, size: int):
        super().tickSize(tickerId, tickType, size)
        # print(current_fn_name(), tickerId, TickTypeEnum.to_str(tickType), size, file=sys.stderr)

    def scannerParameters(self, xml: str):
        open('scanner.xml', 'w').write(xml)
