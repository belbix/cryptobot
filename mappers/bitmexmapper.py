import logging
from datetime import datetime, timedelta

import pandas
from dateutil import parser

from bitmex.market_maker.bitmex import BitMEX
from entity.order import Order
from entity.tick import Tick
from mappers.bitfinexmapper import get_time
from mappers.mapper import Mapper

log = logging.getLogger(__name__)


def calc_amount(side, size):
    if side == "Sell":
        return -int(size)
    return int(size)


class BitmexMapper(Mapper):
    SERVER_NAME = "bitmex"
    order_type = "Limit"
    order = None
    tick = None
    candles = None
    books = None
    ticks = None
    client = None
    test = False
    skipws = False

    def __init__(self, api_key, api_secret, base_url="https://testnet.bitmex.com/api/v1/", shouldWSAuth=True,
                 skipws=False, proxy=None, postOnly=False):
        self.skipws = skipws
        self.client = BitMEX(base_url=base_url, symbol="XBTUSD",
                             apiKey=api_key,
                             apiSecret=api_secret,
                             orderIDPrefix='bbx_', shouldWSAuth=shouldWSAuth, postOnly=postOnly, skipws=skipws,
                             proxy=proxy)

    def validate_symbol(self, symbol: str):
        if len(symbol) == 3:
            symbol = symbol + "USD"
        symbol = symbol.replace("BTC", "XBT")
        return symbol

    def get_candles(self, period, symbol, hist=False, limit=500, start="", end=""):
        if start == "":
            if end == "":
                tt = datetime.utcnow()
            else:
                tt = datetime.strptime(end, "%Y-%m-%d %H:%M:%S")
            t = get_time(period)
            start = tt - timedelta(minutes=t * limit)
            start = start.isoformat()

        data = self.client.candles(period, symbol, hist, limit, start, end)
        try:
            if data is None \
                    or str(data) == "None" \
                    or len(data) == 0:
                log.error("Error get candles! | %s | %s| %s| %s| %s| %s| %s"
                          % (data, period, symbol, hist, limit, start, end))
                return None
        except:
            log.exception(str(data))
            return None

        self.candles = pandas.DataFrame(data, columns=["timestamp", "open", "close", "high", "low", "volume"])
        self.candles["date"] = pandas.to_datetime(self.candles["timestamp"])
        del self.candles["timestamp"]
        return self.candles.sort_values(by="date")

    def create_order(self, amount, price, type, symbol='XBTUSD', instant=False):
        amount = abs(amount)
        old_price = price
        self.client.symbol = symbol
        data = None
        if self.test:
            self.order = Order(self.SERVER_NAME, symbol, datetime.utcnow(), type, amount, price, 0, 0)
            return self.order
        if instant:
            order_type = "Market"
        else:
            order_type = self.order_type

        if order_type == "Market":
            price = 0

        if type == 0:  # buy
            data = self.client.buy(amount, price, ordType=order_type)
        elif type == 1:  # sell
            data = self.client.sell(amount, price, ordType=order_type)

        log.info("data: %s" % data)
        if data is None or (data["ordStatus"] != "New" and data["ordStatus"] != "Filled"):
            log.error("Error get create_order")
        uuid = data["orderID"]

        self.order = Order(self.SERVER_NAME, symbol, datetime.utcnow(), type, amount, old_price, uuid, 0)

        return self.order

    def get_balance(self, currency: str):
        data = self.client.wallet("XBt")
        return data[0]["walletBalance"] / 100000000

    def get_tick(self, symbol):
        self.tick = Tick()
        try:
            if not self.skipws:
                data = self.client.ticker_data(symbol)
                # log.info("ticker_data %s" % data)
                self.tick.bid = data["buy"]
                self.tick.ask = data["sell"]
                self.tick.price = data["last"]
                self.tick.last = data["last"]
                self.tick.amount = data["volume"]
                self.tick.volume = data["volume"]
                self.tick.date = datetime.strptime(data["timestamp"], "%Y-%m-%dT%H:%M:%S.%fZ")

            else:
                data = self.client.instruments(symbol)[0]
                # log.info("instr %s" % data)
                self.tick.bid = data["bidPrice"]
                self.tick.ask = data["askPrice"]
                self.tick.price = data["lastPrice"]
                self.tick.last = data["lastPrice"]
                self.tick.amount = data["volume"]
                self.tick.date = datetime.strptime(data["timestamp"], "%Y-%m-%dT%H:%M:%S.%fZ")
        except:
            log.exception("Error get tick!")
            return None
        self.tick.server = self.SERVER_NAME
        self.tick.symbol = symbol
        # self.tick.date = datetime.utcnow()

        return self.tick

    def get_history_tick(self, symbol, limit, start, end):
        self.ticks = None
        data = ""
        try:
            data = self.client.ticks(symbol, limit, start, end)
            if data is None \
                    or str(data) == "None" \
                    or len(data) == 0 \
                    or data[0] == "error":
                log.error("Error get tick! " + str(data))
                return None
        except:
            try:
                log.exception("Error get tick! %s" % data)
                return None
            except:
                log.exception("UNDEFINED ERROR!!!")
                return None

        panda_data = pandas.DataFrame(data)
        del panda_data['tickDirection']
        del panda_data['grossValue']
        del panda_data['homeNotional']
        del panda_data['foreignNotional']

        panda_data['date'] = panda_data['timestamp'].apply(lambda x: datetime.strptime(x, '%Y-%m-%dT%H:%M:%S.%fZ'))
        del panda_data['timestamp']
        panda_data['amount'] = panda_data.apply(lambda row: calc_amount(row['side'], row['size']), axis=1)
        del panda_data['side']
        del panda_data['size']
        panda_data['str_id'] = panda_data['trdMatchID']
        del panda_data['trdMatchID']

        self.ticks = panda_data
        return self.ticks.sort_values(by="date")

    def get_books(self, symbol, precision, length):
        return None

    def get_symbols(self):
        return None

    def check_open_order(self, order):
        if self.skipws:
            data = self.client.http_open_orders()
        else:
            data = self.client.open_orders()
        log.info("Opens orders: %s" % data)
        for o in data:
            log.info("Order (%s, %s) = %s | %s  = %s" %
                     (o["orderID"], order.market_id, o["orderID"] == order.market_id, o["ordStatus"],
                      o["ordStatus"] == "New"))
            if o["orderID"] == order.market_id and o["ordStatus"] == "New":
                log.info("Order is not execute! %s" % o)
                return True
        return False

    def cancel_order(self, order):
        data = self.client.cancel(order.market_id)
        log.info("canceling order: %s" % data)
        if data[0]["ordStatus"] != "Canceled":
            log.error("Error cancel order %s" % data[0]["ordStatus"])
            return False
        return True

    def isolate_margin(self, symbol, leverage):
        data = self.client.isolate_margin(symbol, leverage)
        return data

    def get_position_amount(self, symbol):
        if self.skipws:
            if len(self.client.http_position(symbol)) > 0:
                return self.client.http_position(symbol)[0]["currentQty"]
            else:
                return 0
        else:
            return self.client.position(symbol)["currentQty"]
