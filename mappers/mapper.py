from abc import ABCMeta, abstractmethod


class Mapper(metaclass=ABCMeta):
    @abstractmethod
    def get_candles(self, period, name, hist=False, limit=100, start="", end=""):
        """get_candles"""

    @abstractmethod
    def create_order(self, amount, price, type, symbol):
        """create_order"""

    @abstractmethod
    def get_balance(self, currency: str):
        """get_balance"""

    @abstractmethod
    def validate_symbol(self, symbol: str):
        """validate_symbol"""
