import logging.config
import os
import time
from datetime import datetime

import numpy
import pandas
from matplotlib import pyplot, finance
from scipy.signal import argrelextrema

from mappers.bitfinexmapper import BitfinexMapper, get_time
from utils import configloader
from utils.history import History
from utils.indicators import deep_stochastic_rsi, deltaprice

log = logging.getLogger(__name__)


# logging.getLogger("history").setLevel(logging.WARNING)


def get_dataset(price_arr, fee):
    log.info(price_arr)
    order = 1
    # fee = 0.008
    emax = argrelextrema(price_arr, numpy.greater, order=order)[0]
    emin = argrelextrema(price_arr, numpy.less, order=order)[0]
    log.info(len(emin))
    log.info(len(emax))

    price_min = 0
    price_max = 0
    mins = {}
    tmp_mins = {}
    tmp_maxs = {}
    maxs = {}
    close_order = True
    first = True
    for i in range(0, len(price_arr)):
        if i in emin:
            if ((close_order and (int(price_max - price_arr[i]) / price_arr[i]) >= fee)
                or price_min >= price_arr[i]) or first:
                if not first and close_order:
                    # write true data
                    id_min = min(tmp_mins, key=tmp_mins.get)
                    id_max = max(tmp_maxs, key=tmp_maxs.get)
                    mins[id_min] = tmp_mins[id_min]
                    maxs[id_max] = tmp_maxs[id_max]
                    tmp_mins = {}
                    tmp_maxs = {}

                first = False
                price_min = price_arr[i]
                close_order = False
                price_max = 0
                tmp_mins[i] = price_arr[i]

        if i in emax:
            if (not close_order or price_max <= price_arr[i]) and len(tmp_mins) >= 0 \
                    and (
                            ((price_arr[i] - price_min) / price_arr[i] >= fee)
                    ):
                close_order = True
                price_max = price_arr[i]
                tmp_maxs[i] = price_max
    if len(maxs) == 0:
        mins = {}
    return [mins, maxs]


def get_levels_points(candles):
    order = 1
    lows = candles["low"].as_matrix()
    highs = candles["high"].as_matrix()
    emax = argrelextrema(highs, numpy.greater, order=order)[0]
    emin = argrelextrema(lows, numpy.less, order=order)[0]
    log.info(len(emin))
    log.info(len(emax))

    mins = {}
    maxs = {}

    for i in range(0, len(lows)):
        if i in emin:
            mins[i] = lows[i]
        if i in emax:
            maxs[i] = highs[i]
    return [mins, maxs]





def calc_level(coords, x, y):
    fy = 0
    if coords[0][1] != 0 and coords[0][1] != 0:
        a = coords[0][1] - coords[1][1]
        b = coords[0][0] - coords[1][0]
        c = coords[0][0] * coords[1][1] - coords[1][0] * coords[0][1]
        # f = a * x + b * y + c
        fy = (a * x + c) / b
        # starty = (a * 0 + c) / b
        # endy = (a * 100 + c) / b
        # log.info("fy:%s" % fy)
        # pyplot.plot(coords[1][0], coords[1][1], "co")
        # pyplot.plot(x, fy, "bo")
        # pyplot.plot([0, coords[0][0], coords[1][0], 100], [starty, coords[0][1], coords[1][1], endy])
    return fy


def draw_level(coords, len):
    if coords[0][1] != 0 and coords[0][1] != 0:
        a = coords[0][1] - coords[1][1]
        b = coords[0][0] - coords[1][0]
        c = coords[0][0] * coords[1][1] - coords[1][0] * coords[0][1]
        starty = (a * 0 + c) / b
        endy = (a * len + c) / b
        # pyplot.plot([0, coords[0][0], coords[1][0], len], [starty, coords[0][1], coords[1][1], endy])
        pyplot.plot([coords[0][0], coords[1][0], len], [coords[0][1], coords[1][1], endy])


def save_dataset(candles):
    # log.info(candles)
    batch = []
    f = open("data/"+"data.txt", "w+")
    for i in range(2, len(candles["close"]) - 2):
        batch.append(candles.iloc[i]["PRICE"])
        # batch.append(candles.iloc[i]["volume"])
        # batch.append(candles.iloc[i]["DELTAKD"])
        # batch.append([candles.iloc[i]["PRICE"], candles.iloc[i]["volume"], candles.iloc[i]["DELTAKD"]])
        if len(batch) == 30:
            if candles.iloc[i]["BUY"] == 1 \
                    or candles.iloc[i - 1]["BUY"] == 1 or candles.iloc[i + 1]["BUY"] == 1:
                # or candles.iloc[i-2]["BUY"] == 1 or candles.iloc[i+2]["BUY"] == 1:
                buy = "1"
            else:
                buy = "0"

            if candles.iloc[i]["SELL"] == 1 \
                    or candles.iloc[i - 1]["SELL"] == 1 or candles.iloc[i + 1]["SELL"] == 1:
                # or candles.iloc[i-2]["SELL"] == 1 or candles.iloc[i+2]["SELL"] == 1:
                sell = "1"
            else:
                sell = "0"

            f.write(str(batch).replace("]", "").replace("[", ""))
            # f.write(str(batch))
            f.write(",%s" % buy)
            f.write(",%s" % sell)
            f.write(",%s" % candles.iloc[i]["volume"])
            f.write(",%s" % candles.iloc[i]["close"])
            f.write(",%s" % candles.iloc[i]["date"])
            f.write("\n")
            # batch.pop(0)
            batch.pop(0)

    f.close()


def save_dataset2d(candles):
    batch = []
    result = [[], [], [], [], [], [], [], []]  # [set_deltaprice, set_deltakd, set_volume, yb, ys, volume, price, date ]
    # 0.delta price
    for i in range(2, len(candles["close"]) - 2):
        batch.append(candles.iloc[i]["PRICE"])
        if len(batch) == 30:
            result[0].append(batch)
            batch.pop(0)

    batch = []
    # 1.delta kd
    for i in range(2, len(candles["close"]) - 2):
        batch.append(candles.iloc[i]["DELTAKD"])
        if len(batch) == 30:
            result[1].append(batch)
            batch.pop(0)

    batch = []
    # 2.volume
    for i in range(2, len(candles["close"]) - 2):
        batch.append(candles.iloc[i]["volume"])
        if len(batch) == 30:
            result[2].append(batch)
            batch.pop(0)

    # 3.yb
    j = 0
    for i in range(2, len(candles["close"]) - 2):
        j += 1
        if j == 30:
            if candles.iloc[i]["BUY"] == 1 \
                    or candles.iloc[i - 1]["BUY"] == 1 or candles.iloc[i + 1]["BUY"] == 1:
                # or candles.iloc[i-2]["BUY"] == 1 or candles.iloc[i+2]["BUY"] == 1:
                buy = [1]
            else:
                buy = [0]
            result[3].append(buy)
            j -= 1

    # 4.ys
    j = 0
    for i in range(2, len(candles["close"]) - 2):
        j += 1

        if j == 30:
            if candles.iloc[i]["SELL"] == 1 \
                    or candles.iloc[i - 1]["SELL"] == 1 or candles.iloc[i + 1]["SELL"] == 1:
                # or candles.iloc[i-2]["SELL"] == 1 or candles.iloc[i+2]["SELL"] == 1:
                sell = [1]
            else:
                sell = [0]
            result[4].append(sell)
            j -= 1

    # 5.volume
    j = 0
    for i in range(2, len(candles["close"]) - 2):
        j += 1
        if j == 30:
            result[5].append([candles.iloc[i]["volume"]])
            j -= 1

    # 6.close
    j = 0
    for i in range(2, len(candles["close"]) - 2):
        j += 1
        if j == 30:
            result[6].append([candles.iloc[i]["close"]])
            j -= 1

    # 7.date
    j = 0
    for i in range(2, len(candles["close"]) - 2):
        j += 1
        if j == 30:
            t = candles.iloc[i]["date"]
            d = datetime.strptime(str(t), "%Y-%m-%d %H:%M:%S")
            result[7].append([d.microsecond])
            j -= 1

    f = open("data/" + "data2d.txt", "w+")
    f.write(str(result))
    f.close()


class DataGetter:
    mapper = BitfinexMapper("", "")

    symbol = None
    period = None
    loop_delay = None
    test = None
    last_tick = None
    candle_batch = None

    def __init__(self, engine):
        self.engine = engine
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)
        self.symbol = config["symbol"]
        self.period = config["period"]
        self.count_minuts = config["count_minuts"]
        self.loop_delay = config["loop_delay"]
        self.test = config["test"]
        self.candle_batch = int(config["candle_batch"])
        self.last_tick = datetime.utcnow()

    def greater(self, x, y):
        f = lambda x, y: x > y and 0.004 < ((x - y) / x)
        return list(map(f, x, y))

    def less(self, x, y):
        f = lambda x, y: x < y and 0.001 < ((y - x) / y)
        return list(map(f, x, y))

    def get_candles(self):
        candles_ = None
        while candles_ is None:
            candles_ = self.mapper.get_candles(self.period, self.symbol, True, self.candle_batch)
            time.sleep(self.loop_delay)
        return candles_

    def get_candles_hist(self):
        h = History(self.engine)
        h.period = self.period
        h.symbol = self.symbol
        # h.count_minuts = 90 * 24 * 60
        h.count_minuts = self.count_minuts
        h.count_loop = 0
        h.candle_time = int(get_time(h.period))
        return h.get_candles()

    def draw_graf(self, candles, mins, maxs):
        draw_extreme = False
        fig, ax = pyplot.subplots()
        finance.candlestick2_ochl(ax, candles["open"], candles["close"], candles["high"], candles["low"],
                                       width=0.3, colorup="#6ba583", colordown="#d75442", alpha=1)

        pyplot.title(self.symbol)
        log.info("mins: %s" % mins)
        log.info("maxs: %s" % maxs)

        low = [[0, 0], [0, 0]]  # [A[x1,y1]B[x2,y2]]
        up = [[0, 0], [0, 0]]
        iter = 0
        new_val = False
        for i in mins:
            iter += 1
            if draw_extreme:
                pyplot.plot(i, mins[i], "g^")
            # log.info("mins[i]: %s < low: %s" % (mins[i], low))
            if mins[i] < low[0][1] or low[0][1] == 0:
                # if iter == len(mins) - 1:
                new_val = True
                low[0][0] = i
                low[0][1] = mins[i]
                low[1][0] = i
                low[1][1] = mins[i]
                continue

            if new_val or calc_level(low, i, mins[i]) > mins[i]:
                # if iter == len(mins):
                new_val = False
                low[1][0] = i
                low[1][1] = mins[i]

        iter = 0
        new_val = False
        for i in maxs:
            iter += 1
            if draw_extreme:
                pyplot.plot(i, maxs[i], "rv")
            if maxs[i] > up[0][1] or up[0][1] == 0:
                # if iter == len(maxs) - 1:
                new_val = True
                up[0][0] = i
                up[0][1] = maxs[i]
                up[1][0] = i
                up[1][1] = maxs[i]
                continue

            if new_val or calc_level(up, i, maxs[i]) < maxs[i]:
                # if iter == len(maxs):
                new_val = False
                up[1][0] = i
                up[1][1] = maxs[i]

        draw_level(low, len(candles["close"]))
        draw_level(up, len(candles["close"]))

        pyplot.show()

    def processing(self):
        candles = self.get_candles_hist()  # type: pandas.DataFrame
        length = len(candles["close"])
        log.info("length: %s" % length)
        candles = candles.assign(BUY=pandas.Series(numpy.zeros(length)).values)
        candles = candles.assign(SELL=pandas.Series(numpy.zeros(length)).values)
        candles = deep_stochastic_rsi(candles).tail(length - 17)
        length -= 17
        # log.info(candles)

        price_arr = candles["close"].as_matrix()
        datasets = get_dataset(price_arr, 0.008)
        mins = datasets[0]
        maxs = datasets[1]

        for i in range(0, length):
            if i in mins:
                candles.set_value(candles.iloc[i].name, "BUY", 1)
            if i in maxs:
                candles.set_value(candles.iloc[i].name, "SELL", 1)

        candles = deltaprice(candles)

        save_dataset2d(candles)
        draw_graf(price_arr, mins, maxs)

    def processing_levels(self):
        candles = self.get_candles_hist()  # type: pandas.DataFrame
        length = len(candles["close"])
        log.info("length: %s" % length)

        datasets = get_levels_points(candles)
        mins = datasets[0]
        maxs = datasets[1]

        for i in range(0, length):
            if i in mins:
                candles.set_value(candles.iloc[i].name, "MIN", 1)
            if i in maxs:
                candles.set_value(candles.iloc[i].name, "MAX", 1)

        # save_dataset2d(candles)
        self.draw_graf(candles, mins, maxs)

    def start(self):
        self.processing_levels()

# logging.config.dictConfig(configloader.get_config_log())
# engine = engine_from_config(configloader.get_config("db"), prefix='db.')
# obj = DataGetter(engine)
# obj.processing()
