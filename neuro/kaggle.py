from nolearn.dbn import DBN

net = DBN(
    [784, 300, 10],
    learn_rates=0.3,
    learn_rate_decays=0.9,
    epochs=10,
    verbose=1,
)