import csv
import logging
from datetime import datetime

import keras
import numpy

# fix random seed for reproducibility
from keras.layers import Dense, Dropout, Conv1D, MaxPooling1D, GlobalAveragePooling1D, BatchNormalization, Activation
from keras.models import Sequential
from matplotlib import pyplot as plt

numpy.random.seed(7)
log = logging.getLogger(__name__)

def date2float(date):
    return float(datetime.strptime(str(date), "b'%Y-%m-%d %H:%M:%S'").microsecond)


def arr(data):
    return str(data).replace("b'", "").replace("'", "")
    # return json.loads(data)


class Neuro:
    model = None
    X = None
    Y = None
    X_test = None
    Y_test = None

    # configurable
    operation = ""
    type = "2d"
    init = True
    datasize = 0
    ypos = 0
    volumepos = 0
    pricepos = 0
    datepos = 0
    prices_test = 0
    volumes_test = 0
    x2start = 0
    model_size = 0
    engine = None
    graphs = True
    datatest_name = "2price3real_data.txt"
    real_test = False
    graph_price = False
    ap = 0.7

    def __init__(self, engine):
        self.engine = engine
        self.datasize = 30
        self.operation = "buy"
        if self.operation == "buy":
            self.ypos = self.datasize
        elif self.operation == "sell":
            self.ypos = self.datasize + 1

        self.volumepos = self.datasize + 2
        self.pricepos = self.datasize + 3
        self.datepos = self.datasize + 4
        self.model_size = self.datasize - self.x2start

    def load_dataset(self):
        perc_dataset = 0.3
        dataset = numpy.loadtxt("data/" + self.type + ".txt", delimiter=",",
                                converters={self.datepos: date2float})
        lds = len(dataset)
        dataset = dataset[int(lds - (lds * perc_dataset)):lds]
        dataset_l = len(dataset)
        test_count = 0.1
        xend = dataset_l - int(dataset_l * test_count)
        self.X = dataset[0:xend, self.x2start:self.datasize]
        self.Y = dataset[0:xend, self.ypos:self.ypos + 1]

        self.X = self.X.astype('float32')
        self.X /= 255
        self.Y = self.Y.astype('float32')

        self.X_test = dataset[xend:dataset_l, self.x2start:self.datasize]
        self.volumes_test = dataset[xend:dataset_l, self.volumepos:self.volumepos + 1]
        self.prices_test = dataset[xend:dataset_l, self.pricepos:self.pricepos + 1]
        self.Y_test = dataset[xend:dataset_l, self.ypos:self.ypos + 1]

        self.X_test = self.X_test.astype('float32')
        self.X_test /= 255
        self.Y_test = self.Y_test.astype('float32')
        if self.graphs:
            batch_size = 500
            prices = dataset[0:batch_size, self.pricepos:self.pricepos + 1]
            ansewers = dataset[0:batch_size, self.ypos:self.ypos + 1]
            plt.plot(prices)
            for i in range(0, len(prices)):
                if ansewers[i] == 1:
                    plt.plot(i, prices[i], "ro")
            plt.show()

    def load_dataset_mnist(self):
        dataset = numpy.loadtxt("data/mnist_train.csv", delimiter=",", skiprows=1)
        dataset_l = len(dataset)


    def load_dataset2d(self):
        f = open("data/" + self.type + ".txt", "r").read()
        dataset = numpy.array(eval(f)[0], dtype=numpy.float32)

        # [set_deltaprice, set_deltakd, set_volume, yb, ys, volume, price, date]

        perc_dataset = 1
        lds = len(dataset)
        dataset = dataset[int(lds - (lds * perc_dataset)):lds]
        dataset_l = len(dataset)
        test_count = 0.1
        xend = dataset_l - int(dataset_l * test_count)
        self.X = dataset[0:3]
        self.X /= 255
        # print(self.X)
        self.Y = dataset[4:5, 0:1]
        # for el in self.X:
        #     print(el)
            # el /= 255


        # self.X_test = dataset[xend:dataset_l, self.x2start:self.datasize]
        # self.volumes_test = dataset[xend:dataset_l, self.volumepos:self.volumepos + 1]
        # self.prices_test = dataset[xend:dataset_l, self.pricepos:self.pricepos + 1]
        # self.Y_test = dataset[xend:dataset_l, self.ypos:self.ypos + 1]
        #
        # self.X_test = self.X_test.astype('float32')
        # self.X_test /= 255
        # self.Y_test = self.Y_test.astype('float32')
        # if self.graphs:
        #     batch_size = 500
        #     prices = dataset[0:batch_size, self.pricepos:self.pricepos + 1]
        #     ansewers = dataset[0:batch_size, self.ypos:self.ypos + 1]
        #     plt.plot(prices)
        #     for i in range(0, len(prices)):
        #         if ansewers[i] == 1:
        #             plt.plot(i, prices[i], "ro")
        #     plt.show()

    # Feedforward neural network
    def create_model_feedforward(self):
        self.model = Sequential()
        self.model.add(Dense(32, input_dim=self.model_size))  # , activity_regularizer=regularizers.l2(0.01)))
        self.model.add(BatchNormalization())
        self.model.add(Activation("relu"))
        # self.model.add(Dropout(0.5))
        # self.model.add(Dense(64))  # , activity_regularizer=regularizers.l2(0.01)))
        # self.model.add(BatchNormalization())
        # self.model.add(Activation("relu"))
        # self.model.add(Dense(32))  # , activity_regularizer=regularizers.l2(0.01)))
        # self.model.add(BatchNormalization())
        # self.model.add(Activation("relu"))
        # self.model.add(Dense(16))  # , activity_regularizer=regularizers.l2(0.01)))
        # self.model.add(BatchNormalization())
        # self.model.add(Activation("relu"))
        self.model.add(Dense(1))
        self.model.add(Activation('sigmoid'))

    # convolutional neural network
    def create_model_feedforward(self):
        self.model = Sequential()
        kernel_size = 30
        self.model.add(Conv1D(64, kernel_size, activation='relu', input_shape=(30, 1)))
        self.model.add(Conv1D(64, kernel_size, activation='relu'))
        self.model.add(MaxPooling1D(kernel_size))
        self.model.add(Conv1D(128, kernel_size, activation='relu'))
        self.model.add(Conv1D(128, kernel_size, activation='relu'))
        self.model.add(GlobalAveragePooling1D())
        self.model.add(Dropout(0.5))
        self.model.add(Dense(1, activation='sigmoid'))

    def fit_model(self):
        # Fit the model
        # reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=5, min_lr=0.000001, verbose=1)
        if self.init:
            history = self.model.fit(self.X, self.Y, epochs=40, batch_size=128,
                                     shuffle=True)  # type: keras.callbacks.History
            self.model.save_weights("data/" + self.operation + "_" + self.type + "_weights.h5", True)
            self.model.save("data/" + self.operation + "_" + self.type + "_model.h5")
            f = open("data/" + self.operation + "_" + self.type + "_model.json", "w+")
            f.write(self.model.to_json())
            f.close()
            if self.graphs:
                plt.plot(history.history["loss"][1:], "r")
                # plt.plot(history.history["acc"], "g")
                plt.show()
            # print("history: %s" % history.history)
        else:
            self.model.load_weights("data/" + self.operation + "_" + self.type + "_weights.h5")

    def get_predicts(self):
        if self.real_test:
            dataset = numpy.loadtxt("data/" + self.datatest_name, delimiter=",")
            X = dataset[:, 0:self.datasize]
            X /= 255
            Y = numpy.zeros((len(dataset), 1))
        else:
            X = self.X_test
            Y = self.Y_test

        scores = self.model.evaluate(X, Y)
        print("\n%s: %.2f%%" % (self.model.metrics_names[1], scores[1] * 100))

        ansewers = self.model.predict(X)
        success = 0
        buys = 0
        nbuys = 0

        if self.graph_price:
            if self.graphs:
                prices = self.prices_test[0:1000]
                plt.plot(prices)
                l = len(prices)
            else:
                l = len(X)
            for i in range(0, l):
                if Y[i] == 1:
                    if self.graphs:
                        plt.plot(i, prices[i], "c^")
                    nbuys += 1
                if ansewers[i] > self.ap:
                    if self.graphs:
                        plt.plot(i, prices[i], "ro")
                    buys += 1
                if ansewers[i] > self.ap and Y[i] == 1:
                    if self.graphs:
                        plt.plot(i, prices[i], "go")
                    success += 1
        else:
            if self.graphs:
                plt.plot(ansewers)
            for i in range(0, len(X)):
                if Y[i] == 1:
                    if self.graphs:
                        plt.plot(i, ansewers[i], "c^")
                    nbuys += 1
                if ansewers[i] > self.ap:
                    if self.graphs:
                        plt.plot(i, ansewers[i], "ro")
                    buys += 1
                if ansewers[i] > self.ap and Y[i] == 1:
                    if self.graphs:
                        plt.plot(i, ansewers[i], "go")
                    success += 1

        if buys > 0:
            print("length %s | success %s | buys %s | nbuys %s | result %s%%"
                  % (len(Y), success, buys, nbuys, int((success / buys) * 100)))
        else:
            print("length %s | success %s | buys %s | nbuys %s | result %s%%"
                  % (len(Y), success, buys, nbuys, 0))
        if self.graphs:
            plt.show()

    def model_compile(self):
        self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    def processing(self):
        self.load_dataset_mnist()
        # self.load_dataset2d()
        # self.create_model_feedforward()
        # self.model_compile()
        # self.fit_model()

        # dataset = numpy.loadtxt(self.type + "_test.txt", delimiter=",", converters={34: date2float})
        # step = len(dataset)
        # for start in range(0, len(dataset), step):
        #     self.evaluate(dataset[start:start + step])
        # self.get_predicts()

    def start(self):
        self.processing()

    def model_predict(self, data, verbose=0):
        return self.model.predict(data, verbose=verbose)

# n = Neuro()
# n.processing()
