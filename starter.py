import argparse
import importlib
import logging.config

from sqlalchemy import engine_from_config

from utils import configloader

parser = argparse.ArgumentParser(description='Start some object.')
parser.add_argument('name', help='Name of object')
parser.add_argument("-s", "--symbol", default="", help="Symbol for strategy")
args = parser.parse_args()
print("Start: %s" % args)

engine = engine_from_config(configloader.get_config("db"), prefix='db.')
logging.config.dictConfig(configloader.get_config_log())

if "." in args.name:
    m = importlib.import_module(str(args.name).lower())
    obj = getattr(m, str(args.name).split(".")[1])(engine)
else:
    m = importlib.import_module("strategies.%s" % str(args.name).lower())
    obj = getattr(m, args.name)(engine)
    if args.symbol != "":
        obj.symbol = args.symbol

obj.start()
