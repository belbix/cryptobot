from sqlalchemy import Column, Integer, BigInteger, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Tick(Base):
    __tablename__ = 'ticks'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    market_id = Column(BigInteger, unique=True)
    str_id = Column(String(100), unique=True)
    server = Column(String(16), index=True)
    symbol = Column(String(16), index=True)
    bid = Column(Float)
    ask = Column(Float)
    last = Column(Float)
    amount = Column(Float)
    price = Column(Float)
    volume = Column(Float)
    date = Column(DATETIME(6), default="1999-01-01 00:00:00", index=True)

    def __repr__(self):
        return "Tick" \
               " | id=%s" \
               " | market_id=%s" \
               " | str_id=%s" \
               " | server=%s" \
               " | symbol=%s" \
               " | bid=%s" \
               " | ask=%s" \
               " | last=%s" \
               " | amount=%s" \
               " | price=%s" \
               " | volume=%s" \
               " | date=%s" \
               ")" \
               % (self.id, self.market_id, self.str_id, self.server, self.symbol, self.bid, self.ask, self.last, self.amount,
                  self.price, self.volume, self.date)

    @staticmethod
    def _keys():
        keys = []
        for key in Tick.__dict__.keys():
            if not str(key).startswith("_"):
                keys.append(key)
        return keys
