from sqlalchemy import Column, Integer, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Book(Base):
    __tablename__ = 'books'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    date = Column(DATETIME(6), index=True, default="1999-01-01 00:00:00")
    server = Column(String(16), index=True)
    symbol = Column(String(16), index=True)
    price = Column(Float)
    count = Column(Float)
    amount = Column(Float)

    def __repr__(self):
        return "Book" \
               " | id=%s" \
               " | date=%s" \
               " | server=%s" \
               " | symbol=%s" \
               " | price=%s" \
               " | count=%s" \
               " | amount=%s" \
               ")" \
               % (self.id, self.date, self.server, self.symbol, self.price, self.count, self.amount)

    @staticmethod
    def _keys():
        keys = []
        for key in Book.__dict__.keys():
            if not str(key).startswith("_"):
                keys.append(key)
        return keys
