from sqlalchemy import Column, Integer, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Order(Base):
    __tablename__ = 'orders'
    id = Column(Integer, primary_key=True)
    server = Column(String(16))
    symbol = Column(String(16))
    date_create = Column(DATETIME(6))
    type = Column(Integer)  # 0 - buy, 1 - sell
    amount = Column(Float)
    price = Column(Float)
    market_id = Column(String(100))
    status = Column(Integer)
    charge_id = Column(Integer)

    def __init__(self, server, symbol, date_create, type, amount, price, market_id, status):
        self.server = server
        self.symbol = symbol
        self.date_create = date_create
        self.type = type
        self.amount = amount
        self.price = price
        self.market_id = market_id
        self.status = status

    def __repr__(self):
        return "Order(" \
               " | id=%s" \
               " | server=%s" \
               " | symbol=%s" \
               " | date_create=%s" \
               " | type=%s" \
               " | amount=%s" \
               " | price=%s" \
               " | market_id=%s" \
               " | status=%s" \
               " | charge_id=%s" \
               ")" \
               % (self.id, self.server, self.symbol, self.date_create, self.type, self.amount, self.price,
                  self.market_id, self.status, self.charge_id)

    @staticmethod
    def _keys():
        keys = []
        for key in Order.__dict__.keys():
            if not str(key).startswith("_"):
                keys.append(key)
        return keys
