import pandas
from sqlalchemy import Column, Integer, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Sentiment(Base):
    __tablename__ = 'sentiments'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    date = Column(DATETIME(6), index=True, default="1999-01-01 00:00:00")
    server = Column(String(16), index=True)
    symbol = Column(String(16), index=True)
    price = Column(Float)
    bids = Column(Float)
    asks = Column(Float)
    bids_count = Column(Integer)
    asks_count = Column(Integer)
    orders = Column(Float)
    orders_perc = Column(Float)

    def __repr__(self):
        return "Book" \
               " | id=%s" \
               " | date=%s" \
               " | server=%s" \
               " | symbol=%s" \
               " | price=%s" \
               " | bids=%s" \
               " | asks=%s" \
               " | bids_count=%s" \
               " | asks_count=%s" \
               " | orders=%s" \
               " | orders_perc=%s" \
               ")" \
               % (self.id, self.date, self.server, self.symbol, self.price, self.bids,
                  self.asks, self.bids_count, self.asks_count, self.orders, self.orders_perc)

    @staticmethod
    def _keys():
        keys = []
        for key in Sentiment.__dict__.keys():
            if not str(key).startswith("_"):
                keys.append(key)
        return keys

    def df(self):
        return pandas.DataFrame([[self.id, self.date, self.server, self.symbol, self.price, self.bids,
                                  self.asks, self.bids_count, self.asks_count, self.orders, self.orders_perc]],
                                columns=["id", "date", "server", "symbol", "price", "bids", "asks", "bids_count",
                                         "asks_count",
                                         "orders", "orders_perc"])
