import pandas
from sqlalchemy import Column, Integer, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Candle(Base):
    __tablename__ = 'candles'
    id = Column(Integer, primary_key=True, nullable=False, index=True)
    server = Column(String(16), index=True)
    symbol = Column(String(16), index=True)
    time = Column(Integer, index=True)
    date = Column(DATETIME(6), index=True)
    amount = Column(Float)
    close = Column(Float)
    high = Column(Float)
    low = Column(Float)
    open = Column(Float)
    volume = Column(Float)

    #metrics
    slowk = Column(Float)
    slowd = Column(Float)

    def __init__(self, server, symbol, time, date, amount, close, high, low, open, volume):
        self.server = server
        self.symbol = symbol
        self.time = time
        self.date = date
        self.amount = amount
        self.close = close
        self.high = high
        self.low = low
        self.open = open
        self.volume = volume

    def __repr__(self):
        return "Candle " \
               " | id=%s," \
               " | server=%s" \
               " | symbol=%s" \
               " | time=%s" \
               " | date=%s" \
               " | amount=%s" \
               " | close=%s" \
               " | high=%s" \
               " | low=%s" \
               " | open=%s" \
               " | volume=%s" \
               " | slowk=%s" \
               " | slowd=%s" \
               ")" % \
               (self.id, self.server, self.symbol, self.time, self.date,
                self.amount, self.close, self.high, self.low,
                self.open, self.volume, self.slowk, self.slowd)

    def df(self):
        return pandas.DataFrame([[self.id, self.server, self.symbol, self.time, self.date,
                                  self.amount, self.close, self.high, self.low,
                                  self.open, self.volume]],
                                columns=["id", "server", "symbol", "time", "date", "amount", "close", "high", "low",
                                         "open", "volume"])

    def update(self, candle):
        self.amount = candle.amount
        self.close = candle.close
        self.high = candle.high
        self.low = candle.low
        self.open = candle.open
        self.volume = candle.volume
        self.slowk = candle.slowk
        self.slowd = candle.slowd

    def copy(self):
        candle = Candle(self.server, self.symbol, self.time, self.date, self.amount, self.close,
                        self.high, self.low, self.open, self.volume)
        candle.slowk = self.slowk
        candle.slowd = self.slowd
