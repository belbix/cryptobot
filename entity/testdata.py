from sqlalchemy import Column, Integer, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Testdata(Base):
    __tablename__ = 'testdata'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False)
    date = Column(DATETIME(6), default="1999-01-01 00:00:00")
    data = Column(String(4000))

    def __repr__(self):
        return "Tick" \
               " | id=%s" \
               " | date=%s" \
               " | data=%s" \
               ")" \
               % (self.id, self.date, self.data)

    @staticmethod
    def _keys():
        keys = []
        for key in Testdata.__dict__.keys():
            if not str(key).startswith("_"):
                keys.append(key)
        return keys
