from sqlalchemy import Column, Integer, String, Float, DATETIME
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Charge(Base):
    __tablename__ = 'charges'
    id = Column(Integer, primary_key=True)
    server = Column(String(16))
    symbol = Column(String(16))
    date_create = Column(DATETIME(6))
    type = Column(Integer)  # 0 - buy, 1 - sell
    price = Column(Float)
    candle_id = Column(Integer)
    strategy_id = Column(Integer)
    init_charge = Column(Integer)
    close_position = Column(Integer)  # 0 - init new, 1 - close old

    def __init__(self, server, symbol, date_create, type, price, candle_id, strategy_id, init_charge, close_position=0):
        self.server = server
        self.symbol = symbol
        self.date_create = date_create
        self.type = type
        self.price = price
        self.candle_id = candle_id
        self.strategy_id = strategy_id
        self.init_charge = init_charge
        self.close_position = close_position

    def __repr__(self):
        return "Charge(" \
               " | id=%s" \
               " | server=%s" \
               " | symbol=%s" \
               " | date_create=%s" \
               " | type=%s" \
               " | price=%s" \
               " | candle_id=%s" \
               " | strategy_id=%s" \
               " | init_charge=%s" \
               " | close_position=%s" \
               ")" \
               % (self.id, self.server, self.symbol, self.date_create, self.type, self.price,
                  self.candle_id, self.strategy_id, self.init_charge, self.close_position)

    @staticmethod
    def _keys():
        keys = []
        for key in Charge.__dict__.keys():
            if not str(key).startswith("_"):
                keys.append(key)
        return keys
