import argparse
import importlib
import logging.config

from sqlalchemy import engine_from_config

from utils import configloader
from utils.historyticks import HistoryTicks

engine = engine_from_config(configloader.get_config("db"), prefix='db.')
logging.config.dictConfig(configloader.get_config_log())

h = HistoryTicks(engine)
h.plot = False
h.strategy.kdeltax = 2
h.strategy.kmin = 100
h.strategy.kmax = 0
h.strategy.row_number = 2
h.strategy.datedeltax = 0
h.strategy.tp_price = 5
h.strategy.sl_price = 0
h.strategy.use_old_delta_k2 = False
h.strategy.enable_long = False
h.strategy.enable_short = True
h.strategy.hard_close = False
h.strategy.day_validate = False
h.strategy.kdeltax_day = 0
h.strategy.old_kdeltax = 0
h.start()

h = HistoryTicks(engine)
h.plot = False
h.strategy.kdeltax = 2
h.strategy.kmin = 100
h.strategy.kmax = 0
h.strategy.row_number = 2
h.strategy.datedeltax = 0
h.strategy.tp_price = 5
h.strategy.sl_price = 0
h.strategy.use_old_delta_k2 = False
h.strategy.enable_long = False
h.strategy.enable_short = True
h.strategy.hard_close = False
h.strategy.day_validate = False
h.strategy.kdeltax_day = 0
h.strategy.old_kdeltax = 2
h.start()

h = HistoryTicks(engine)
h.plot = False
h.strategy.kdeltax = 2
h.strategy.kmin = 100
h.strategy.kmax = 0
h.strategy.row_number = 2
h.strategy.datedeltax = 0
h.strategy.tp_price = 5
h.strategy.sl_price = 0
h.strategy.use_old_delta_k2 = False
h.strategy.enable_long = False
h.strategy.enable_short = True
h.strategy.hard_close = False
h.strategy.day_validate = False
h.strategy.kdeltax_day = 0
h.strategy.old_kdeltax = -2
h.start()

