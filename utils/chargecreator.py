import datetime
import logging

from sqlalchemy.orm import sessionmaker

from entity import charge
from entity.charge import Charge
from utils import emailsender

log = logging.getLogger(__name__)


class ChargeCreator:
    engine = None
    Session = None
    es = emailsender.EmailSender()

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)

        charge.Base.metadata.create_all(engine)

    def create_charge(self, symbol, candle, type, strategy_id, init_charge, server="", close_position=0):
        session = self.Session()
        candle_id = 0
        try:
            candle_id = candle.id
            server = candle.server
        except:
            log.debug("candle not persist")

        c = Charge(server, symbol, datetime.datetime.utcnow(), type,
                   candle.close, candle_id, strategy_id, init_charge, close_position)
        id = 0
        try:
            session.add(c)
            session.commit()
            id = c.id
            # self.es.send_email("Trying create order by charge %s" % c)
        except:
            session.rollback()
            log.exception("error add order")
            session.close()
        finally:
            session.close()

        return c
