import logging.config
import os
from datetime import datetime
from math import *

import numpy
import pandas
from matplotlib import pyplot
# from matplotlib import finance
from scipy.signal import argrelextrema

from mappers.bitfinexmapper import BitfinexMapper, get_time
from utils import configloader, mpl_finance
from utils.history import History

log = logging.getLogger(__name__)
logging.getLogger("utils.history").setLevel(logging.WARNING)


def angle_trunc(a):
    while a < 0.0:
        a += pi * 2
    return a


def getAngleBetweenPoints(x_orig, y_orig, x_landmark, y_landmark):
    deltaY = y_landmark - y_orig
    deltaX = x_landmark - x_orig
    return angle_trunc(atan2(deltaY, deltaX)) * 180 / pi


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / numpy.linalg.norm(vector)


def angle_between(v1, v2):
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return numpy.arccos(numpy.clip(numpy.dot(v1_u, v2_u), -1.0, 1.0))


def calc_y(coords, x):
    y = None
    if coords[0][1] != 0 and coords[0][1] != 0:
        a = coords[0][1] - coords[1][1]
        b = coords[0][0] - coords[1][0]
        c = coords[0][0] * coords[1][1] - coords[1][0] * coords[0][1]
        # f = a * x + b * y + c
        y = (a * x + c) / b
    return y


def draw_level(coords, len):
    if coords[0][1] != 0 and coords[0][1] != 0:
        a = coords[0][1] - coords[1][1]
        b = coords[0][0] - coords[1][0]
        c = coords[0][0] * coords[1][1] - coords[1][0] * coords[0][1]
        starty = (a * 0 + c) / b
        endy = (a * len + c) / b
        # pyplot.plot([0, coords[0][0], coords[1][0], len], [starty, coords[0][1], coords[1][1], endy])
        pyplot.plot([coords[0][0], coords[1][0], len], [coords[0][1], coords[1][1], endy])


class Levels:
    mapper = BitfinexMapper("", "")

    enable_plot = False
    symbol = None
    period = None
    loop_delay = None
    test = None
    last_tick = None
    candle_batch = None
    down = [[0, 0], [0, 0]]  # [A[x1,y1]B[x2,y2]]
    up = [[0, 0], [0, 0]]
    candles = None
    pairs = {}
    mins = None
    maxs = None
    symbols = None
    base_count_minutes = 0
    count_minutes = 0
    level_buy = 1
    level_buy_end = 1.000001
    level_sell = 1
    level_sell_end = 0.999999
    y = 0
    fy_up = 0
    fy_down = 0
    tail_count = 0
    draw_extreme = False
    close_buy = False
    close_sell = False
    down_angle = 0
    up_angle = 0
    extend_candles = None
    scale_try_count = 2
    upl = 0
    downl = 0
    exclude = ["AVT", "BCC", "BCU", "BT1", "BT2", "DAT", "EDO", "EOS", "ETP", "RRT", "SAN"]

    def __init__(self, engine):
        self.engine = engine
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)
        self.symbol = config["symbol"]
        self.period = config["period"]
        self.base_count_minutes = config["count_minuts"]
        self.count_minutes = self.base_count_minutes
        self.loop_delay = config["loop_delay"]
        self.test = config["test"]
        self.candle_batch = int(config["candle_batch"])
        self.last_tick = datetime.utcnow()

    def get_candles_hist(self):
        h = History(self.engine)
        h.period = self.period
        h.symbol = self.symbol
        h.count_minuts = self.count_minutes
        h.count_loop = 0
        h.candle_time = int(get_time(h.period))
        return h.get_candles()

    def calc_linear_func(self, mins, maxs):
        if len(mins) == 0 or len(maxs) == 0:
            return False
        down = [[0, 0], [0, 0]]  # [A[x1,y1]B[x2,y2]]
        up = [[0, 0], [0, 0]]
        new_val = False
        for i in mins:
            # log.info("mins[i]: %s < down: %s" % (mins[i], down))
            if mins[i] < down[0][1] or down[0][1] == 0:
                new_val = True
                down[0][0] = i
                down[0][1] = mins[i]
                down[1][0] = i
                down[1][1] = mins[i]
                continue

            if new_val or calc_y(down, i) > mins[i]:
                new_val = False
                down[1][0] = i
                down[1][1] = mins[i]

        new_val = False
        for i in maxs:
            if maxs[i] > up[0][1] or up[0][1] == 0:
                new_val = True
                up[0][0] = i
                up[0][1] = maxs[i]
                up[1][0] = i
                up[1][1] = maxs[i]
                continue

            if new_val or calc_y(up, i) < maxs[i]:
                new_val = False
                up[1][0] = i
                up[1][1] = maxs[i]

        dy = up[0][1] - down[1][1]
        x1 = min(mins)
        x2 = max(maxs)

        scale = dy / (x2 - x1)

        if down[0][1] == 0 \
                or down[1][1] == 0 \
                or down[0][1] == down[1][1]:
            log.debug("Level down not found for %s, try on greater scale. %s " % (self.symbol, down))
            self.down_angle = 0
            self.down = [[0, 0], [0, 0]]
            self.downl = 0
        else:
            x1 = down[1][0] - down[0][0]
            y1 = down[1][1] - down[0][1]
            y1 /= scale
            self.down_angle = int(getAngleBetweenPoints(0, 0, x1, y1))
            self.down = down
            self.downl = x1

        if up[0][1] == 0 \
                or up[1][1] == 0 \
                or up[0][1] == up[1][1]:
            log.debug("Level up not found for %s, try on greater scale%s" % (self.symbol, up))
            self.up_angle = 0
            self.up = [[0, 0], [0, 0]]
            self.upl = 0
        else:
            x1 = up[1][0] - up[0][0]
            y1 = up[0][1] - up[1][1]
            y1 /= scale
            # log.info("%s | %s | %s" % (x1, y1, scale))
            t = int(getAngleBetweenPoints(0, y1, x1, 0))
            if t == 0:
                self.upl = 0
                self.up_angle = 0
            else:
                self.upl = x1
                self.up_angle = 360 - t
            self.up = up  # [A[x1,y1]B[x2,y2]]

        if self.up_angle == 0 or self.down_angle == 0:
            return False
        else:
            return True

    def draw_graph(self):
        mins = self.mins
        maxs = self.maxs
        candles = self.candles
        fig, ax = pyplot.subplots()
        try:
            mpl_finance.candlestick2_ochl(ax, candles["open"], candles["close"], candles["high"], candles["low"],
                                          width=0.3, colorup="#6ba583", colordown="#d75442", alpha=1)
        except Exception as e:
            log.exception("Error draw plot for %s" % candles)

        pyplot.title(self.symbol)
        if self.draw_extreme:
            for i in mins:
                pyplot.plot(i, mins[i], "g^")

            for i in maxs:
                pyplot.plot(i, maxs[i], "rv")

        draw_level(self.down, len(candles["close"]))
        draw_level(self.up, len(candles["close"]))
        try:
            pyplot.savefig("images" + os.sep + self.symbol + ".png", dpi=180)
        except:
            log.error("Error save plot!")
        # pyplot.show()
        pyplot.close()

    def get_levels_points(self):
        # log.info("candles: %s" % self.candles)
        order = 1
        l = len(self.candles["low"])
        # log.error("l: %s" % l)
        candles = self.candles.head(l - self.tail_count)
        lows = candles["low"].as_matrix()
        highs = candles["high"].as_matrix()
        emax = argrelextrema(highs, numpy.greater, order=order)[0]
        emin = argrelextrema(lows, numpy.less, order=order)[0]
        # log.info(len(emin))
        # log.info(len(emax))

        mins = {}
        maxs = {}

        for i in range(0, len(lows)):
            if i in emin:
                mins[i] = lows[i]
            if i in emax:
                maxs[i] = highs[i]
        return [mins, maxs]

    def check_event(self):
        x = len(self.candles["close"]) - 1
        y = self.y = self.candles["close"].iloc[-1]
        # log.info("x: %s | y: %s" % (x, y))
        # pyplot.plot(x, y, "ro")
        self.fy_up = fy_up = calc_y(self.up, x)
        self.fy_down = fy_down = calc_y(self.down, x)
        if fy_down is None or fy_up is None:
            log.debug("Level not set! %s \n %s \n %s" % (self.up, self.down, x))
            return

        if fy_up < fy_down and fy_down > y > fy_up:
            log.debug("UP < DOWN! up=%s | down=%s" % (fy_up, fy_up))
            return

        if (fy_up * self.level_buy) < y < (fy_up * self.level_buy_end):
            if self.pairs.get(self.symbol) is not None:
                log.warning("Pair %s rewrite!" % self.symbol)
            self.pairs[self.symbol] = "buy"
            log.debug(self.symbol + ":" + "buy")
            # pyplot.plot(x, y, "go")

        if (fy_down * self.level_sell) > y > (fy_down * self.level_sell_end):
            if self.pairs.get(self.symbol) is not None:
                log.warning("Pair %s rewrite!" % self.symbol)
            self.pairs[self.symbol] = "sell"
            log.debug(self.symbol + ":" + "sell")
            # pyplot.plot(x, y, "ro")

            # if self.pairs.get(self.symbol) is None:
            #     self.pairs[self.symbol] = "none"

    def processing_levels(self, get_candles=True):
        levels_found = False
        try_count = 0
        while not levels_found:
            if get_candles:
                log.debug("get candle start %s" % self.symbol)
                self.candles = self.get_candles_hist()  # type: pandas.DataFrame
                log.debug("get candle end")
            if self.extend_candles is not None:
                self.candles = self.extend_candles.tail(self.count_minutes)
            datasets = self.get_levels_points()
            # log.debug("get level points end")
            self.mins = datasets[0]
            self.maxs = datasets[1]
            try_count += 1
            levels_found = self.calc_linear_func(self.mins, self.maxs)
            # log.debug("calc_linear_func end")
            if not levels_found:
                # log.debug("levels not found, minuts * 2")
                self.count_minutes = self.count_minutes * 2
            if try_count > self.scale_try_count:
                break
        self.count_minutes = self.base_count_minutes

    def get_symbols(self):
        self.symbols = self.mapper.get_symbols()
        log.info(self.symbols)

    def check_pairs(self, get_candles=True):
        self.pairs = {}
        self.y = 0
        self.fy_down = 0
        self.fy_up = 0
        for pair in self.symbols:
            # log.info("pair: %s" % pair)
            # if (not str(pair).upper().endswith("BTC") and not str(pair).upper().startswith("BTC")) \
            if str(pair).upper()[:-3] in self.exclude:
                continue
            self.symbol = str(pair).upper()
            try:
                self.processing_levels(get_candles=get_candles)
                if self.enable_plot:
                    self.draw_graph()
                self.check_event()
            except:
                log.exception("error in processing symbol %s" % self.symbol)

    def write_result(self):
        f = open("pairs.txt", "w+")
        for pair in self.pairs.keys():
            line = pair + ": " + self.pairs.get(pair)
            # log.info(line)
            f.write(line + "\n")
        f.close()

    def start(self):
        # logging.getLogger(__name__).setLevel(logging.INFO)
        # self.get_symbols()
        self.symbols = [str("XBTUSD").upper()]
        self.check_pairs()
        self.write_result()
