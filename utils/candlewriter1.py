import datetime
import logging
import os

from sqlalchemy import engine_from_config

from entity.candle import Base
from utils import configloader
from utils.candlecreator import CandleCreator


log = logging.getLogger(__name__)


class CandleWriter1:

    def __init__(self, engine):
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])

        self.engine = engine
        self.server = config["server"]
        self.symbol = config["symbol"]
        self.timeframe = config["timeframe"]
        self.date_start = config["date"]
        self.count = config["count"]

        Base.metadata.create_all(engine)
        self.cc = CandleCreator(engine)

    def start(self):
        if self.date_start == "":
            date = datetime.datetime.utcnow()
        else:
            date = datetime.datetime.strptime(self.date_start, "%Y-%m-%d %H:%M:%S")

        for i in range(0, self.count):
            rd = self.cc.create_dates(date - datetime.timedelta(minutes=self.timeframe * i), self.timeframe, False)
            d = rd[1] - datetime.timedelta(microseconds=1)
            # log.info("date: %s | %s" % (rd[0], d))
            self.cc.get_batch_candles2(1, d, self.timeframe, self.symbol, self.server, True)

