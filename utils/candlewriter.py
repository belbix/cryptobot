import logging
import os
import threading
import time
from datetime import datetime, timedelta

import pandas
from sqlalchemy import select, column
from sqlalchemy.orm import sessionmaker

from entity import tick, candle as candle_clazz
from mappers.bitfinexmapper import get_time, BitfinexMapper
from mappers.bitmexmapper import BitmexMapper
from mappers.ibmapper import IBMapper
from utils import configloader
from utils.candlecreator import CandleCreator

log = logging.getLogger(__name__)
logging.getLogger("bitmex.market_maker.ws.ws_thread").setLevel(logging.INFO)


# logging.getLogger("utils.candlecreator").setLevel(logging.INFO)


class CandleWriter:
    DELAY = 5
    cc = None

    def __init__(self, engine):
        self.engine = engine
        self.cc = CandleCreator(engine)
        self.engine = engine
        candle_clazz.Base.metadata.create_all(engine)
        tick.Base.metadata.create_all(engine)

        config = configloader.get_config(os.path.basename(__file__).split(".")[0])

        self.loop_delay = config["loop_delay"]
        self.symbol = config["symbol"]
        self.server = config["server"]
        self.api_key = config["api_key"]
        self.api_secret = config["api_secret"]
        self.timeframe = config["timeframe"]
        self.timeframe = get_time(self.timeframe)
        self.rewrite_candle_count = config["rewrite_candle_count"]
        self.cc.write_metrics = config["write_metrics"]
        self.cc.candle_limit = config["candle_limit"]
        self.proxy = config["proxy"]
        if self.server == "bitmex":
            # self.mapper = BitmexMapper(self.api_key, self.api_secret, base_url="https://www.bitmex.com/api/v1/",
            #                            shouldWSAuth=False, proxy=self.proxy)
            self.mapper = BitmexMapper(self.api_key, self.api_secret, base_url="https://www.bitmex.com/api/v1",
                                       skipws=True, postOnly=True)
        elif self.server == "bitfinex":
            self.mapper = BitfinexMapper(self.api_key, self.api_secret)
        elif self.server == "bitmex":
            self.mapper = BitmexMapper("", "", skipws=True)
        elif self.server == "ib":
            self.mapper = IBMapper(self.api_key, self.api_secret)
        else:
            raise Exception("Not found server name!")

    def tick_writer(self, symbol):
        Session = sessionmaker(bind=self.engine)
        session = Session()
        first = True
        last_price = None
        while True:
            start = datetime.utcnow()
            try:
                t = None
                i = 0
                while i < (20 * 30):
                    threading.Thread(target=self.mapper.get_tick, args=(symbol,)).start()
                    time.sleep(self.loop_delay)
                    if self.mapper.tick is not None:
                        t = self.mapper.tick
                        self.mapper.tick = None
                        break
                    else:
                        log.info("Tick lag...")
                if t is None:
                    log.info("Error get tick!")
                    continue
                if last_price is None or last_price != t.price:
                    log.info(t)
                    session.add(t)
                    session.commit()
                    session.flush()
                    last_price = t.price
                    self.candle_writer2(first)
                    first = False
                else:
                    log.debug("skip double tick")
            except Exception as e:
                session.rollback()
                log.exception("Error in tick write %s" % e)
                session.close()
                session = Session()
            # finally:
            #     session.close()
            log.debug("tick_writer time %s" % (datetime.utcnow() - start))

    def candle_writer(self, period, name):
        Session = sessionmaker(bind=self.engine)
        session = Session()
        while True:
            try:
                candle_write(period, name, session)
                session.commit()
                time.sleep(2)
            except Exception as e:
                session.rollback()
                log.exception("Error in candle write")
                session.close()
                session = Session()
            finally:
                session.close()

    def candle_writer2(self, first, srv=None):
        if first:
            try:
                Session = sessionmaker(bind=self.engine)
                session = Session()
                last_candle = session.query(candle_clazz.Candle) \
                    .filter(candle_clazz.Candle.server == self.server) \
                    .filter(candle_clazz.Candle.time == self.timeframe) \
                    .filter(candle_clazz.Candle.symbol == self.symbol) \
                    .order_by('date desc').limit(1).all()
                session.close()
                log.info("last_candle: %s" % last_candle)
                last_date = None
                if len(last_candle) > 0:
                    last_date = last_candle[0].date
                else:
                    raise Exception("Cant get last candle!")
                delta_seconds = (datetime.utcnow() - last_date).total_seconds()
                log.info("%s | %s delta_seconds: %s" % (datetime.utcnow(), last_date, delta_seconds))
                count = int(round(delta_seconds / 60) / self.timeframe) + 2
                log.info("Rewrite first %s candles" % count)
            except Exception as e:
                log.error("%s" % e)
                count = self.rewrite_candle_count
        else:
            count = self.rewrite_candle_count
        log.info("get %s candles" % count)
        for i in range(0, count):
            rd = self.cc.create_dates(datetime.utcnow() - timedelta(minutes=self.timeframe * i), self.timeframe, False)
            d = rd[1] - timedelta(microseconds=1)
            log.debug("d: %s | iteration: %s" % (d, i))
            if srv is None:
                srv = self.mapper.SERVER_NAME
            try:
                self.cc.get_batch_candles2(1, d, self.timeframe, symbol=self.symbol, server=srv, indb=True,
                                           cached=self.rewrite_candle_count)
            except Exception as e2:
                log.exception("Error get_batch_candles2 %s" % e2)

    def start(self):
        self.tick_writer(self.symbol)


def candle_write(period, name, session):
    candle_date = datetime.utcnow() - timedelta(minutes=period)
    sql = select(['*']).select_from(tick.Tick) \
        .where(column('date') > candle_date) \
        .where(column('name') == name)
    ticks = pandas.read_sql(sql, session.bind, parse_dates=["date"], index_col=["date"])
    length = len(ticks["name"])
    if length == 0:
        log.info("Ticks nor found for period " + str(period) + " min " + str(candle_date))
        return
    elif length < 4:
        log.info("Small count ticks for " + str(period) + " min " + str(length))
        return
    candle = calculate_candle(ticks, period)
    candle_exist = session.query(candle_clazz.Candle) \
        .filter(candle_clazz.Candle.date == candle.date) \
        .filter(candle_clazz.Candle.time == candle.time) \
        .all()
    if len(candle_exist) != 0:
        if len(candle_exist) > 1:
            log.info("Error count candles! " + str(candle_exist[0]))
        # log.info(str(candles[0]))
        candle_exist[0].amount = candle.amount
        candle_exist[0].close = candle.close
        candle_exist[0].high = candle.high
        candle_exist[0].low = candle.low
        candle_exist[0].open = candle.open
        candle_exist[0].volume = candle.volume
    else:
        session.add(candle)


def calculate_candle(ticks, period):
    ohlc = ticks["last"].resample(str(period) + 'Min').ohlc()
    # log.info(ohlc)
    c = ohlc.iloc[-1]
    # log.info(ticks.iloc[0])
    return candle_clazz.Candle(ticks.iloc[0]["server"], ticks.iloc[0]["name"], period,
                               c.name, 0, c["close"], c["high"], c["low"], c["open"], 0)
