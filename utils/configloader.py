import json
import os

import yaml

CONFIGS_PATH = "configs" + os.sep
LOGS_PATH = "logs" + os.sep


def get_config(name):
    file_name = name + ".json"
    if not os.path.exists(CONFIGS_PATH + file_name):
        file_name = name + "_default" + ".json"
    with open(CONFIGS_PATH + file_name) as f:
        result = json.load(f)
    return result


def get_config_log():
    if not os.path.exists(LOGS_PATH):
        os.makedirs(LOGS_PATH)
    file_name = "logging.yaml"
    if not os.path.exists(CONFIGS_PATH + file_name):
        file_name = "logging_default.yaml"
        if not os.path.exists(CONFIGS_PATH + file_name):
            print("Error load logger!!!")
            return None
    with open(CONFIGS_PATH + file_name) as f:
        result = yaml.safe_load(f)
    return result


def get_file(name):
    if not os.path.exists(CONFIGS_PATH + name):
        return {}
    with open(CONFIGS_PATH + name) as f:
        result = json.load(f)
    return result