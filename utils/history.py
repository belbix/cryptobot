import calendar
import importlib
import logging.config
import os
import time
from datetime import datetime, timedelta

import numpy
import pandas
from matplotlib import pyplot

from mappers.bitfinexmapper import BitfinexMapper, get_time
from mappers.bitmexmapper import BitmexMapper
from mappers.ibmapper import IBMapper
from utils import configloader

log = logging.getLogger(__name__)


def history_loop(strategy, h_candles, count_candles, config):
    result_profit = 0
    candles_size = len(h_candles["date"])
    # log.info("candles_size: " + str(candles_size))
    i = 0
    profits = []
    sum_profits_arr = []
    sum_profits = 0
    try:
        while i < (candles_size - count_candles):
            df = h_candles.head(count_candles)
            # log.info(df["date"])
            strategy.processing(df)

            profits.append(strategy.profit)
            if strategy.profit != 0:
                sum_profits += strategy.profit
                sum_profits_arr.append(sum_profits)

            h_candles = h_candles.drop(h_candles.index[0])
            i += 1
    finally:
        # log.info("Balance history:")
        for p in sum_profits_arr:
            log.debug(p)
        if len(sum_profits_arr) > 0:
            if config["simple"]:
                pyplot.plot(sum_profits_arr)
                pyplot.savefig("images" + os.sep + "FH_" + ("_%s" % int(datetime.utcnow().timestamp())) + ".png", dpi=180)
                pyplot.show()
            result_profit = numpy.sum(profits)
            result = "Result:\n"
            result += "  profits:" + str(result_profit) + "\n"
            result += "  max profit:" + str(numpy.amax(profits)) + "\n"
            result += "  min profit:" + str(numpy.amin(profits)) + "\n"
            result += "  max balance:" + str(numpy.amax(sum_profits_arr)) + "\n"
            result += "  min balance:" + str(numpy.amin(sum_profits_arr)) + "\n"
            result += "  count: " + str(strategy.count) + "\n"
            result += str(config) + "\n"
            result += str(strategy.config)
            log.warning(result)
        else:
            log.warning("Result: count = 0")
    return result_profit


class History:
    # configurable variable
    engine = None
    loop_delay = None
    symbol = None
    period = None
    count_days = None
    count_loop = None
    skip_loop = False
    profit = 0
    profits = []
    last_profits = 0

    # variable
    current_date = datetime.utcnow()
    run = True
    count = 0
    candle_time = None
    log.info("candle_time: " + str(candle_time))
    count_minuts = None
    params = None
    simple = True
    strategy_name = ""

    def __init__(self, engine):
        self.engine = engine
        self.config = config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.loop_delay = int(config["loop_delay"])
        self.server = config["server"]
        self.symbol = config["symbol"]
        self.period = config["period"]
        self.simple = config["simple"]
        self.count_days = int(config["count_days"])
        self.count_loop = int(config["count_loop"])
        self.params = config["params"]
        self.strategy_name = config["strategy_name"]
        self.count_candles = config["count_candles"]
        self.proxy = config["proxy"]
        self.candle_time = int(get_time(self.period))
        self.count_minuts = self.count_days * 24 * 60

    def processing(self):
        if self.skip_loop:
            return
        log.info("----------Start-------------")
        h_candles = self.get_candles()
        log.info("Start date: " + str(h_candles.iloc[0]["date"]))
        m = importlib.import_module("strategies.%s" % str(self.strategy_name).lower())
        strategy = getattr(m, self.strategy_name)(self.engine)
        strategy.STRATEGY_ID = -1
        strategy.test = True
        strategy.hist = True
        strategy.symbol = self.symbol
        if self.simple:
            history_loop(strategy, h_candles, self.count_candles, self.config)
        else:
            log.setLevel(logging.INFO)
            params_value = {}
            for key in self.params:
                params_value[key] = getattr(strategy, key)
            self.profits = {0: params_value}

            try:
                for x in range(0, self.count_loop):
                    for param_name1 in self.params:
                        # setup strategy fo max result
                        for key in self.params:
                            param_value = self.profits[max_val(self.profits)][key]
                            # if param_value is None or param_value == 0:
                            #     self.profits[max_val(self.profits)][key] = getattr(strategy, key)
                            # else:
                            #     if max_val(self.profits) > 0:
                            #         params_value[key] = param_value
                            #     else:
                            #         params_value[key] = self.params[key]["min"]
                            setattr(strategy, key, param_value)
                            params_value[key] = param_value

                        for i in range(self.params[param_name1]["min"], self.params[param_name1]["max"]):
                            last_param = params_value[param_name1]
                            params_value[param_name1] = i
                            setattr(strategy, param_name1, i)
                            h_profit = history_loop(strategy, h_candles, self.count_candles, self.config)
                            log.info("profit: %s for parametrs: %s" % (h_profit, params_value))
                            if self.last_profits == 0 or self.last_profits < h_profit:
                                self.last_profits = h_profit
                                if h_profit > max_val(self.profits):
                                    self.profits[h_profit] = params_value
                                    log.info("new best profit %s" % h_profit)
                            else:
                                log.info("regress profit %s -> %s" % (self.last_profits, h_profit))
                                self.last_profits = 0
                                params_value[param_name1] = last_param
                                break

                        log.info("best profits: %s" % self.profits)
                        log.info("END loop %s: %s" % (param_name1, params_value[param_name1]))
                    log.info("END X LOOP %s: %s" % (x, params_value))

            finally:
                log.info(params_value)

        log.info("----------End-------------")

    def get_candles(self):
        if self.count_minuts > self.candle_time:
            limit = self.count_minuts / self.candle_time
        else:
            limit = 1
        MAX_LIMIT = 500
        proxy = {"http": "http://localhost:3132", "https": "https://localhost:3132"}
        # proxy = {}
        if self.server == "bitmex":
            mapper = BitmexMapper("JKIAo442CYwX5ak5KlYJGwPa",
                                  "83UWnblikD2j-2l5-KBtyvHa4P3ViuqAJdern4XMm2e0bpZr",
                                  base_url="https://www.bitmex.com/api/v1/", shouldWSAuth=False,
                                  proxy=self.proxy)
        elif self.server == "bitfinex":
            mapper = BitfinexMapper("", "")
        elif self.server == "ib":
            mapper = IBMapper("", "", start=False)
        else:
            raise Exception("Not found server name!")

        if limit > MAX_LIMIT:
            limit = MAX_LIMIT

        log.info("limit: " + str(limit))
        history_candles_arr = []
        log.info("get history to " + str(datetime.utcnow() - timedelta(minutes=self.count_minuts)))
        while self.run:
            try:
                # mapper = BitfinexMapper("", "")
                # delta_days = int(limit / ((24 * 60) / candle_time))
                delta_minutes = int(limit * self.candle_time)
                if delta_minutes == 0:
                    delta_minutes = 1
                # delta_days = int(((candle_time * limit) / 60) / 24)
                log.info("delta_minutes: " + str(delta_minutes))

                start = calendar.timegm((self.current_date - timedelta(minutes=delta_minutes)).utctimetuple())
                end = calendar.timegm(self.current_date.utctimetuple())
                start = str(int(start)) + "000"
                end = str(int(end)) + "000"
                candles_ = None
                count = 0
                while candles_ is None:
                    count += 1
                    if count > 10:
                        return None
                    if mapper.SERVER_NAME != "bitmex":
                        candles_ = mapper.get_candles(self.period, self.symbol, True, limit, start, end)
                    else:
                        candles_ = mapper.get_candles(self.period, self.symbol, True, limit,
                                                      str(self.current_date - timedelta(minutes=delta_minutes)),
                                                      str(self.current_date))
                    if candles_ is None:
                        time.sleep(10)

                l = len(candles_["date"])
                log.info("len: " + str(l))
                if l == 0:
                    log.warning("l = 0")
                    break

                self.current_date = candles_.iloc[-1]["date"]
                log.info("current = " + str(self.current_date))

                self.current_date = candles_.iloc[0]["date"]
                log.info("last = " + str(self.current_date))
                self.current_date -= timedelta(minutes=self.candle_time)
                self.count += 1
                self.count_minuts -= delta_minutes
                if self.count_minuts < delta_minutes:
                    limit = self.count_minuts / self.candle_time
                    log.info("Last limit: " + str(limit))
                log.info("count_minuts: " + str(self.count_minuts))

                history_candles_arr.append(pandas.DataFrame(candles_))

                if self.count_minuts <= 0:
                    self.run = False
                time.sleep(self.loop_delay)
            except:
                log.exception("main loop error")
                self.run = False
        if history_candles_arr is not None:
            return pandas.concat(history_candles_arr, ignore_index=True).sort_values(by="date")
        else:
            return None

    def start(self):
        self.processing()


def max_val(arr):
    max_key = 0
    for key in arr:
        if key > max_key:
            max_key = key
    return max_key

# logging.config.dictConfig(configloader.get_config_log())
# engine = engine_from_config(configloader.get_config("db"), prefix='db.')
# History(engine).processing()
