import calendar
import logging.config
import os
import time
from datetime import datetime, timedelta

from sqlalchemy import func
from sqlalchemy.orm import sessionmaker

from entity import tick
from entity.tick import Tick
from mappers.bitfinexmapper import BitfinexMapper
from mappers.bitmexmapper import BitmexMapper
from mappers.ibmapper import IBMapper
from utils import configloader
from utils.candlewriter import CandleWriter

log = logging.getLogger(__name__)


class TickDownloader:
    run = True
    Session = None
    symbol = None
    server_name = None
    from_date = None  # 01/14/2013
    mapper = None
    last_date = None
    batch = None
    ticks_cache = []
    ticks_cache2 = []
    exist_market_id = []
    exist_str_id = []
    ticks = []
    candlewriter = None

    def __init__(self, engine):
        self.Session = sessionmaker(bind=engine, autocommit=False)

        tick.Base.metadata.create_all(engine)

        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.loop_delay = int(config["loop_delay"])
        self.symbol = config["symbol"]
        self.timeframe = config["timeframe"]
        self.server_name = config["server_name"]
        self.batch = config["batch"]
        self.create_candles = config["create_candles"]
        self.rewrite_candle_count = config["rewrite_candle_count"]
        self.from_date = datetime.utcnow() - timedelta(days=int(config["day_delta"]))
        self.last_date = self.from_date

        if self.server_name == "bitfinex":
            self.mapper = BitfinexMapper("", "")
        elif self.server_name == "bitmex":
            key = configloader.get_file("bitmex_api.json")
            self.mapper = BitmexMapper(key["api_key_read"], key["api_secret_read"],
                                       base_url="https://www.bitmex.com/api/v1", skipws=True, postOnly=True)
        elif self.server_name == "ib":
            self.mapper = IBMapper("", "")
        else:
            raise Exception("Not found server name!")

        self.candlewriter = CandleWriter(engine)
        self.candlewriter.symbol = self.symbol
        self.candlewriter.timeframe = self.timeframe
        self.candlewriter.server = self.server_name
        self.candlewriter.rewrite_candle_count = self.rewrite_candle_count

    def start(self):
        log.info("--------------------Start write history from %s--------------------", self.from_date)
        last_date_db = self.get_max_date()
        if last_date_db is not None:
            log.info("Last date from DB %s " % last_date_db)
            self.last_date = last_date_db
        else:
            log.info("Cant get last date from DB, setup %s " % self.last_date)

        while self.run:
            start = datetime.utcnow()
            if self.processing() == -1:
                delta_time = (int(datetime.utcnow().timestamp()) - int(self.last_date.timestamp()))
                if delta_time > (60 * 60 * 24):
                    self.last_date += timedelta(minutes=60)
                    log.info("add 60 minutes to last date and get: %s" % self.last_date)
                else:
                    log.info("sleep 10 sec, %s" % delta_time)
                    time.sleep(10)
                continue
            if self.create_candles:
                self.candlewriter.candle_writer2(True, self.server_name)
            log.debug("processing time %s" % (datetime.utcnow() - start))
            time.sleep(self.loop_delay)

    def create_date(self, date):
        if self.server_name == "bitmex":
            return date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
        else:
            return str(calendar.timegm(date.utctimetuple())) + "000"

    def processing(self):
        try:
            current_date = self.last_date
            start = self.create_date(self.last_date)
            end = self.create_date(datetime.utcnow())

            try:
                panda_ticks = self.mapper.get_history_tick(self.symbol, self.batch, start, end)
            except Exception as e:
                log.error("Cant get history tick: %s" % e, e)
                time.sleep(1)
                return

            if panda_ticks is None or panda_ticks.empty:
                log.warning("Get 0 ticks!")
                return -1
            log.debug("Get %s ticks from date %s" % (panda_ticks["date"].size, panda_ticks.iloc[0]["date"]))
            session = self.Session()
            self.get_exist_ticks(session, panda_ticks)

            count = self.mapping_ticks(panda_ticks)

            if count == 0 and self.last_date == current_date:
                log.info("In this time hasn't actual ticks, add 1 milliseconds")
                self.last_date += timedelta(milliseconds=1)

            try:
                start_time = datetime.utcnow()
                session.bulk_save_objects(self.ticks)
                log.debug("try commit")
                session.commit()
                log.info("Write %s ticks | %s | time: %s" % (count, self.last_date, (datetime.utcnow() - start_time)))
            except:
                session.rollback()
                log.info("start: %s | end: %s" % (panda_ticks["date"].min(), panda_ticks["date"].max()))
                log.exception("Error in commit tick")
                session.close()
            # if len(self.ticks_cache) > 100000:
            #     self.ticks_cache2.clear()
            #     self.ticks_cache2 = self.ticks_cache.copy()
            #     self.ticks_cache.clear()
        except:
            log.exception("main loop error")
            # self.run = False
            time.sleep(10)

    def mapping_ticks(self, panda_ticks):
        start = datetime.utcnow()
        count = 0
        exist_id = 'id' in panda_ticks
        exist_str_id = 'str_id' in panda_ticks
        self.ticks.clear()
        for el in panda_ticks.itertuples():
            if exist_id and el.id in self.exist_market_id:
                continue
            if exist_str_id and el.str_id in self.exist_str_id:
                continue

            count += 1
            # log.info(el)
            t = Tick()
            t.server = self.server_name
            t.symbol = self.symbol
            t.amount = el.amount
            t.price = el.price
            t.date = el.date

            if exist_id and el.id:
                t.market_id = el.id
            else:
                t.market_id = None

            if exist_str_id and el.str_id:
                t.str_id = el.str_id
            else:
                t.str_id = None

            self.ticks.append(t)
            self.last_date = el.date
        log.debug("mapping_ticks time: %s" % (datetime.utcnow() - start))
        return count

    def get_exist_ticks(self, session, panda_ticks):
        start = datetime.utcnow()
        log.debug("start get exist ticks")
        try:
            exist_ticks = session.query(Tick) \
                .filter(Tick.server == self.server_name) \
                .filter(Tick.symbol == self.symbol) \
                .filter(Tick.date >= panda_ticks["date"].min() - timedelta(minutes=1)) \
                .filter(Tick.date <= panda_ticks["date"].max() + timedelta(minutes=1)) \
                .all()
        except:
            log.exception("Error in get exist ticks")
            session.close()
            return
        self.exist_market_id = []
        self.exist_str_id = []
        for et in exist_ticks:
            if et.market_id is not None and et.market_id != "":
                self.exist_market_id.append(et.market_id)
            if et.str_id is not None and et.str_id != "":
                self.exist_str_id.append(et.str_id)
        skip_count = len(self.exist_market_id) + len(self.exist_str_id)
        log.debug("end get exist ticks %s | %s" % (skip_count, datetime.utcnow() - start))

    def get_max_date(self, from_date=None, server_name=None, symbol=None):
        start = datetime.utcnow()
        if from_date is None:
            from_date = self.from_date
        # else:
        #     from_date = from_date
        if server_name is None:
            server_name = self.server_name
        if symbol is None:
            symbol = self.symbol

        result = None
        while result is None:
            session = self.Session()
            try:
                max_date_tick = session.query(func.max(Tick.date)) \
                    .filter(Tick.server == server_name) \
                    .filter(Tick.symbol == symbol) \
                    .filter(Tick.date > from_date).all()
                if len(max_date_tick) != 0:
                    log.info("max_date_tick: %s" % max_date_tick)
                    result = max_date_tick[0][0]
                    if result is None:
                        return None
                else:
                    log.warning("Ticks not found!")
                    return None
            except Exception as e:
                log.exception("Error in max date %s" % e)
                session.close()
                time.sleep(self.loop_delay)
            finally:
                session.close()
        log.debug("get_max_date time: %s" % (datetime.utcnow() - start))
        return result

# log.info("----------Start-------------")
# engine = engine_from_config(configloader.get_config("db"), prefix='db.')
# logging.config.dictConfig(configloader.get_config_log())
#
# td = TickDownloader(engine)
# td.start()
#
# log.info("----------End-------------")
