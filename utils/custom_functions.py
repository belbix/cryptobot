from __future__ import print_function

import contextlib
import copy
# import httplib
import os
import shutil
import socket
# noinspection PyPep8Naming
import xml.etree.ElementTree as ET
# import urllib2
import subprocess

import zipfile

import libs.argparse as argparse
import datetime

try:
    from collections import OrderedDict
except ImportError:
    from libs.ordereddict import OrderedDict

verbose = False
args = None
root = os.path.dirname(os.path.abspath(__file__))

db_pass = OrderedDict()
for l in open("db_pass.txt", "a+").readlines():
    tmp = l.split("=")
    if len(tmp) == 2:
        db_pass[tmp[0]] = tmp[1]


def pex(e, msg):
    traceback.print_exception(e, "\n" + str(msg), sys.exc_traceback)


def unzip(source, target):
    with contextlib.closing(zipfile.ZipFile(source, "r")) as z:
        z.extractall(target)
    print("Extracted : " + source + " to: " + target)


def find_in_zip(source, key):
    if not os.path.exists(source):
        return False
    with contextlib.closing(
            zipfile.ZipFile(source, "r")) as z:
        for name in z.namelist():
            if key in name:
                return True
    return False


def trimstr(s):
    return s.replace(" ", "").replace("\n", "").replace("\r", "")


def p(msg):
    print(msg)
    flog = open('log.txt', 'a+')
    flog.write(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " " + str(msg) + "\n")
    flog.close()


def variables_map(input_str):
    try:
        config = {"0": "0"}
        input_str = str(input_str).replace("{", "").replace("}", "").split(",")
        for v in input_str:
            v1 = v.strip().split("=", 1)
            config[v1[0]] = v1[1]
        return config
    except Exception as ex:
        msg = "%r error parse: " % input_str
        raise argparse.ArgumentTypeError(msg + str(ex))


def find_in_steps(arr, v):
    for el in arr:
        str_el = ""
        for char in el:
            str_el += char
        if str_el == v:
            return True
    return False


def indent(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def create_dir(parent, name):
    # p("parent:" + parent)
    if name not in os.listdir(parent):
        os.mkdir(parent + os.sep + name)
        os.chmod(parent + os.sep + name, 0o744)


def find_file_in_dir(r, start, end):
    for dir_name, dir_names, file_names in os.walk(r):
        for f in file_names:
            if str(f).startswith(start) and str(f).endswith(end):
                return f
    return ''


def find_file_in_dir2(r, word):
    for dir_name, dir_names, file_names in os.walk(r):
        for f in file_names:
            if f == word:
                return dir_name + os.sep + f
    return ""


def find_dir_in_dir(r, start, end, deep):
    for dir_name, dir_names, file_names in os.walk(r):
        for d in dir_names:
            if str(d).startswith(start) and str(d).endswith(end):
                return d
        if not deep:
            break
    return ''


def get_path_dir(r, name):
    path = ''
    for dir_name, dir_names, file_names in os.walk(r):
        for d in dir_names:
            if d == name:
                return dir_name
    return ''


def check_port(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((host, int(port)))
    if verbose:
        if result != 0:
            p("check port " + str(host) + ":" + str(port) + " = OK")
        else:
            p("check port " + str(host) + ":" + str(port) + " = BUSY")

    if result == 0:
        return True
    else:
        return False


def create_port(def_ports, count, host, start_count):
    new_ports = copy.deepcopy(def_ports)
    tmp_dict = OrderedDict()
    search = True
    count += start_count - 1
    while search:

        for port in def_ports:
            if len(def_ports[port]) < 4:
                new_port = str(count) + def_ports[port]
            else:
                new_port = def_ports[port]
            if check_port(host, new_port):
                if len(def_ports[port]) >= 4:
                    p("Port " + new_port + " not reachable! Exit...")
                    exit(-1)
                p("Port " + new_port + " not reachable! Select next count")
                count += 1
                search = True
                tmp_dict = OrderedDict()
                break
            tmp_dict[port] = new_port
            search = False
            if verbose:
                p(port + ":" + tmp_dict[port])

    for k in tmp_dict:
        new_ports[k] = tmp_dict[k]

    return new_ports


def get_key(elem):
    return len(elem)


def xml_or_json(f, name):
    if str(name).endswith(".xml"):
        return xml_to_map(ET.parse(f).getroot())
    if str(name).endswith(".json"):
        return json.load(f)


def xml_to_map(elem):
    map = OrderedDict()
    tag_names = list()
    elem_map = dict()
    for elem in elem:
        if len(elem):  # type(elem) is not str and
            map[elem.tag] = xml_to_map(elem)
        else:
            if elem.tag == "map" and elem.get('xpath'):
                map[elem.get('xpath')] = elem.text
            elif elem.tag == "jvm_opt" and elem.get('name'):
                map[elem.get('name')] = elem.text
            else:
                tag_names.append(elem.tag)
                elem_map[elem.tag] = elem.text
                # map[elem.tag] = elem.text
    tag_names.sort()
    for tag in tag_names:
        if (elem.tag == "map" and elem.get('xpath')) or (elem.tag == "jvm_opt" and elem.get('name')):
            continue
        map[tag] = elem_map[tag]
    return map


def found_xml_element(root, e):
    for i in root.findall(e):
        return True
    return False


def create_element(root, e):
    t = str(e).rsplit("/", 1)
    parent_name = t[0]
    e_name = t[1]
    # p("parent_name:" + parent_name)
    if not found_xml_element(root, parent_name):
        create_element(root, parent_name)
    for parent in root.findall(parent_name):
        # p("parent:" + str(parent))
        new_element = ET.Element(e_name)
        parent.append(new_element)
        break


def replace_local(val, inst_conf):
    # p("inst_conf:" + str(inst_conf))
    tag1 = "#@"
    tag2 = "#"
    if tag1 in val and tag2 in val:
        r = val.split(tag1, 1)[1].split(tag2)[0]
        # p("r:" + str(r))
        val = val.replace(tag1 + r + tag2, inst_conf[r])
    if tag1 in val and tag2 in val:
        val = replace_local(val, inst_conf)
    return val


def config_mapping(template_configs, dst_conf_path, inst_conf):
    # p("m:" + str(m) +
    #       " \ntemplate_configs:" + str(template_configs) +
    #       " \ndst_conf_path:" + str(dst_conf_path)
    #       )
    tmp = os.path.dirname(os.path.abspath(__file__)) + os.sep + "tmp"

    # p("template_configs:" + str(template_configs))
    for template in template_configs:
        try:
            template_path = template_configs[template]
        except Exception as e:
            traceback.print_exception(e, "Error in template: " + str(template), sys.exc_traceback)
            continue
        # p("template_path:" + str(template_path))
        template_path = replace_local(template_path, inst_conf)
        # p("replaced template_path:" + str(template_path))
        custom_path = os.sep + 'esb' + os.sep
        if template_path.startswith("["):
            t = template_path.replace("[", "", 1).split("]", 1)
            template_path = t[1]
            custom_path = t[0]

        template_path = template_path.replace("/", os.sep)
        template_name = template_path.split(os.sep)[template_path.count(os.sep)]
        if len(template_path.split(os.sep)) > 2:
            template_dir = template_path.split(os.sep)[template_path.count(os.sep) - 1]
            dst = dst_conf_path + os.sep + template_dir
            create_dir(tmp, template_dir)
            template_tmp_path = tmp + os.sep + template_dir + os.sep + template_name
            tmp_path = tmp + os.sep + template_dir
        else:
            dst = custom_path
            template_tmp_path = tmp + os.sep + template_name
            tmp_path = tmp

        shutil.copy2(inst_conf['dir_distribs'] + os.sep + template_path, tmp_path)

        tree = ET.parse(template_tmp_path)
        r = tree.getroot()

        m = OrderedDict()
        # p("inst_conf:" + str(inst_conf))
        for c in inst_conf:
            # p("c:" + str(c) + ' : ' + str(inst_conf[c]))
            if str(c).startswith("@") and "/" in c:
                t = str(c).replace("@", "", 1).split("/", 1)
                td = OrderedDict()
                td["./" + t[1]] = inst_conf[c]
                if t[0] not in m:
                    lt = list()
                    lt.append(td)
                    m[t[0]] = lt
                else:
                    m[t[0]].append(td)

        if template in m:
            if verbose:
                p(template + ":" + str(m[template]))
            for arr_key_xpath in m[template]:
                for key_xpath in arr_key_xpath:
                    p("key_xpath:" + str(key_xpath))
                    p("arr_key_xpath:" + str(arr_key_xpath))
                    p("m[template]:" + str(m[template]))
                    val = ""
                    for k in arr_key_xpath:
                        val = arr_key_xpath[k]
                        key_xpath = k
                        break
                    p("key_xpath1:" + str(key_xpath))
                    if str(key_xpath).replace(' ', '').replace('\n', '') == '':
                        continue

                    # tt = m[template][arr_key_xpath]
                    # p("tt:" + str(tt))
                    # val = str(tt[key_xpath])
                    val = replace_local(val, inst_conf)
                    attribs = list()

                    p("val:" + str(val))
                    if val.startswith("["):
                        t = val.replace("[", "").split("]")
                        val = t[1]
                        attribs = t[0].split(",")

                    if not found_xml_element(r, key_xpath):
                        try:
                            create_element(r, key_xpath)
                        except Exception:
                            p("Error create element:" + str(r) + " key_xpath:" + str(key_xpath))

                    for item in r.findall(key_xpath):
                        try:
                            for attrib in attribs:
                                a = attrib.split("=")
                                item.set(a[0], a[1])
                        except Exception:
                            p("Error with key_xpath:" + key_xpath)
                        item.text = val
        indent(r)
        tree.write(template_tmp_path)
        shutil.copy2(template_tmp_path, dst)
        os.remove(template_tmp_path)
    return ''


def list_to_json(l):
    l = list(l)
    j = {}
    for e in l:
        if verbose:
            p(e)
        if type(e) is l and len(e):
            j[e] = list_to_json(e)
        else:
            j[e] = l
    return j


def get_artifact(root, name, version):
    if version is None:
        get_last = True
    else:
        get_last = False
    for data in root:
        if data.tag == 'data':
            for artifact in data:
                artifact_dict = OrderedDict()
                for elem in artifact:
                    artifact_dict[elem.tag] = elem.text
                # p('artifact_dict:' + str(artifact_dict))
                if artifact_dict['artifactId'] != name:
                    continue
                if get_last:
                    if artifact_dict['version'] == artifact_dict['latestRelease']:
                        return artifact_dict
                elif artifact_dict['version'] == version:
                    return artifact_dict
            return None


# def get_classifier(root, name, version):
#     for artifact in root.findall("./data/artifact"):
#         for artifactId in artifact.findall("artifactId"):
#             for v in artifact.findall("version"):
#                 if name in artifactId.text and version == v:
#                     for classifier in \
#                             artifact.findall("./artifactHits/artifactHit/artifactLinks/artifactLink/classifier"):
#                         if classifier.text != "resource":
#                             return artifact
#     return None


def get_classifier(root, name, version):
    artifact_wrapper = OrderedDict()
    for artifact in root.findall("./data/artifact"):
        for artifactId in artifact.findall("./artifactId"):
            if name in artifactId.text:

                for artifactLink in artifact.findall("./artifactHits/artifactHit/artifactLinks/artifactLink"):
                    for classifier in artifactLink.findall("./classifier"):
                        if classifier.text == "resource":
                            for v in artifact.findall("./version"):
                                if version is None or v.text == version:
                                    artifact_wrapper['version'] = v.text
                                else:
                                    return None
                            artifact_wrapper['artifactId'] = artifactId.text
                            for extension in artifactLink.findall("./extension"):
                                artifact_wrapper['extension'] = extension.text
                            return artifact_wrapper
    return None


def get_extension(root, name):
    for artifact in root.findall("./data/artifact"):
        for artifactId in artifact.findall("artifactId"):
            if artifactId.text == name:
                for extension in artifact.findall("./artifactHits/artifactHit/artifactLinks/artifactLink/extension"):
                    if extension.text != "pom":
                        return extension.text
    return "ear"


# def download(download_url, ear_path, ear_name):
#     try:
#         u = urllib2.urlopen(download_url)
#         f = open(ear_path, 'wb')
#         meta = u.info()
#         file_size = int(meta.getheaders("Content-Length")[0])
#         if file_size > 0:
#             print("Downloading: %s Bytes: %s" % (ear_name, file_size))
#         else:
#             p("ERROR DOWNLOADING ARTIFACT " + ear_name)
#
#         file_size_dl = 0
#         block_sz = 1024 * 1024
#         while True:
#             buffer = u.read(block_sz)
#             if not buffer:
#                 break
#
#             file_size_dl += len(buffer)
#             f.write(buffer)
#             status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
#             status += chr(8) * (len(status) + 1)
#             if verbose:
#                 p(status)
#
#         f.close()
#         return True
#     except Exception as e:
#         pex(e, "Error download " + download_url)
#         return False

#
# def get_resource(artifact_wrapper, download_url):
#     extension = artifact_wrapper["extension"]
#     artifactId = artifact_wrapper["artifactId"]
#     version = artifact_wrapper["version"]
#     ear_name_resource = artifactId + '-' + version + '-resource.' + extension
#     ear_path_resource = root + os.sep + 'tmp' + os.sep + ear_name_resource
#     if os.path.exists(ear_path_resource):
#         if verbose:
#             p("File exist! " + ear_path_resource)
#     else:
#         download_resource_url = download_url
#         download_resource_url += artifactId + '/' + version + '/' + ear_name_resource
#         p(download_resource_url)
#         if not download(download_resource_url, ear_path_resource, ear_name_resource):
#             ear_path_resource = None
#         else:
#             if not find_in_zip(ear_path_resource, "resource-config.json"):
#                 ear_path_resource = None
#
#     if ear_path_resource is not None:
#         if not find_in_zip(ear_path_resource, "resource-config.json"):
#             ear_path_resource = None
#     return ear_path_resource
#
#
# def download_artifact(name, version, host):
#     req = '/nexus/service/local/lucene/search?q=' + name
#     connection = httplib.HTTPConnection(host)
#     connection.request("GET", req)
#     res = connection.getresponse().read()
#     res_root = ET.fromstring(res)
#     artifact = get_artifact(res_root, name, version)
#     tag = get_extension(res_root, name)
#     p("tag:" + tag)
#     if artifact is None:
#         return None
#     if verbose:
#         p(str(artifact))
#
#     download_url = 'http://' + host + '/nexus/service/local/repositories/releases/content/'
#     for c in str(artifact['groupId']).split('.'):
#         download_url += c + '/'
#
#     if tag != "":
#         ear_name = artifact['artifactId'] + '-' + artifact['version'] + '.' + tag
#     else:
#         ear_name = artifact['artifactId'] + '-' + artifact['version'] + '.ear'
#     create_dir(root, "tmp")
#     ear_path = root + os.sep + 'tmp' + os.sep + ear_name
#
#     if os.path.exists(ear_path):
#         if verbose:
#             p("File exist! " + ear_path)
#     else:
#         download_url_app = download_url + artifact['artifactId'] + '/' + artifact['version'] + '/' + ear_name
#         if verbose:
#             p(download_url_app)
#         download(download_url_app, ear_path, ear_name)
#
#     if find_in_zip(ear_path, "resource-config.json"):
#         ear_path_resource = ear_path
#         p("Find resource-config.json in main app.")
#     else:
#         try:
#             resource_name = str(name).split("-")[0]
#         except:
#             resource_name = name
#             p("Cant split resource name, use " + resource_name)
#         req = '/nexus/service/local/lucene/search?q=' + resource_name
#         connection = httplib.HTTPConnection(host)
#         connection.request("GET", req)
#         res = connection.getresponse().read()
#         res_root = ET.fromstring(res)
#         artifact_wrapper = get_classifier(res_root, resource_name, version)
#         if artifact_wrapper is not None:
#             ear_path_resource = get_resource(artifact_wrapper, download_url)
#             if not find_in_zip(ear_path_resource, "resource-config.json"):
#                 p("Resource in " + ear_path_resource + " not found!")
#                 ear_path_resource = None
#         else:
#             p("Resource file not found!")
#             ear_path_resource = None
#     wrapper = OrderedDict()
#     wrapper['ear_path'] = ear_path
#     wrapper['ear_path_resource'] = ear_path_resource
#     return wrapper
#

def install_ear(asadmin_path, target, ear_path):
    os.system(asadmin_path + " deploy --target " + target + " " + ear_path)


def prepare_jvm_opt(s):
    return s.replace(':', '\:')
    # if os.name == "nt":
    #     return s.replace(':', '\:')
    # if os.name == "posix":
    #     return s


def start_domain(cmd_asadmin):
    # res = subprocess.check_output(cmd_asadmin + "start-domain domain1", shell=True)
    if verbose:
        p("Start domain...")
    res = run_command(cmd_asadmin + " start-domain domain1")
    # if verbose:
    #     p("res:" + str(res))
    if find_in_array(res, "There is a process already using the admin port") \
            or find_in_array(res, "Successfully started the domain"):
        if verbose:
            p("Start domain successful!")
        return True
    if find_in_array(res, "Command start-instance failed."):
        p("Command start-instance failed.")
        print_res(res)
        start_domain(cmd_asadmin)
    if find_in_array(res, "Error starting domain domain1."):
        p("ERROR! Command start-instance critical error.")
        exit(-1)


def stop_domain(cmd_asadmin, domain_path):
    # res = subprocess.check_output(cmd_asadmin + "stop-domain domain1", shell=True)
    if verbose:
        p("Stop domain...")
    res = run_command(cmd_asadmin + " stop-domain domain1")
    # if verbose:
    #     p("res:" + str(res))
    if find_in_array(res, "is not running") or find_in_array(res, "Command stop-domain executed successfully."):
        if verbose:
            p("Stop domain successful!")
    if find_in_array(res, "Command stop-instance failed.") or find_in_array(res, "remote failure"):
        p("Command stop-instance failed. ")
        print_res(res)
        stop_domain(cmd_asadmin)

    kill_proc("Dcom.sun.aas.instanceRoot=" + str(domain_path).replace("//", "/").replace("\\\\", "\\"))
    return True


def stop_instance(cmd_asadmin, name):
    if verbose:
        p("Stop instance " + name + "...")
    res = run_command(cmd_asadmin + " stop-instance " + name)
    if os.name != "nt":
        kill_proc(name)
    return True


def run_command(cmd):
    res = ""
    if verbose:
        p("Command:" + cmd)
    try:
        if os.name == "nt":
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= 1
            res = subprocess.Popen(cmd,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   stdin=subprocess.PIPE,
                                   shell=True,
                                   startupinfo=startupinfo).communicate()
        if os.name == "posix":
            res = subprocess.Popen(cmd,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   stdin=subprocess.PIPE,
                                   shell=True
                                   ).communicate()

        if find_in_array(res, "Is the server up?") \
                or find_in_array(res, "Command create-cluster failed.") \
                or find_in_array(res, "Command create-node-ssh failed.") \
                or find_in_array(res, "Command create-instance failed."):
            p("Critical output, execution failed!")
            if verbose:
                print_res(res)
            exit(-1)

    except Exception as e:
        p("Error run_command: " + cmd)
        p("Error: " + str(e))
        exit(-1)
    if verbose:
        print_res(res)

    return res


def print_res(res):
    for r in res:
        if trimstr(r) != "":
            p("   Command Out:")
            for line in r.splitlines():
                p("      " + str(line))


def kill_proc(name):
    if args.das_host != "localhost" or os.name == "nt":
        return
    if os.name == "nt":
        res = run_command("taskkill /IM java.exe /f")
        if verbose:
            p("Kill all old java")
    if os.name == "posix":
        res = run_command("ps -ef | grep " + name + " | grep -v grep | awk '{print $2}' | xargs -r kill -9")
        print_res(res)


def find_in_array(arr, key):
    for elem in arr:
        if elem is not None and key in str(elem):
            return True
    return False


def chmod_to_dir(path, mod):
    for root, dirs, files in os.walk(path):
        for momo in dirs:
            os.chmod(os.path.join(root, momo), mod)
        for momo in files:
            os.chmod(os.path.join(root, momo), mod)


def log(msg):
    if verbose:
        p(">>>" + str(msg).strip() + "<<<")


def create_passfile(file_name, pas, new):
    f = open(root + os.sep + file_name, 'w+')
    f.write("AS_ADMIN_ADMINPASSWORD=" + pas + "\n")
    f.write("AS_ADMIN_SSHPASSWORD=" + pas + "\n")
    # f.write("AS_ADMIN_USERPASSWORD=" + pas + "\n")
    # f.write("AS_ADMIN_MASTERPASSWORD=" + pas + "\n")
    f.write("AS_ADMIN_PASSWORD=" + pas + "\n")
    # if new:
    #     f.write("AS_ADMIN_NEWPASSWORD=" + pas + "\n")
    #     f.write("AS_ADMIN_PASSWORD=" + "\n")
    # else:
    #     f.write("AS_ADMIN_PASSWORD=" + pas + "\n")
    f.close()


def mapping_db_pass(val):
    arr = str(val).split(":")
    user = ""
    for prop in arr:
        if "User=" in prop:
            user = prop.replace("User=", "")
            user = trimstr(user)
    if user != "":
        try:
            val = val.replace("Password=*****", "Password=" + db_pass[user])
            p("Password for user " + str(user) + " replaced")
        except Exception as e:
            p("Not found user in db_pass:" + str(db_pass))
            traceback.print_exception(e, "Error find pass for user: " + user, sys.exc_traceback)
    return val


def asadmin_cmd(asadmin_command, config_install, asadmin_commands, cl_conf, cmd_asadmin, recursion):
    try:
        ascmds = asadmin_commands[asadmin_command]
    except:
        ascmds = None
    try:
        if ascmds is None:
            ascmds = config_install['asadmin_commands'][asadmin_command]
            if ascmds is None:
                p("Not found cmd:" + str(asadmin_command))
    except Exception:
        p("Error links by asadmin_commands key:" + asadmin_command)

    try:
        for el in ascmds:
            if el == "connectionpoolid" or el == "poolname" and not recursion:
                if verbose:
                    print("---------RECURSION_ASADMIN---------------el:" + el + ":" + ascmds[el])
                asadmin_cmd(str(ascmds[el]).replace("/", "_"), config_install, asadmin_commands, cl_conf, cmd_asadmin,
                            True)
                if verbose:
                    print("---------RECURSION_ASADMIN---------------end")
    except:
        p("Error asadmin command:" + str(ascmds))
        return

    cmd = ' '
    command = ''
    name = ''
    target = ""
    for ascmd in ascmds:
        # if args.verbose:
        # cf.p(ascmd + ":" + str(ascmds[ascmd]))
        val = replace_local(ascmds[ascmd], cl_conf)
        if ascmd == "property" and "Password=" in val:
            val = mapping_db_pass(val)
        if ascmd == "target":
            target = val
        if ascmd == 'asadmin_command':
            command = val
        elif ascmd == 'asadmin_command_name':
            name = val
        else:
            cmd += '--' + ascmd + ' ' + str(val).replace(' ', '').replace('\n', '') + ' '

    cmd = command + ' ' + cmd + ' ' + name
    res = run_command(cmd_asadmin + ' ' + cmd)

    for r in res:
        if trimstr(r) != "":
            for line in r.splitlines():
                if "please use create-resource-ref" in line:
                    # create-resource-ref --target Cluster1 jms/Topic
                    p("Resource exist, add cluster in target.")
                    run_command(cmd_asadmin + ' create-resource-ref --target ' + target + " " + name)


import json
import logging
import sys
import traceback

import numpy
import pandas
import requests

log = logging.getLogger(__name__)


def get(url):
    r = requests.get(url)
    return json.loads(r.text)


def calculate(arr, key):
    result = 0.0
    for order in arr:
        result += order[key]
    return result


def pex(e, msg):
    traceback.print_exception(e, "\n" + str(msg), sys.exc_traceback)


def calc_indicators(candles):
    K_depth = 3
    D_depth = 3
    rsi_depth = 14
    stochastic_depth = 14
    ewma_min_periods = 0
    ewma_span = 11

    if len(candles["date"]) < rsi_depth * 4 + 3:
        log.error("Too small candles!")
        return None

    candles = candles.tail(rsi_depth * 4 + 2)
    length = len(candles['date'])
    candles = candles.assign(U=pandas.Series(numpy.zeros(length)).values)
    candles = candles.assign(D=pandas.Series(numpy.zeros(length)).values)
    i = 0
    while i <= rsi_depth * 4:
        i += 1
        ii = i + 1
        U = candles.iloc[-i]["close"] - candles.iloc[-ii]["close"]
        D = candles.iloc[-ii]["close"] - candles.iloc[-i]["close"]

        if U > 0:
            D = 0
        elif D > 0:
            U = 0
        else:
            U = 0
            D = 0
        candles.set_value(candles.iloc[-i].name, "U", U)
        candles.set_value(candles.iloc[-i].name, "D", D)

    candles = candles.assign(EMAU=candles.get("U").ewm(com=ewma_span, min_periods=ewma_min_periods).mean())
    candles = candles.assign(EMAD=candles.get("D").ewm(com=ewma_span, min_periods=ewma_min_periods).mean())
    i = 0
    while i <= rsi_depth * 2:
        i += 1
        df = candles.iloc[-i]
        r = 100. * (df["EMAU"] / (df["EMAU"] + df["EMAD"]))
        candles.set_value(candles.iloc[-i].name, "RSI", r)

    i = 0
    while i <= stochastic_depth:
        i += 1
        rsi = candles.tail(stochastic_depth + i - 1).head(stochastic_depth)
        current_rsi = rsi.iloc[-1]["RSI"]
        K = (current_rsi - rsi["RSI"].min()) / (rsi["RSI"].max() - rsi["RSI"].min())
        candles.set_value(rsi.iloc[-1].name, "FASTK", K * 100)

    candles = candles.assign(FASTD=candles["FASTK"].rolling(3).mean())
    candles = candles.assign(SLOWK=candles["FASTK"].rolling(3).mean())
    candles = candles.assign(SLOWD=candles["SLOWK"].rolling(3).mean())
    return candles


def print_current(candles, n):
    i = 0
    while i <= n:
        i += 1
        log.info(str(candles.iloc[-i]["date"]) +
                 "  " + str(round(candles.iloc[-i]["close"], 2)) +
                 "  " + str(round(candles.iloc[-i]["RSI"], 2)) +
                 "  " + str(round(candles.iloc[-i]["SLOWK"], 2)) +
                 "  " + str(round(candles.iloc[-i]["SLOWD"], 2))
                 )
