import logging.config
import os
import time

from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker

from mappers.bitfinexmapper import BitfinexMapper
from mappers.bittrexmapper import BittrexMapper
from utils import configloader

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)


class BookDownload:
    run = True
    engine = None
    loop_delay = None
    precision = None
    symbol = None
    length = None
    price_skip = None
    Session = None
    bm = BitfinexMapper("", "")
    btm = BittrexMapper("", "")

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=engine)

        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.loop_delay = int(config["loop_delay"])
        self.precision = config["precision"]
        self.symbol = config["symbol"]
        self.length = config["length"]
        self.price_skip = config["price_skip"]

    def start(self):
        while self.run:
            self.processing()
            # self.run = False
            time.sleep(self.loop_delay)

    def processing(self):
        last_price = self.bm.get_tick(self.symbol).last
        books = self.bm.get_books(self.symbol, self.precision, self.length)
        # last_price = books[0].price

        sell = 0
        sell_count = 0
        buy = 0
        buy_count = 0
        all = 0
        for book in books:
            if abs(book.price - last_price) > last_price * 0.01 * self.price_skip:
                continue
            # log.info(book)
            all += book.amount
            if book.amount > 0:
                buy += book.amount
                buy_count += book.count
            else:
                sell += book.amount
                sell_count += book.count

        all_perc = int((all / (buy + abs(sell))) * 100)
        sell = round(sell, 2)
        buy = round(buy, 2)
        all = round(all, 2)

        log.info("%s | buy: %s/%s | sell: %s/%s | all: %s/%s"
                 % (last_price, buy, buy_count, sell, sell_count, all, all_perc))


engine = engine_from_config(configloader.get_config("db"), prefix='db.')
logging.config.dictConfig(configloader.get_config_log())
log.info("----------Start-------------")
obj = BookDownload(engine)
obj.start()
log.info("----------End-------------")
