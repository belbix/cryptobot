import logging
import os

from sqlalchemy.orm import sessionmaker

from entity import order
from mappers.bitfinexmapper import BitfinexMapper
from mappers.bitmexmapper import BitmexMapper
from mappers.bittrexmapper import BittrexMapper
from utils import configloader
from utils import emailsender

log = logging.getLogger(__name__)
logging.getLogger("bitmex.market_maker.ws.ws_thread").setLevel(logging.INFO)


class OrderCreator:
    engine = None
    Session = None
    amount_percent = None
    servers = None
    mappers = []
    min_amount = 0
    es = emailsender.EmailSender()

    def __init__(self, engine, skipws=True):
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.amount_percent = config["amount_percent"]
        self.leverage = config["leverage"]
        self.servers = config["servers"]
        self.order_type = config["order_type"]
        self.proxy = config["proxy"]
        self.postOnly = config["postOnly"]
        self.change_price = config["change_price"]
        self.perc_recreate = config["perc_recreate"]

        self.mappers.append(
            BitfinexMapper(
                self.servers[BitfinexMapper.SERVER_NAME]["api_key"],
                self.servers[BitfinexMapper.SERVER_NAME]["api_secret"])
        )
        self.mappers.append(
            BittrexMapper(
                self.servers[BittrexMapper.SERVER_NAME]["api_key"],
                self.servers[BittrexMapper.SERVER_NAME]["api_secret"])
        )

        self.mappers.append(
            BitmexMapper(
                self.servers[BitmexMapper.SERVER_NAME]["api_key"],
                self.servers[BitmexMapper.SERVER_NAME]["api_secret"],
                self.servers[BitmexMapper.SERVER_NAME]["server"],
                proxy=self.proxy,
                skipws=skipws,
                postOnly=self.postOnly)
        )

        bitmex_test = BitmexMapper(
            self.servers[BitmexMapper.SERVER_NAME + "test"]["api_key"],
            self.servers[BitmexMapper.SERVER_NAME + "test"]["api_secret"],
            self.servers[BitmexMapper.SERVER_NAME + "test"]["server"],
            proxy=self.proxy,
            skipws=skipws,
            postOnly=self.postOnly)
        bitmex_test.SERVER_NAME = "bitmextest"
        self.mappers.append(bitmex_test)

        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)
        order.Base.metadata.create_all(engine)

    def create_order(self, charge, amount=0, instant=False):
        log.info("create order from %s" % charge)
        symbol = charge.symbol
        type = charge.type
        ids = []
        skip = True
        for mapper in self.mappers:
            for server in self.servers:
                if bool(self.servers[server]["active"]) and server == mapper.SERVER_NAME:
                    skip = False
                    break
                else:
                    skip = True

            if skip:
                continue

            log.info("Server: " + mapper.SERVER_NAME)
            symbol = mapper.validate_symbol(symbol)

            # mapper.test = True
            position_amount = 0
            balance = 0
            if charge.close_position == 0:
                currency = get_currency(symbol, type, charge.close_position)
                balance = mapper.get_balance(currency)
                # if mapper.SERVER_NAME.startswith("bitmex"):
                #     balance *= self.leverage

                if balance is None:
                    log.warning("Error get balance!")
                    continue
                log.info(currency + " Balance: " + str(balance))
                if (amount != 0 and balance < amount) or (not mapper.test and balance == 0):
                    log.warning("Not enough balance!")
                    continue

            o = None
            last_price = 0
            try:
                last_price = mapper.get_tick(symbol).last
            except Exception as e:
                log.exception("%s | %s" % (e, symbol))
            first_last_price = last_price
            # if mapper.SERVER_NAME != "bitmex":
            #     if amount == 0:
            #         amount = self.get_amount(balance, last_price, charge, mapper.SERVER_NAME)
            #         log.info("set amount %s" % amount)
            #     log.info("Last price: %s | amount: %s" % (last_price, amount))
            #     o = mapper.create_order(amount, last_price, type, symbol)
            # else:
            create_count = 0
            while o is None:
                create_count += 1

                position_amount = 0
                try:
                    position_amount = mapper.get_position_amount(symbol)
                    log.info("position: %s" % position_amount)
                    if charge.close_position == 0:
                        log.info("isolate_margin: %s" % mapper.isolate_margin(symbol, self.leverage))
                except Exception as e:
                    log.exception("%s" % e)
                mapper.order_type = self.order_type
                # if mapper.SERVER_NAME.startswith("bitmex"):
                #     if position_amount != 0 and charge.close_position == 1:
                #         amount = position_amount
                #     else:
                #         amount = self.get_amount(balance, last_price, charge, mapper.SERVER_NAME)
                # else:
                if position_amount and position_amount != 0 and charge.close_position == 1:
                    amount = position_amount
                else:
                    amount = self.get_amount(balance, last_price, charge, mapper.SERVER_NAME)
                log.info("Last price: %s | amount: %s" % (last_price, amount))
                if self.change_price != 0:
                    if type == 0:
                        last_price -= self.change_price
                    elif type == 1:
                        last_price += self.change_price
                try:
                    o = mapper.create_order(amount, last_price, type, symbol, instant=instant)
                except Exception as e:
                    log.exception("%s" % e)
                if o is None:
                    if create_count >= 100 or (
                            abs(first_last_price - last_price) / last_price) > 0.005:  # or create_count > 20:
                        log.warning("Instant order!")
                        o = mapper.create_order(amount, last_price, type, symbol, instant=True)
                        break
                last_price = mapper.get_tick(symbol).last

            if o is None:
                continue

            o.charge_id = charge.id
            log.info("ORDER CREATED!!! " + str(o))
            id = 0

            try:
                balance = mapper.get_balance(get_currency(symbol, type, charge.close_position))
            except Exception as e:
                log.exception("%s" % e)
            session = None
            try:
                session = self.Session()
                session.add(o)
                session.commit()
                id = o.id

                if o.type == 1:
                    buy_sell = "SELL"
                else:
                    buy_sell = "BUY"

                if charge.close_position == 0:
                    position_create = "CREATE"
                else:
                    position_create = "CLOSE"
                email = ""
                email += "Balance: %s" % balance + " \n"
                email += "Amount: %s" % position_amount + " \n"
                email += "Order created: %s" % o
                self.es.send_email(email, position_create + " " + buy_sell + " " + o.server)
            except:
                log.exception("error add order")
                if session:
                    session.rollback()
                    session.close()
                    session = self.Session()
            finally:
                session.close()
            ids.append(id)
        return ids

    def check_price_order(self, order):
        for mapper in self.mappers:
            if mapper.SERVER_NAME.startswith("bitmex"):
                last_price = mapper.get_tick(order.symbol).last
                if (abs(last_price - order.price) / last_price) * 100 <= self.perc_recreate:
                    return True
                else:
                    return False
        return False

    def get_buys(self, symbol):
        buys = 0
        for mapper in self.mappers:
            for server in self.servers:
                if bool(self.servers[server]["active"]) and mapper.SERVER_NAME == server:
                    if server.startswith("bitmex") or server == "bitfinex":
                        continue
                    b = mapper.get_balance(get_currency(symbol, 0, 0))
                    s = mapper.get_balance(get_currency(symbol, 1, 1))
                    if s is None:
                        s = 0

                    price = mapper.get_tick(symbol).last
                    bs = s * price
                    log.info("%s | buy: %s | sell: %s" % (price, b, bs))
                    if bs == 0 or (b / bs) * 100 < 10:
                        if bs == 0:
                            log.warning("Buys = 0, set buy: %s" % s)
                        else:
                            log.warning("ALREADY BUYS!!! set to buy: %s" % s)
                        buys += s
        return buys

    def check_order(self, order):
        for mapper in self.mappers:
            if mapper.SERVER_NAME == order.server:
                return mapper.check_open_order(order)
        return False

    def get_amount(self, balance, last_price, charge, server):
        log.info("get_amount balance %s, last_price %s " % (balance, last_price))
        if charge.close_position == 1:
            return 0
        if server.startswith("bitmex"):
            amount = balance * last_price
        else:
            amount = balance / last_price

        amount *= self.amount_percent
        if server.startswith("bitmex"):
            amount *= self.leverage
        return amount


def get_currency(symbol, type, close_position):
    try:
        s = ""
        symbol = symbol.replace("-", "")
        if type == 0:
            if close_position == 0:
                s = symbol[3] + symbol[4] + symbol[5]
            else:
                s = symbol[0] + symbol[1] + symbol[2]
        elif type == 1:
            if close_position == 1:
                s = symbol[0] + symbol[1] + symbol[2]
            else:
                s = symbol[3] + symbol[4] + symbol[5]
        return s
    except Exception as e:
        log.exception("get_currency error %s | %s" % (symbol, type))
        raise e
