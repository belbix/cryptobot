import logging.config
import threading

import os
import time
from datetime import datetime, timedelta

from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker

from entity import order, charge
from entity.charge import Charge
from entity.order import Order
from utils import configloader
from utils.ordercreator import OrderCreator

log = logging.getLogger(__name__)


class Trader:
    engine = None
    run = True
    loop_delay = None
    Session = None
    oc = None
    charge_delay = 0
    order_delay = 0
    worked_orders = []

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.loop_delay = config["loop_delay"]
        self.charge_delay = config["charge_delay"]
        self.order_delay = config["order_delay"]
        self.proxy = config["proxy"]
        order.Base.metadata.create_all(engine)
        charge.Base.metadata.create_all(engine)
        self.oc = OrderCreator(engine)

    def start(self):
        log.info("----------Start chargereader-------------")
        try:
            while self.run:
                self.processing()
                time.sleep(self.loop_delay)
                # self.run = False
        finally:
            log.info("----------End chargereader-------------")

    def processing(self):
        session = self.Session()
        try:
            self.recreate_orders(session)
            charges = self.get_charges(session)
            if charges is None or len(charges) == 0:
                log.debug("Charge not found!")
                return None

            for charge in charges:
                log.debug(charge)
                if self.check_orders(charge, session):
                    log.info("Try create order...")
                    try:
                        self.oc.create_order(charge)
                        # t = threading.Thread(target=self.oc.create_order, args=(charge,))
                        # t.daemon = True
                        # t.start()
                    except:
                        log.exception("create order error")
        except:
            log.exception("main loop error")
        finally:
            session.close()

    def get_charges(self, session):
        charges = None
        try:
            charges = session.query(Charge) \
                .filter(Charge.strategy_id > 0) \
                .filter(Charge.date_create > datetime.utcnow() - timedelta(seconds=self.charge_delay)).all()
        except:
            log.exception("error in get charges")

        return charges

    def check_orders(self, charge, session):
        try:
            orders = session.query(Order) \
                .filter(Order.charge_id == charge.id).all()
            if len(orders) == 0:
                log.info("New charge! %s" % charge)
                return True
            else:
                return False
        except:
            log.exception("error check orders")

    def recreate_orders(self, session):
        try:
            unchecked_orders = session.query(Order) \
                .filter(Order.date_create < datetime.utcnow() - timedelta(seconds=self.order_delay)) \
                .filter(Order.status == 0) \
                .all()
            for o in unchecked_orders:
                log.info("Check order: %s" % o)
                if not self.oc.check_order(o):  # if not open
                    log.info("Order set status complete %s" % o)
                    o.status = 1
                    # session.add(o)
                else:  # recreate open order
                    if self.oc.check_price_order(o):  # skip if price not change
                        log.info("Skip recreate order because price not change")
                        continue

                    if not self.oc.cancel_order(o):
                        log.warning("Error canceling order, skip recreate %s" % o)
                        continue
                    log.warning("Order recreate! %s" % o)
                    charge = session.query(Charge).filter(Charge.id == o.charge_id).first()
                    if charge is not None:
                        self.oc.create_order(charge, instant=True)
                        # t = threading.Thread(target=self.oc.create_order, args=(charge,))
                        # t.daemon = True
                        # t.start()
                    session.delete(o)
            session.commit()
        except:
            session.rollback()
            log.exception("error recreate orders")


# logging.config.dictConfig(configloader.get_config_log())
# cr = Buyer(engine=engine_from_config(configloader.get_config("db"), prefix='db.'))
# cr.start()
