import datetime
import logging
from time import sleep

from ibapi.client import EClient
from ibapi.contract import Contract
from ibapi.wrapper import EWrapper
from mappers.ibmapper import IBMapper

log = logging.getLogger(__name__)


class TestIB(EClient, EWrapper):
    def __init__(self, engine):
        self.engine = engine
        EClient.__init__(self, self)
        self.nextValidOrderId = None
        self.permId2ord = {}

    def start(self):
        log.info("Start TestIB")
        ib = IBMapper("", "")


        sleep(2)

        # ib.req_current_time()
        # ib.reqManagedAccts()
        ib.reqAccountSummary(reqId=2, groupName="All", tags="NetLiquidation")
        # ib.reqAllOpenOrders()


        # self.tick(ib)
        # self.hist(ib)


        sleep(3)

        ib.disconnect()

        log.info("Stop main thread TEST")

    def tick(self, ib):
        contract = Contract()
        contract.symbol = "EUR"
        contract.secType = "CASH"
        contract.currency = "USD"
        contract.exchange = "IDEALPRO"
        ib.reqMktData(ib.nextOrderId(), contract, genericTickList="", snapshot=False,
                      regulatorySnapshot=False, mktDataOptions=[])

    def hist(self, ib):
        start = (datetime.datetime.now() - datetime.timedelta(minutes=1))
        start_tt = str(start.timestamp()).split(".")[0] + "000"
        hist = ib.get_history_tick("EUR", 1000, start_tt, "")
        log.info(hist)