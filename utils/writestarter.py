import logging.config
import threading

from sqlalchemy import engine_from_config

from entity import tick, candle as candle
from utils import configloader
from utils.candlewriter import CandleWriter


log = logging.getLogger(__name__)
logging.getLogger("bitmex.market_maker.ws.ws_thread").setLevel(logging.INFO)

class WriteStarter:

    writer = None

    def __init__(self, engine):
        self.engine = engine
        tick.Base.metadata.create_all(engine)
        candle.Base.metadata.create_all(engine)
        self.writer = CandleWriter(engine)

    def start(self):
        threading.Thread(target=self.writer.tick_writer, args=("XBTUSD",)).start()

