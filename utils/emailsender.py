import logging.config
import os
import smtplib

from utils import configloader

log = logging.getLogger(__name__)


class EmailSender:
    loop_delay = None
    server = None
    email = None
    password = None
    to = None

    def __init__(self):
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.active = config["active"]
        self.loop_delay = int(config["loop_delay"])
        self.server = config["server"]
        self.email = config["email"]
        self.password = config["password"]
        self.to = [config["to"]]

    def send_email(self, msg, theme="CryptoBot Notification"):
        if not self.active:
            return
        try:
            msg = """From: %s\nTo: %s\nSubject: %s\n\n%s
            """ % (self.email, ", ".join(self.to), theme, msg)
            log.info(msg)
            server = smtplib.SMTP(self.server)
            server.ehlo()
            server.starttls()
            server.login(self.email, self.password)
            server.sendmail(self.email, [self.to], msg)
            log.info("Email sending!")
            server.quit()

        except:
            log.exception("error send email")

# logging.config.dictConfig(configloader.get_config_log())
# log.info("----------Start-------------")
# obj = Main()
# obj.send_email("hello!")
# log.info("----------End-------------")
