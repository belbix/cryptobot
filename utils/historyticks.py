import importlib
import logging.config
import os
import time
from datetime import datetime, timedelta

import numpy
from matplotlib import pyplot
from sqlalchemy.orm import sessionmaker

from utils import configloader
from utils.candlecreator import CandleCreator
from utils.candlewriter import CandleWriter

log = logging.getLogger(__name__)


def delta_minutes(d1, d2):
    # Convert to Unix timestamp
    d1_ts = time.mktime(d1.timetuple())
    d2_ts = time.mktime(d2.timetuple())
    # They are now in seconds, subtract and then divide by 60 to get minutes.
    return int(d2_ts - d1_ts) / 60


class HistoryTicks:
    run = True
    engine = None
    loop_delay = None
    Session = None
    strategy = None
    sum_profits_arr = []
    profits = []
    deposits = []
    deposit = 0
    sum_profits = 0
    cc = None
    symbol = None
    date_start = None
    timeframe = None
    ticktime = None

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=engine)

        config = self.config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.loop_delay = int(config["loop_delay"])
        self.symbol = config["symbol"]
        self.server = config["server"]
        self.date_start = datetime.strptime(config["date_start"], "%Y-%m-%d %H:%M:%S")
        self.date_stop = datetime.strptime(config["date_stop"], "%Y-%m-%d %H:%M:%S")
        self.timeframe = config["timeframe"]
        self.ticktime = config["ticktime"]
        self.rewrite_candles = config["rewrite_candles"]
        self.plot = config["plot"]
        self.show_tp_ls = config["show_tp_ls"]
        self.leverage = config["leverage"]

        m = importlib.import_module("strategies.%s" % str(config["strategy"]).lower())
        self.strategy = getattr(m, config["strategy"])(engine)

        self.strategy.STRATEGY_ID = -1
        self.strategy.test = True
        self.strategy.symbol = self.symbol
        self.strategy.leverage = self.leverage
        # self.strategy.FEE = 0

        self.cc = CandleCreator(engine)

    def start(self):
        log.info("date start: %s | date stop: %s" % (self.date_start, self.date_stop))
        if self.rewrite_candles:
            cw = CandleWriter(self.engine)
            cw.timeframe = self.timeframe
            cw.symbol = self.symbol
            cw.server = self.server
            delta = delta_minutes(self.date_start, self.date_stop)
            log.info("delta minutes: %s" % (delta))
            cw.rewrite_candle_count = int(delta / self.timeframe) + 70
            cw.candle_writer2(False, self.server)

        if self.date_stop == "":
            self.date_stop = datetime.utcnow()
        try:
            while self.date_start < self.date_stop:
                start = datetime.utcnow()
                self.strategy.metric_from_db = False
                self.strategy.download_candles = 500
                start_c = datetime.utcnow()
                candles = None
                candles_len = 0
                try:
                    candles = self.cc.get_candles(count=self.strategy.download_candles,
                                                  date=self.date_start,
                                                  timeframe=self.timeframe,
                                                  server=self.server,
                                                  symbol=self.symbol,
                                                  first_dynamic=True)
                    if candles is not None:
                        candles_len = len(candles["date"])
                except Exception as e:
                    log.exception("%s" % e)
                log.debug("get candles time %s | %s" % ((datetime.utcnow() - start_c), candles_len))
                # log.info(candles)
                if candles is not None and not candles.empty:
                    try:
                        self.processing(candles)
                    except Exception as e:
                        log.exception("%s" % e)
                        break
                else:
                    log.info(
                        "No candles! %s | %s | %s | %s" % (self.date_start, self.server, self.symbol, self.timeframe))
                self.date_start += timedelta(seconds=self.ticktime)
                log.debug("h proc time %s" % (datetime.utcnow() - start))
                # time.sleep(self.loop_delay)

        finally:
            log.info("Balance history:\n %s" % self.sum_profits_arr)
            if self.plot:
                pyplot.plot(self.deposits)
                pyplot.savefig("images" + os.sep + "H_" + self.symbol + ("_%s" % int(datetime.utcnow().timestamp()))
                           + ".png", dpi=180)
                pyplot.show()

            if len(self.sum_profits_arr) > 0:
                result = ""
                result += "Result:\n"
                result += "  deposit:" + str(numpy.sum(self.strategy.deposit)) + "\n"
                result += "  profits:" + str(numpy.sum(self.profits)) + "\n"
                result += "  max profit:" + str(numpy.amax(self.profits)) + "\n"
                result += "  min profit:" + str(numpy.amin(self.profits)) + "\n"
                result += "  max balance:" + str(numpy.amax(self.deposits)) + "\n"
                result += "  min balance:" + str(numpy.amin(self.deposits)) + "\n"
                result += "  count: " + str(self.strategy.count) + "\n"

                result += " period: %s" % self.timeframe + "\n"
                result += " ticktime: %s" % self.ticktime + "\n"
                result += " date_start: %s" % self.date_start + "\n"
                result += " date_stop: %s" % self.date_stop + "\n"
                result += str(self.config) + "\n"
                result += str(self.strategy.config)
                log.info(result)

            if self.show_tp_ls \
                    and len(self.strategy.price_delta_ups) > 0 \
                    and len(self.strategy.price_delta_downs) > 0:
                log.info("%s" % self.strategy.price_delta_ups)
                log.info("%s" % self.strategy.price_delta_downs)
                log.info("price_delta_ups max: %s min: %s" %
                         (numpy.amax(self.strategy.price_delta_ups), numpy.amin(self.strategy.price_delta_ups)))
                pyplot.plot(self.strategy.price_delta_ups)
                pyplot.show()
                log.info("price_delta_downs max: %s min: %s" %
                         (numpy.amax(self.strategy.price_delta_downs), numpy.amin(self.strategy.price_delta_downs)))
                pyplot.plot(self.strategy.price_delta_downs)
                pyplot.show()

    def processing(self, candles):
        self.strategy.processing(candles)
        self.profits.append(self.strategy.profit)
        if self.deposit == 0 or self.deposit != self.strategy.deposit:
            self.deposit = self.strategy.deposit
            self.deposits.append(self.strategy.deposit)
        if self.strategy.profit != 0:
            self.sum_profits += self.strategy.profit
            self.sum_profits_arr.append(self.sum_profits)


