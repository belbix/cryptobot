import logging.config
import os
import smtplib

import sqlalchemy
from sqlalchemy.orm import sessionmaker

from entity.tick import Tick
from utils import configloader

log = logging.getLogger(__name__)


class SqlImport:
    engine = sqlalchemy.engine.base.Engine  # type: sqlalchemy.engine.base.Engine
    write_count = 0
    skip_count = 0
    filename = ""
    old_impl = False

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=engine)

        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.filename = config["filename"]
        self.old_impl = config["old_impl"]

    def start(self):
        log.info("----------Start import-------------")
        if self.old_impl:
            self.old_impl(self)
            return


    def handle(self):
        file = open(self.filename)
        with self.engine.connect() as con:
            for line in file:
                result = con.execute(line)


    def old_impl(self):
        try:
            session = self.Session()
            file = open(self.filename)
            count = 0
            for line in file:
                count += 1
                if count >= 51:
                    line = line.split("(")
                    self.imp(line, session)
                log.info("Write %s ticks" % self.write_count)
            session.close()
        except Exception as e:
            log.exception("%s" % e)
        finally:
            log.info("----------End import, write %s | skip %s-------------" % (self.write_count, self.skip_count))

    def imp(self, data, session):
        for state in data:
            state = state.replace("),", "").replace(");\n", "").replace("'", "")
            state = state.split(",")
            if len(state) < 10 or state[2] == "bitmex" or state[8] == "NULL":
                # log.info("skip %s" % state)
                self.skip_count += 1
                continue
            self.write_count += 1
            log.info(state)
            t = Tick()
            try:
                if state[1] is not None and state[1] != "NULL" and state[1] != 0:
                    t.market_id = state[1]
                t.server = state[2]
                t.symbol = state[3]
                t.bid = state[4]
                t.ask = state[5]
                t.last = state[6]
                if state[7] != "NULL":
                    t.amount = float(state[7])
                t.price = state[8]
                t.volume = state[9]
                t.date = state[10]
                session.add(t)
                session.commit()
            except Exception as e:
                if "Duplicate entry" in str(e):
                    log.info("Duplicate entry")
                else:
                    log.exception("%s" % e)
                session.rollback()

