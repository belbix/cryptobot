import logging.config
from datetime import datetime, timedelta

import pandas
from collections import OrderedDict
from sqlalchemy import func, select, column
from sqlalchemy.orm import sessionmaker

from entity.candle import Candle
from entity.tick import Tick
from utils.indicators import stochastic_rsi

log = logging.getLogger(__name__)


def get_volume(ticks, server):
    try:
        # log.info("vlolume: %s ||| %s" % (ticks.iloc[-1]["volume"], ticks.iloc[0]["volume"]))
        if server == "bitfinex":
            return ticks["amount"].abs().sum()
        elif server == "bitmex":
            return ticks.iloc[-1]["amount"] - ticks.iloc[0]["amount"]
        return 0
    except Exception as e:
        log.debug("Error get volume: %s" % e)
        return 0


class CandleCreator:
    run = True
    engine = None
    loop_delay = None
    Session = None
    write_metrics = True
    candle_limit = 500
    ticks_for_dynamic_candle = None
    candles = None
    last_date_for_dynamic_candle = None
    last_ticks = OrderedDict()

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=engine)

        # config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        # self.loop_delay = int(config["loop_delay"])

    def get_dynamic_candle(self, date: datetime, timeframe=1, symbol="BTCUSD",
                           server="bitfinex", first=True, start=None, end=None):

        # log.info("get_dynamic_candle")
        # date = datetime.strptime("2017-08-25 23:46:21", "%Y-%m-%d %H:%M:%S")
        session = self.Session()
        candle = None
        if start is None or end is None:
            dates = self.create_dates(date, timeframe, first)
            start = dates[0]
            end = dates[1]
        # log.info("%s timeframe: %s" % (date, timeframe))
        # log.info("%s | %s th: %s" % (date, start, end))

        try:
            if self.last_date_for_dynamic_candle is None \
                    or self.last_date_for_dynamic_candle != start:
                try:
                    if self.last_date_for_dynamic_candle is not None:
                        log.info("times %s | %s" % (self.last_date_for_dynamic_candle, start))
                    self.last_date_for_dynamic_candle = start
                    abs_end = self.create_dates(date, timeframe, False)[1]
                    s2 = datetime.utcnow()
                    sql = select([Tick.date, Tick.price, Tick.amount]) \
                        .select_from(Tick) \
                        .where(column("price") != 0) \
                        .where(column("server") == server) \
                        .where(column("symbol") == symbol) \
                        .where(column("date") > start) \
                        .where(column("date") < abs_end) \
                        .order_by(Tick.date)

                    self.ticks_for_dynamic_candle = pandas.read_sql(sql, session.bind, parse_dates=["date"])
                    log.info(
                        "get ticks %s | %s" % ((datetime.utcnow() - s2), len(self.ticks_for_dynamic_candle['date'])))
                except Exception as e:
                    if "database is locked" in e:
                        log.warning(str(e))
                        self.ticks_for_dynamic_candle = pandas.read_sql(sql, session.bind, parse_dates=["date"])
                    else:
                        raise e

            if self.ticks_for_dynamic_candle.empty:
                return None
            ticks = self.ticks_for_dynamic_candle[(self.ticks_for_dynamic_candle['date'] < end)]
            # log.info("ticks: \n %s" % ticks)
            if ticks.empty:
                return None

            amount = len(ticks["date"])
            opend = ticks.iloc[0]["price"]
            low = ticks["price"].min()
            high = ticks["price"].max()
            close = ticks.iloc[-1]["price"]
            volume = get_volume(ticks, server)

            candle = Candle(server, symbol, timeframe, start, amount, close, high, low, opend, volume)

            # log.info(candle)

        except:
            log.exception("Error in get min_tick")
        finally:
            session.close()

        return candle

    def get_candles(self, count, date: datetime, timeframe=1, symbol="BTCUSD", server="bitfinex", first_dynamic=False):
        # log.debug(datetime.utcnow())
        session = self.Session()
        candles = None
        start = datetime.utcnow()
        try:
            if not first_dynamic or self.candles is None \
                    or self.candles.empty \
                    or self.candles.iloc[-1]["date"] <= (date - timedelta(minutes=timeframe)):
                start1 = datetime.utcnow()
                sql_query = select([Candle]) \
                    .select_from(Candle) \
                    .where(column("server") == server) \
                    .where(column("symbol") == symbol) \
                    .where(column("time") == timeframe) \
                    .where(column("date") <= date) \
                    .order_by(column("date").desc()) \
                    .limit(count)
                self.candles = candles = pandas.read_sql(sql_query, session.bind,
                                                         parse_dates=["date"]).sort_values(by="date")
                log.debug("get candles from db time %s" % (datetime.utcnow() - start1))
            else:
                candles = self.candles
        except Exception as e:
            log.exception("%s" % e)
        # log.info("t1 %s" % ((datetime.utcnow() - start)))
        if first_dynamic and len(candles["date"]) > 0:
            index = -1
            index2 = -2
            if candles.iloc[index]["date"] == date:
                candles.set_value(candles.iloc[index].name, "amount", candles.iloc[index2]["amount"])
                candles.set_value(candles.iloc[index].name, "open", candles.iloc[index2]["open"])
                candles.set_value(candles.iloc[index].name, "low", candles.iloc[index2]["low"])
                candles.set_value(candles.iloc[index].name, "high", candles.iloc[index2]["high"])
                candles.set_value(candles.iloc[index].name, "close", candles.iloc[index2]["close"])
                candles.set_value(candles.iloc[index].name, "volume", candles.iloc[index2]["volume"])
            else:
                start2 = datetime.utcnow()
                tmpdate = str(candles.iloc[index]["date"])
                tmpdate = tmpdate.replace("Timestamp(", "").replace(")", "")
                fc = self.get_dynamic_candle(date, timeframe, symbol, server,
                                             first=False, start=tmpdate, end=date)

                if fc is not None and fc.date == tmpdate:
                    candles.set_value(candles.iloc[index].name, "amount", fc.amount)
                    candles.set_value(candles.iloc[index].name, "open", fc.open)
                    candles.set_value(candles.iloc[index].name, "low", fc.low)
                    candles.set_value(candles.iloc[index].name, "high", fc.high)
                    candles.set_value(candles.iloc[index].name, "close", fc.close)
                    candles.set_value(candles.iloc[index].name, "volume", fc.volume)
                elif fc is not None:
                    log.info(candles)
                    log.error("ALARMA DATE NOT COMPARE!!! %s | %s == %s = %s" %
                              (date, fc.date, tmpdate, fc.date == tmpdate))
                    return None
                else:
                    log.error("First candle is none!")
                    return None
                log.debug("get dynamic first candle %s" % (datetime.utcnow() - start2))
        return candles

    def get_batch_candles(self, count, date: datetime, timeframe=1, symbol="BTCUSD", server="bitfinex"):
        # log.debug(datetime.utcnow())
        candles = []
        first = True
        for i in range(0, count):
            # log.debug(datetime.utcnow())
            candle = self.get_dynamic_candle(date - timedelta(minutes=timeframe * i), timeframe, symbol, server, first)
            first = False
            # log.info(candle.df())
            if candle is not None:
                candles.append(candle.df())
        return pandas.concat(candles, ignore_index=True).sort_values(by="date")

    def get_batch_candles2(self, count, date: datetime, timeframe=1, symbol="BTCUSD", server="bitfinex", indb=False, cached=10):
        session = self.Session()
        candles = []
        dates = []

        first = True
        for i in range(0, count):
            rd = self.create_dates(date - timedelta(minutes=timeframe * i), timeframe, first)
            dates.append(rd)
            first = False

        first_date = dates[len(dates) - 1][0]
        old_first_date = first_date

        cached_ticks = self.last_ticks.get(str(first_date))
        if cached_ticks is not None and len(cached_ticks["date"]) > 0:
            log.debug("cached_ticks: %s" % len(cached_ticks["date"]))
            t = cached_ticks["date"].iloc[-1]
            log.debug("%s new first date %s" % (first_date,t))
            first_date = t

        last_date = dates[0][1]
        all_ticks = None
        log.debug("get_batch_candles2 first_date %s last_date %s" % (first_date, last_date))
        try:
            sql = select([Tick.date, Tick.price, Tick.amount]).select_from(Tick) \
                .where(column("price") != 0) \
                .where(column("server") == server) \
                .where(column("symbol") == symbol) \
                .where(column("date") > first_date) \
                .where(column("date") < last_date)
            all_ticks = pandas.read_sql(sql, session.bind, parse_dates=["date"])
            log.debug("all_ticks: %s" % len(all_ticks["date"]))
        except:
            log.exception("Error in get min_tick")
            return None
        finally:
            session.close()

        if cached_ticks is not None and len(cached_ticks["date"]) > 0:
            if all_ticks is not None and not all_ticks.empty:
                all_ticks = cached_ticks.append(all_ticks, ignore_index=True)
            else:
                all_ticks = cached_ticks

        all_ticks.drop_duplicates("date")

        self.last_ticks[str(old_first_date)] = all_ticks

        if len(self.last_ticks) > cached:
            for el in self.last_ticks:
                del self.last_ticks[el]
                log.debug("clear %s" % el)
                break

        if all_ticks is None or len(all_ticks["date"]) == 0:
            log.warning("no ticks %s|%s %s -> %s" % (server, symbol, first_date, last_date))
            return None


        for d in dates:
            ticks = all_ticks[(all_ticks["date"] > d[0] + timedelta(seconds=1)) &
                              (all_ticks["date"] < d[1] - timedelta(seconds=1))]
            if not ticks.empty:
                # log.warning(ticks)
                amount = len(ticks["date"])
                opend = ticks.iloc[0]["price"]
                low = ticks["price"].min()
                high = ticks["price"].max()
                close = ticks.iloc[-1]["price"]
                volume = get_volume(ticks, server)
                candle = Candle(server, symbol, timeframe, d[0], amount, close, high, low, opend, volume)
                # log.info(candle.df())
            else:
                candle = None
            if candle is not None:
                if indb:
                    self.write_in_db(candle, d == dates[-1])
                else:
                    candles.append(candle.df())
        if indb:
            return len(candles)
        else:
            return pandas.concat(candles, ignore_index=True).sort_values(by="date")

    def get_batch_candles3(self, count, date: datetime, timeframe=1, symbol="BTCUSD", server="bitfinex"):
        session = self.Session()
        candles = []
        first = True
        for i in range(0, count):
            if first:
                candle = self.get_dynamic_candle(date - timedelta(minutes=timeframe * i, microseconds=1), timeframe,
                                                 symbol, server,
                                                 first)
            else:
                # log.info("Get candles from db")
                exist_candles = []
                try:
                    exist_candles = session.query(Candle) \
                        .filter(Candle.server == server) \
                        .filter(Candle.symbol == symbol) \
                        .filter(Candle.date < date - timedelta(minutes=timeframe * i)) \
                        .filter(Candle.date > date - timedelta(minutes=timeframe * count)) \
                        .filter(Candle.time == timeframe).all()
                except:
                    log.exception("get exist candle error")
                finally:
                    session.close()

                for c in exist_candles:
                    candles.append(c.df())
                break

            first = False
            # log.info(candle.df())
            if candle is not None:
                candles.append(candle.df())
        if len(candles) > 0:
            return pandas.concat(candles, ignore_index=True).sort_values(by="date")
        else:
            return pandas.DataFrame()

    def write_in_db(self, candle, first=False):
        start = datetime.utcnow()
        session = self.Session()
        updated_candle = None
        old_candle = None
        try:
            exist_candle = session.query(Candle) \
                .filter(Candle.server == candle.server) \
                .filter(Candle.symbol == candle.symbol) \
                .filter(Candle.date == candle.date) \
                .filter(Candle.time == candle.time).all()

            not_exist = len(exist_candle) == 0

            if not_exist:
                session.add(candle)
                updated_candle = candle
                # log.debug("add new candle")
            else:
                old_candle = exist_candle[0].copy()
                exist_candle[0].update(candle)
                updated_candle = exist_candle[0]
                # log.debug("update exist candle %s" % updated_candle)
            # session.commit()

            if self.write_metrics and first:
                sql_query = select([Candle]) \
                    .select_from(Candle) \
                    .where(column("server") == candle.server) \
                    .where(column("symbol") == candle.symbol) \
                    .where(column("time") == candle.time) \
                    .where(column("date") <= candle.date) \
                    .order_by(column("date").desc()) \
                    .limit(self.candle_limit)
                candles = pandas.read_sql(sql_query, session.bind, parse_dates=["date"]).sort_values(by="date")

                if candles is not None and len(candles["date"]) > 0 and updated_candle.date != candles.iloc[-1]["date"]:
                    log.debug("date not compare! updated_candle= %s ||| rsi_candle= %s"
                              % (updated_candle.date, candles.iloc[-1]["date"]))
                    candles = None

                rsi_candles = None
                if candles is not None and len(candles["date"]) > 0:
                    rsi_candles = stochastic_rsi(candles)
                if rsi_candles is not None:
                    rsi_candle = rsi_candles.iloc[-1]
                    if updated_candle.date == rsi_candle.date:
                        updated_candle.slowd = rsi_candle["SLOWD"]
                        updated_candle.slowk = rsi_candle["SLOWK"]
                    else:
                        log.error("date not compare! updated_candle= %s ||| new_candle= %s"
                                  % (updated_candle.date, rsi_candle.date))
                old_candle = None

            if old_candle is not None:
                log.info(old_candle)
                updated_candle.slowd = old_candle.slowd
                updated_candle.slowk = old_candle.slowk
            log.info(updated_candle)
            session.commit()
        except Exception as e:
            session.rollback()
            log.exception("Write candle in db error!")
            session.close()
        log.debug("write_in_db time %s" % (datetime.utcnow() - start))

    def create_dates(self, date, timeframe, first=True):
        start = date.replace(second=0, microsecond=0)

        if 1 <= timeframe < 60:
            start = start.replace(minute=int(int(start.minute / timeframe) * timeframe))
        if 60 <= timeframe < 1440:
            hour = int((int((start.minute + (start.hour * 60)) / timeframe) * timeframe) / 60)
            start = start.replace(minute=0, hour=hour)
        if first:
            end = date
        else:
            end = start + timedelta(minutes=timeframe)
        return [start, end]

    def min_date(self):
        session = self.Session()
        min_tick = None

        try:
            min_tick = session.query(Tick, func.min(Tick.date)).where(column("price") != 0).all()
        except:
            session.rollback()
            log.exception("Error in get min_tick")
            session.close()
        finally:
            session.close()
        log.info(min_tick[0])
        return min_tick[0]

# engine = engine_from_config(configloader.get_config("db"), prefix='db.')
# logging.config.dictConfig(configloader.get_config_log())
#
# log.info("----------Start------------- %s" % datetime.utcnow())
# Base.metadata.create_all(engine)
# obj = CandleCreator(engine)
# # date = datetime.utcnow()
# date = datetime.strptime("2017-08-26 01:41:21", "%Y-%m-%d %H:%M:%S")
# candle = obj.get_batch_candles2(1000, date, 360, indb=True)
# log.info(candle)
# log.info("----------End-------------")
