import logging.config
import os
from datetime import datetime

import numpy
from keras.models import Sequential
from keras.models import model_from_json

from neuro.neuro import Neuro, date2float
from strategies.strat import Strat
from utils import configloader
from utils.chargecreator import ChargeCreator
from utils.indicators import deltaprice
from utils.ordercreator import OrderCreator

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)


class MLStrat(Strat):
    # constant
    FEE = 0.002
    STRATEGY_ID = 4
    # buy_model = None
    # sell_model = None
    candles = None
    datadir = "data"
    mlbuy = 0
    mlsell = 0
    generate_model = False
    init = True

    # configurable variable
    perc_buy = 0
    perc_sell = 0
    model_name = ""

    def __init__(self, engine):
        self.engine = engine
        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)
        self.symbol = config["symbol"]
        self.period = config["period"]
        self.model_name = config["model_name"]
        self.perc_buy = float(config["perc_buy"])
        self.perc_sell = float(config["perc_sell"])
        self.test = int(config["test"])
        self.server = config["server"]
        self.loop_delay = int(config["loop_delay"])

        self.last_tick = datetime.utcnow()
        self.charger = ChargeCreator(self.engine)
        self.orderer = OrderCreator(self.engine)

        if self.generate_model:
            self.bn = Neuro(engine)
            self.bn.graphs = False
            self.bn.init = self.init
            self.bn.type = self.model_name
            self.bn.ypos = self.bn.datasize
            self.bn.load_dataset()
            self.bn.create_model_feedforward()
            self.bn.model_compile()
            self.bn.fit_model()
            # self.bn.evaluate()

            self.sn = Neuro(engine)
            self.sn.graphs = False
            self.sn.init = self.init
            self.sn.type = self.model_name
            self.sn.ypos = self.sn.datasize + 1
            self.sn.load_dataset()
            self.sn.create_model_feedforward()
            self.sn.model_compile()
            self.sn.fit_model()
            # self.sn.evaluate()
        else:
            json_buy_model = open(self.datadir + os.sep + "buy_" + self.model_name + "_model.json", "r").read()
            json_sell_model = open(self.datadir + os.sep + "sell_" + self.model_name + "_model.json", "r").read()
            self.buy_model = model_from_json(json_buy_model)  # type:Sequential
            self.sell_model = model_from_json(json_sell_model)  # type:Sequential
            self.buy_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            self.sell_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
            self.buy_model.load_weights(self.datadir + os.sep + "buy_" + self.model_name + "_weights.h5")
            self.sell_model.load_weights(self.datadir + os.sep + "sell_" + self.model_name + "_weights.h5")


        # self.test_model()

    def test_model(self):
        n = Neuro(None)
        n.graphs = False
        dataset = numpy.loadtxt("data/" + self.model_name + ".txt", delimiter=",",
                                converters={n.datepos: date2float})
        n.X_test = dataset[:, 0:n.datasize]
        n.Y_test = dataset[:, n.ypos:n.ypos + 1]

        n.model = self.buy_model
        n.get_predicts()
        n.model = self.sell_model
        n.get_predicts()

    def main_loop(self, candles):
        self.candles = deltaprice(candles)
        self.real_row = self.candles.iloc[-1]
        self.profit = 0
        self.decision_ml()
        self.buy_or_sell()

    def print_processing(self):
        log.info(
            " | " + str(self.real_row["date"]) +
            " | " + str(self.real_row["close"]) +
            # " | " + str(self.real_row["PVOH"]) +
            " | buy: %f | sell: %f" % (self.mlbuy, self.mlsell) +
            " | " + self.status)

    def print_event(self, msg):
        log.warning(msg +
                    " date=" + str(self.real_row["date"]) +
                    " price=" + str(self.real_row["close"]) +
                    " | charge_id=" + str(self.charge_id)
                    )

    def decision_ml(self):
        data = [[]]
        cdata = self.candles.tail(30)
        f = open("real_data.txt", "a")
        # log.info(cdata)
        for i in range(0, len(cdata["close"])):
            x = cdata.iloc[i]["PRICE"]
            f.write(str(x))
            x /= 255
            data[0].append(x)
            if i != len(cdata["close"]) - 1:
                f.write(",")
            else:
                f.write(str(cdata.iloc[i]["date"]))
                f.write("\n")
        f.close()
        data = numpy.array(data)
        # log.info(data)
        if self.generate_model:
            self.mlbuy = self.bn.model_predict(data)
            self.mlsell = self.sn.model_predict(data)
        else:
            self.mlbuy = self.buy_model.predict(data)
            self.mlsell = self.sell_model.predict(data)
        # log.info("buy: %f | sell: %f" % (self.mlbuy, self.mlsell))

        if self.mlbuy > self.perc_buy and self.buys == 0:
            self.buy = True
        # elif self.real_row["PVOH"] < -pvo_min and self.row["PVOH"] < -pvo_min and self.buys != 0:
        elif self.mlsell > self.perc_sell and self.buys != 0:
            self.sell = True
