import logging.config
import time
from datetime import datetime, timedelta

import numpy
from matplotlib import pyplot

from utils.candlecreator import CandleCreator
from utils.chargecreator import ChargeCreator
from mappers.bitfinexmapper import BitfinexMapper, get_time

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)


class Strat:
    # class variable
    run = True
    last_tick = None

    # constant
    FEE = 0.002
    STRATEGY_ID = -1
    TYPE_BUY = 0
    TYPE_SELL = 1
    test = True
    download_candles = 500

    # not loop variable
    deposit = 1000
    buys = 0
    sells = 0
    profits = 0
    profit = 0
    count = 0
    row = None
    real_row = None
    status = ""
    buy = False
    sell = False
    mapper = BitfinexMapper("", "")
    cc = None

    # configurable variable
    candle_from_db = False
    charge_id = None
    symbol = None
    period = None
    loop_delay = None
    engine = None
    charger = None
    orderer = None
    server = None
    operation_delay = 0
    simple_create = False
    leverage = 1
    # extremums sl / tp

    price_delta_downs = []
    price_delta_down_max = 0
    price_delta_down = 0

    price_delta_ups = []
    price_delta_up_max = 0
    price_delta_up = 0
    last_charge = None

    def start(self):
        self.init_depo()
        try:
            while self.run:
                candles = self.get_candles()
                if candles is None or len(candles) == 0:
                    log.info("nope candles")
                    continue
                self.processing(candles)
                self.last_tick = datetime.utcnow()
        finally:
            log.warning("Strategy process stop!")
            log.info("price_delta_ups: %s" % self.price_delta_ups)
            log.info("price_delta_downs: %s" % self.price_delta_downs)
            log.info("price_delta_ups max: %s" % numpy.amax(self.price_delta_ups))
            log.info("price_delta_downs max: %s" % numpy.amax(self.price_delta_downs))

    def init_depo(self):
        log.info("Init deposit")
        try:
            self.buys = self.orderer.get_buys(self.symbol)
        except:
            log.exception("Can`t get balance")
        if self.buys != 0:
            self.status = "buy %s" % self.buys
            candles = self.get_candles()
            self.real_row = candles.iloc[-1]
            self.deposit = self.buys * self.real_row["close"]
            log.info("deposit %s" % self.deposit)

    def get_candles(self):
        candles_ = None
        while candles_ is None:
            try:
                if self.candle_from_db:
                    timeframe = get_time(self.period)
                    # d = datetime.utcnow() - timedelta(minutes=timeframe * 30)
                    d = datetime.utcnow()
                    candles_ = self.cc.get_candles(self.download_candles, d, timeframe=timeframe, symbol=self.symbol,
                                                          server=self.mapper.SERVER_NAME)
                        # .sort_values(by="date", ascending=False)
                else:
                    candles_ = self.mapper.get_candles(self.period, self.symbol, True, self.download_candles)
            except:
                log.exception("get candles error")
            time.sleep(self.loop_delay)
        return candles_

    def buy_or_sell(self):
        close_position = 0
        if self.buy or self.sell:
            if self.price_delta_down_max != 0 and abs(self.price_delta_down_max) != 100:
                log.info("price_delta_up_max: %s" % self.price_delta_down_max)
                self.price_delta_downs.append(self.price_delta_down_max)
            self.price_delta_down_max = 0
            if self.price_delta_up_max != 0 and abs(self.price_delta_up_max) != 100:
                log.info("price_delta_down_max: %s" % self.price_delta_up_max)
                self.price_delta_ups.append(self.price_delta_up_max)
            self.price_delta_up_max = 0

        if self.buy:
            if self.sells != 0:
                close_position = 1
                self.print_event("BUY! close sell")
                self.status = "BUY! close sell"
                amount = self.sells * self.real_row["close"]
                commission = amount * self.FEE
                self.profit = (self.deposit * self.leverage) - amount - commission
                self.deposit += self.profit
                self.profits += self.profit
                log.warning(
                    "We have profit: " + str(self.profit) + " All profit: " +
                    str(self.profits) + " All games:" + str(self.count) + " depo: %s" % self.deposit +
                    " amount %s | p: %s| commission: %s | deposit: %s" % (amount, self.deposit - amount, commission, self.deposit))
                self.buys = 0
            else:
                # self.charge_id = 0
                self.print_event("BUY! new long")
                self.status = "BUY! new long"
                amount = (self.deposit / self.real_row["close"]) * self.leverage
                self.buys = amount
                self.buys -= amount * self.FEE
                self.count += 1

            if not self.test:
                self.prepare_charge(self.TYPE_BUY, close_position)

            self.sells = 0
            self.clear()

        if self.sell:
            close_position = 0
            if self.buys != 0:
                close_position = 1
                self.print_event("SELL! close buy")
                self.status = "SELL! close buy"
                amount = self.buys * self.real_row["close"]
                commission = amount * self.FEE
                self.profit = amount - (self.deposit * self.leverage) - commission
                self.deposit += self.profit
                self.profits += self.profit
                log.warning(
                    "We have profit: " + str(self.profit) + " All profit: " +
                    str(self.profits) + " All games:" + str(self.count) + " depo: %s" % self.deposit +
                    " amount %s | p: %s| com: %s | dep: %s" % (amount, self.deposit - amount, commission, self.deposit))
            else:
                self.print_event("SELL! new short")
                # self.charge_id = 0
                amount = (self.deposit / self.real_row["close"]) * self.leverage
                self.sells = amount
                self.sells -= amount * self.FEE
                self.count += 1
                self.status = "SELL! new short"

            if not self.test:
                self.prepare_charge(self.TYPE_SELL, close_position)
            self.buys = 0
            self.clear()

        if self.deposit <= 0:
            log.warning("Deposit = 0! You loser!")
            exit(1)

    def prepare_charge(self, type, close_position):
        c = None
        if self.simple_create:
            c = self.create_charge(type, close_position, self.last_charge)
            c.strategy_id = -2  # set id for simple charge
            if c is not None:
                try:
                    self.orderer.create_order(c, instant=True)
                except Exception as e:
                    log.exception("%s" % e)
        else:
            c = self.create_charge(type, close_position, self.last_charge)
            if c is not None:
                self.charge_id = c.id
            else:
                self.charge_id = 0

        if c and close_position == 0:
            self.last_charge = c
        else:
            self.last_charge = None

        time.sleep(self.operation_delay)

    def create_charge(self, t, close_position=0, last_charge = None):
        charge = None
        init_charge = None
        if last_charge:
            init_charge = last_charge.id
        while charge is None:
            try:
                oc = ChargeCreator(self.engine)
                charge = oc.create_charge(self.symbol, self.real_row, t, self.STRATEGY_ID, init_charge,
                                      self.mapper.SERVER_NAME, close_position)
            except:
                log.exception("charge error")
                time.sleep(1)
        return charge

    def main_loop(self, candles):
        log.warning("empty strategy!")

    def processing(self, candles):
        try:
            if self.price_delta_down != 0 and \
                    (self.price_delta_down_max == 0 or self.price_delta_down_max < self.price_delta_down):
                self.price_delta_down_max = self.price_delta_down

            if self.price_delta_up != 0 and \
                    (self.price_delta_up_max == 0 or self.price_delta_up_max < self.price_delta_up):
                self.price_delta_up_max = self.price_delta_up

            self.main_loop(candles)
        except:
            log.exception("processing error")
        finally:
            try:
                self.print_processing()
            except:
                log.exception("print error")

    def print_processing(self):
        log.info("overwrite this method!")

    def print_event(self, msg):
        log.warning(msg + " overwrite this method!")

    def clear(self):
        self.buy = False
        self.sell = False
