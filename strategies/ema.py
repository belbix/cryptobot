import logging.config
import os
from datetime import datetime

from sqlalchemy.orm import sessionmaker

from entity import testdata
from mappers.bitmexmapper import BitmexMapper
from strategies.strat import Strat
from utils import configloader, indicators
from utils.candlecreator import CandleCreator
from utils.indicators import stochastic_rsi, add_pvo
from utils.ordercreator import OrderCreator

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)
logging.getLogger("utils.candlecreator").setLevel(logging.INFO)
logging.getLogger("bitmex.market_maker.bitmex").setLevel(logging.WARNING)


class EMA(Strat):
    # constant
    FEE = (0.075 + 0.15) / 100
    STRATEGY_ID = 6

    # not loop variable
    real_delta_k = None
    skip = False
    delta_k = 0
    K = 0
    D = 0
    old_k = 0
    old_d = 0
    old_delta_k = 0
    old_k2 = 0
    old_d2 = 0
    old_delta_k2 = 0
    order_price = 0
    candles_day = None
    row_day = 0
    day_delta_k = 0
    namek = "SLOWK"
    named = "SLOWD"
    price_delta = 0
    real_price_delta = 0
    dynamic_candle = None
    dynamic_delta_kd = 0
    tp_sell = False
    sl_sell = False
    ema1_value = 0
    ema2_value = 0
    dema = 0

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)
        self.cc = CandleCreator(engine)
        self.config = config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)
        self.candle_from_db = config["candle_from_db"]
        self.symbol = config["symbol"]
        self.period = config["period"]
        self.test = config["test"]
        self.loop_delay = int(config["loop_delay"])
        self.simple_create = config["simple_create"]
        self.download_candles = config["download_candles"]
        self.metric_from_db = config["metric_from_db"]
        self.tp_price = config["tp_price"]
        self.sl_price = config["sl_price"]
        self.proxy = config["proxy"]
        self.enable_long = config["enable_long"]
        self.enable_short = config["enable_short"]
        self.datedeltax = config["datedeltax"]

        self.ema1 = config["ema1"]
        self.ema2 = config["ema2"]
        self.ema1bs = config["ema1bs"]
        self.ema1bc = config["ema1bc"]
        self.ema1ss = config["ema1ss"]
        self.ema1sc = config["ema1sc"]

        self.enable_pvo = config["enable_pvo"]
        self.pvo_max_long_open = config["pvo_max_long_open"]
        self.pvo_max_long_close = config["pvo_max_long_close"]
        self.pvo_max_short_open = config["pvo_max_short_open"]
        self.pvo_max_short_close = config["pvo_max_short_close"]

        self.enable_testdata = False

        self.mapper = BitmexMapper("JKIAo442CYwX5ak5KlYJGwPa",
                                   "83UWnblikD2j-2l5-KBtyvHa4P3ViuqAJdern4XMm2e0bpZr",
                                   base_url="https://www.bitmex.com/api/v1/", skipws=True, proxy=self.proxy)

        self.last_tick = datetime.utcnow()
        self.orderer = OrderCreator(self.engine, skipws=True)

    def main_loop(self, candles):
        candles = indicators.EMA2(candles, self.ema1, self.ema2)

        if self.enable_pvo:
            candles = add_pvo(candles)

        self.profit = 0
        try:
            if not self.validate(candles):
                return
        except Exception as e:
            # log.info(candles)
            raise e
        self.decision()
        self.buy_or_sell()

    def print_event(self, msg):

        s = ""

        log.warning(msg + s)

        if self.enable_testdata:
            session = self.Session()
            try:
                td = testdata.Testdata()
                td.date = datetime.utcnow()
                data = ""
                data += "%s;" % self.real_row["date"]
                data += "%s;" % self.real_row["close"]
                data += "%s;" % round(self.real_row["volume"] / 1000000, 2)
                data += "%s;" % self.ema1_value
                data += "%s;" % self.ema2_value
                data += "%s;" % ("buy" if self.buy else "sell")
                data += "%s;" % self.config
                td.data = data
                session.add(td)
                session.commit()
            except Exception as e:
                log.exception("Error save testdata")
            finally:
                session.close()

    def print_processing(self):
        s = ""
        s += "%s" % self.real_row["date"]
        s += " | %s" % self.real_row["close"]
        s += " | %s" % round(self.dema, 2)
        s += " |ema_%s: %s" % (self.ema1, round(self.ema1_value, 1))
        s += " |ema_%s: %s" % (self.ema2, round(self.ema2_value, 1))
        if self.enable_pvo:
            s += " |pvo: %s" % round(self.real_row["PVOH"], 1)
        s += " |up_max: %s" % round(self.price_delta_up_max, 2)
        s += " |down_max: %s" % round(self.price_delta_down_max, 2)
        s += " | %s" % self.status + "(%s, %s, %s)" % (self.deposit, self.buys, self.sells)
        log.info(s)

    def validate(self, candles):
        self.real_row = candles.iloc[-1]
        self.row = candles.iloc[-2]
        datedelta = (datetime.utcnow() - self.real_row["date"]).total_seconds()
        if datedelta < self.datedeltax:
            self.status = "skip first %s seconds" % self.datedeltax
            return False

        self.ema1_value = self.real_row["EMA_%s" % self.ema1]
        self.ema2_value = self.real_row["EMA_%s" % self.ema2]

        self.dema = ((self.ema1_value - self.ema2_value) / self.ema1_value) * 100

        self.price_delta = round(((self.row["close"] - self.row["open"]) / self.row["open"]) * 100, 2)
        self.real_price_delta = round(((self.real_row["close"] - self.real_row["open"]) / self.real_row["open"]) * 100,
                                      2)
        self.otskok = abs(self.price_delta) - abs(self.real_price_delta)

        return True

    def decision(self):

        if self.order_price != 0:
            self.price_delta_up = 100 * ((self.real_row["close"] - self.order_price) / self.real_row["close"])
            self.price_delta_down = 100 * ((self.order_price - self.real_row["close"]) / self.real_row["close"])
        else:
            self.price_delta_down = 0
            self.price_delta_up = 0

        self.long()
        self.short()
        if self.buy or self.sell:
            self.order_price = self.real_row["close"]

    def long(self):
        if not self.enable_long:
            return

        tp = self.tp_price != 0 and self.tp_price < self.price_delta_up
        sl = self.sl_price != 0 and self.sl_price < self.price_delta_down

        if self.enable_pvo:
            pvo_decision = self.real_row["PVOH"] > self.pvo_max_long_open
        else:
            pvo_decision = True

        decision = self.dema > self.ema1bs and pvo_decision
        close_decision = self.dema < self.ema1bc or tp or sl

        if decision and self.buys == 0:
            self.order_price = self.real_row["close"]
            self.tp_sell = False
            self.sl_sell = False
            self.buy = True
            return

        if close_decision and self.buys != 0:
            self.sell = True
            self.order_price = 0
            if tp:
                log.info("TAKE PROFIT!!!")
                self.tp_sell = True
            if sl:
                log.info("STOP LOSE!!!")
                self.sl_sell = True

    def short(self):
        if not self.enable_short:
            return

        tp = self.tp_price != 0 and self.tp_price < self.price_delta_down
        sl = self.sl_price != 0 and self.sl_price < self.price_delta_up

        if self.enable_pvo:
            pvo_decision = self.real_row["PVOH"] < self.pvo_max_short_open
        else:
            pvo_decision = True

        decision = self.dema < self.ema1ss and pvo_decision
        close_decision = self.dema > self.ema1bc or tp or sl

        if decision and self.sells == 0:
            self.tp_sell = False
            self.sl_sell = False
            self.sell = True
            self.order_price = self.real_row["close"]
            return

        if close_decision and self.sells != 0:
            self.buy = True
            self.order_price = 0
            if tp:
                log.info("TAKE PROFIT!!!")
                self.tp_sell = True
            if sl:
                log.info("STOP LOSE!!!")
                self.sl_sell = True
