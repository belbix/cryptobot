import logging.config
import os
import time
from datetime import datetime

from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker

from entity import sentiment
from entity.sentiment import Sentiment
from mappers.bitfinexmapper import BitfinexMapper
from utils import configloader
from utils.chargecreator import ChargeCreator

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)


class BookStrat:
    # class variable
    run = True
    last_tick = None

    # constant
    FEE = 0.002
    STRATEGY_ID = 2
    TYPE_BUY = 0
    TYPE_SELL = 1
    test = True

    # not loop variable
    buy = False
    sell = False
    buys = 0
    sells = 0
    profits = 0
    profit = 0
    count = 0
    charge_id = None

    status = ""
    mapper = BitfinexMapper("", "")

    last_price = 0
    books = None
    candle = None
    sentiment = None
    sentiments = None

    # configurable variable
    engine = None
    loop_delay = None
    precision = None
    symbol = None
    length = None
    price_skip = None
    order_perc_max = None
    order_perc_min = None

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=engine)

        sentiment.Base.metadata.create_all(engine)

        config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        self.loop_delay = int(config["loop_delay"])
        self.precision = config["precision"]
        self.symbol = config["symbol"]
        self.length = config["length"]
        self.price_skip = config["price_skip"]
        self.order_perc_max = config["order_perc_max"]
        self.order_perc_min = config["order_perc_min"]

        self.last_tick = datetime.utcnow()

    def start(self):
        while self.run:
            try:
                self.last_price = self.mapper.get_tick(self.symbol).last
                self.books = self.mapper.get_books(self.symbol, self.precision, self.length)
            except:
                log.exception("error geting...")
                continue

            if self.books is None:
                log.info("nope books")
                continue

            self.processing()

            session = self.Session()
            try:
                session.add(self.sentiment)
                session.commit()
            except:
                log.exception("")
            finally:
                session.close()

            self.last_tick = datetime.utcnow()
            time.sleep(self.loop_delay)
        log.warning("Strategy process stop!")

    def processing(self):
        self.profit = 0
        try:
            self.validate()

            self.decision()

            self.buy_or_sell()
        except:
            log.exception("processing error")
        finally:
            try:
                log.info(self.sentiment)
            except:
                log.exception("print error")

    def validate(self):
        bids = 0
        asks = 0
        bids_count = 0
        asks_count = 0
        orders = 0
        for book in self.books:
            if abs(book.price - self.last_price) > self.last_price * 0.01 * self.price_skip:
                continue
            orders += book.amount
            if book.amount > 0:
                bids += book.amount
                bids_count += book.count
            else:
                asks += book.amount
                asks_count += book.count

        orders_perc = int((orders / (bids + abs(asks))) * 100)
        asks = round(asks, 2)
        bids = round(bids, 2)
        orders = round(orders, 2)

        self.sentiment = Sentiment()
        self.sentiment.date = self.books[0].date
        self.sentiment.server = self.books[0].server
        self.sentiment.symbol = self.books[0].symbol
        self.sentiment.price = self.last_price
        self.sentiment.bids = bids
        self.sentiment.asks = asks
        self.sentiment.bids_count = bids_count
        self.sentiment.asks_count = asks_count
        self.sentiment.orders = orders
        self.sentiment.orders_perc = orders_perc

        if self.sentiments is None:
            self.sentiments = self.sentiment.df()
        else:
            self.sentiments = self.sentiments.append(self.sentiment.df(), ignore_index=True)

        min_periods = 5

        if len(self.sentiments["date"]) > min_periods:
            self.sentiments = self.sentiments.drop(self.sentiments.head(1).index)
            self.sentiments = \
                self.sentiments.assign(EMAPRICE=self.sentiments.get("orders_perc").ewm(com=min_periods).mean())

            log.info("%s | %s" %
                     (self.sentiments.iloc[-1]["orders_perc"]
                      , self.sentiments.iloc[-1]["EMAPRICE"]))

    def decision(self):
        self.status = ""
        if self.sentiment.orders_perc > self.order_perc_max:
            if self.buys == 0:
                self.buy = True

        if self.sentiment.orders_perc < -self.order_perc_min:
            if self.buys != 0:
                self.sell = True

    def clear(self):
        self.sells = 0
        self.buy = False
        self.sell = False

    def create_charge(self, t):
        oc = ChargeCreator(self.engine)
        return oc.create_charge(self.symbol, self.candle, t, self.STRATEGY_ID, self.charge_id)

    def print_event(self, msg):
        log.warning(msg +
                    " price=" + str(self.last_price) +
                    " all=" + str(self.sentiment.orders) +
                    " all_perc=" + str(self.sentiment.orders_perc) +
                    " | charge_id=" + str(self.charge_id))

    def buy_or_sell(self):
        if self.buy:
            self.buys = self.last_price
            self.buys += self.last_price * self.FEE
            self.count += 1
            self.clear()
            if not self.test:
                self.charge_id = self.create_charge(self.TYPE_BUY)
            self.status = "buy"
            self.print_event("BUY!!!")
        elif self.sell:
            self.sells = self.last_price
            self.sells -= self.last_price * self.FEE
            self.profit = self.sells - self.buys
            self.profits += self.profit
            self.clear()
            self.buys = 0
            if not self.test:
                self.create_charge(self.TYPE_SELL)
                self.charge_id = None
            log.warning(
                "We have profit: " + str(self.profit) + " All profit: " +
                str(self.profits) + " All games:" + str(self.count))
            self.status = "sell"
            self.print_event("SELL!!!")


engine = engine_from_config(configloader.get_config("db"), prefix='db.')
logging.config.dictConfig(configloader.get_config_log())
log.info("----------Start-------------")
obj = BookStrat(engine)
obj.start()
log.info("----------End-------------")
