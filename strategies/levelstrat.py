import logging.config
import os
from datetime import datetime

from sqlalchemy.orm import sessionmaker

from entity import testdata
from mappers.bitmexmapper import BitmexMapper
from mappers.ibmapper import IBMapper
from strategies.strat import Strat
from utils import configloader
from utils.candlecreator import CandleCreator
from utils.chargecreator import ChargeCreator
from utils.levels import Levels
from utils.ordercreator import OrderCreator

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)
logging.getLogger("utils.levels").setLevel(logging.INFO)


class LevelStrat(Strat):
    levels = None
    # constant
    FEE = 0.002
    STRATEGY_ID = 5

    candles = None
    level_buy = 0
    level_sell = 0
    order_price = 0
    init = True
    pairs = None
    decision = ""
    config = None
    decision_in_triangle_buy = False
    decision_in_triangle_sell = False
    levels_delta = 0
    start_levels_delta = 0
    in_triangle = False
    out_triangle_order = False
    ppresult = ""
    final_decision_buy = False
    final_decision_sell = False
    delta_k = 0
    count_dk = 0.1
    use_rsi = False

    # configurable variable

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)
        self.levels = Levels(engine)
        self.config = config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)
        self.candle_from_db = config["candle_from_db"]
        self.symbol = config["symbol"]
        self.levels.symbols = [self.symbol]
        self.period = config["period"]
        self.test = config["test"]
        self.levels.test = config["test"]
        self.download_candles = config["download_candles"]
        self.levels.scale_try_count = config["scale_try_count"]
        if config["scale_try_count"] > 1:
            self.levels.count_minuts = int(int(config["download_candles"]) / (2 ** config["scale_try_count"]))
        else:
            self.levels.count_minuts = config["download_candles"]
        self.levels.base_count_minutes = self.levels.count_minuts
        self.levels.candle_batch = config["download_candles"]
        self.server = config["server"]
        self.loop_delay = int(config["loop_delay"])
        self.simple_create = config["simple_create"]
        self.enable_testdata = config["enable_testdata"]

        self.levels.enable_plot = config["enable_plot"]
        self.skip_first = config["skip_first"]
        self.levels.tail_count = config["tail_count"]
        self.levels.level_buy = config["level_buy"]
        self.levels.level_buy_end = config["level_buy_end"]
        self.levels.level_sell = config["level_sell"]
        self.levels.level_sell_end = config["level_sell_end"]
        self.level_close_buy = config["level_close_buy"]
        self.level_close_sell = config["level_close_sell"]
        self.up_angle = config["up_angle"]
        self.down_angle = config["down_angle"]
        self.level_buy_delta = config["level_buy_delta"]
        self.level_buy_delta_start = config["level_buy_delta_start"]
        self.level_sell_delta = config["level_sell_delta"]
        self.level_sell_delta_start = config["level_sell_delta_start"]
        self.levels_delta_perc = config["levels_delta_perc"]
        self.min_length_level = config["min_length_level"]
        self.not_triangle_hard = config["not_triangle_hard"]

        self.tp_price = config["tp_price"]
        self.sl_price = config["sl_price"]
        self.enable_long = config["enable_long"]
        self.enable_short = config["enable_short"]
        self.levels.draw_extreme = False
        self.proxy = config["proxy"]

        self.last_tick = datetime.utcnow()
        self.charger = ChargeCreator(self.engine)
        self.orderer = OrderCreator(self.engine)
        self.cc = CandleCreator(engine)
        testdata.Base.metadata.create_all(engine)

        if self.server == "bitmex":
            self.mapper = BitmexMapper("JKIAo442CYwX5ak5KlYJGwPa",
                                       "83UWnblikD2j-2l5-KBtyvHa4P3ViuqAJdern4XMm2e0bpZr",
                                       base_url="https://www.bitmex.com/api/v1/", skipws=True, proxy=self.proxy)
        elif self.server == "ib":
            self.mapper = IBMapper("", "", start=False)

    def main_loop(self, candles):
        self.real_row = candles.iloc[-1]
        if self.skip_first != 0:
            candles = candles.head(len(candles["close"]) - self.skip_first)
        self.levels.extend_candles = candles
        self.profit = 0
        self.decision_level()
        self.buy_or_sell()

    def print_processing(self):
        price_round = 0
        l = len(self.levels.candles["date"])
        r = ""
        r += "%s" % l
        r += " | %s" % (self.real_row["date"])
        r += " | %s/%s" % (self.decision, int(self.decision_in_triangle_sell))
        r += " | %s(%s) / %s(%s/%s) / %s(%s)" % \
             (int(self.levels.fy_up), round((self.levels.y / self.levels.fy_up) * 100 - 100, 1),
              int(self.levels.y),
              int(self.levels.y > self.levels.fy_up * self.levels.level_buy),
              int(self.levels.y < self.levels.fy_down * self.levels.level_sell),
              int(self.levels.fy_down), round((self.levels.y / self.levels.fy_down) * 100 - 100, 1))
        r += " | %s°(%s°)/%s°(%s°)" % (self.levels.up_angle, self.up_angle, self.levels.down_angle, self.down_angle)
        r += " | %s/%s(%s)" % (self.levels.upl, self.levels.downl, self.min_length_level)
        r += " | ^%s/^%s" % (int(self.start_levels_delta), int(self.levels_delta))
        if self.start_levels_delta != 0:
            r += "(%s)" % (int((self.levels_delta / self.start_levels_delta) * 100))
        else:
            r += "(0)"
        try:
            r += " | rsi:%s" % round(self.real_row["slowk"] - self.real_row["slowd"], 1)
        except:
            log.debug("error rsi")
        r += " | IN? %s" % int(self.in_triangle)
        r += " | fdb:%s" % int(self.final_decision_buy)
        r += " | fds:%s" % int(self.final_decision_sell)
        r += " | " + self.status
        r += "(%s, b:%s, s:%s)" % (self.deposit, self.buys, self.sells)
        self.ppresult = r
        log.info(r)

    def print_event(self, msg):
        if self.enable_testdata:
            session = self.Session()
            td = testdata.Testdata()
            td.date = datetime.utcnow()
            data = ""
            data += "%s;" % self.real_row["date"]
            data += "%s;" % self.decision
            data += "%s;" % self.levels.y
            data += "%s;" % self.levels.fy_up
            data += "%s;" % self.levels.fy_down
            data += "%s;" % self.levels.down_angle
            data += "%s;" % self.levels.up_angle
            data += "%s;" % self.levels.upl
            data += "%s;" % self.levels.downl
            data += "%s;" % round(self.levels_delta / self.start_levels_delta, 2)

            data += "%s;" % round(((self.levels.fy_up - self.levels.y) / self.levels.y) * 100, 2)
            data += "%s;" % round(((self.levels.y - self.levels.fy_down) / self.levels.y) * 100, 2)

            data += "%s;" % int(self.decision_in_triangle_buy)
            data += "%s;" % int(self.decision_in_triangle_sell)
            data += "%s;" % ("buy" if self.buy else "sell")
            try:
                data += "%s;" % round(self.delta_k, 2)
            except:
                data += "%s;" % 0

            data += "%s;" % self.ppresult
            data += "%s;" % self.config
            td.data = data
            try:
                session.add(td)
                session.commit()
            except Exception as e:
                session.rollback()
                log.exception("Error save testdata")
            finally:
                session.close()

        log.warning(msg +
                    " date=%s" % self.real_row["date"] + "\n" +
                    " | price=%s" % self.real_row["close"] + "\n" +
                    " | levels_delta=%s" % self.levels_delta + "\n" +
                    " | decision_in_triangle_buy=%s" % self.decision_in_triangle_buy + "\n" +
                    " | decision_in_triangle_sell=%s" % self.decision_in_triangle_sell + "\n" +
                    " | charge_id=%s" % self.charge_id
                    )

    def decision_level(self):
        self.levels.check_pairs(get_candles=False)
        pairs = self.levels.pairs
        try:
            self.delta_k = self.real_row["slowk"] - self.real_row["slowd"]
        except:
            self.delta_k = 0

        if len(pairs) != 0:
            self.decision = pairs.get(self.symbol)
        else:
            self.decision = "none"
            if self.levels.fy_up is None:
                self.levels.fy_up = 0
            if self.levels.fy_down is None:
                self.levels.fy_down = 0

        self.levels_delta = self.levels.fy_up - self.levels.fy_down
        self.start_levels_delta = self.levels.up[0][1] - self.levels.down[0][1]
        if self.start_levels_delta != 0:
            self.in_triangle = (self.levels_delta / self.start_levels_delta) > self.levels_delta_perc
        else:
            self.in_triangle = False

        if self.in_triangle and self.levels.fy_up != 0 and self.levels.fy_down != 0:
            if self.level_buy_delta != 0:
                level_buy = self.levels_delta * self.level_buy_delta
                level_buy_start = self.levels_delta * self.level_buy_delta_start
                self.decision_in_triangle_buy = \
                    (self.levels.fy_down + level_buy) > self.levels.y > (self.levels.fy_down + level_buy_start)
            if self.level_sell_delta != 0:
                level_sell = self.levels_delta * self.level_sell_delta
                level_sell_start = self.levels_delta * self.level_sell_delta_start
                self.decision_in_triangle_sell = \
                    (self.levels.fy_up - level_sell_start) > self.levels.y > (self.levels.fy_up - level_sell)

        self.long()
        if not self.sell and not self.buy:
            self.short()

        if self.buy or self.sell:
            self.order_price = self.real_row["close"]

    def long(self):
        if not self.enable_long:
            return

        if not self.in_triangle:
            local_decision = self.decision == "buy"
        else:
            local_decision = self.decision_in_triangle_buy

        self.final_decision_buy = (local_decision or (self.not_triangle_hard and self.decision == "buy")) \
                                  and self.levels.up_angle < self.up_angle \
                                  and self.levels.upl > self.min_length_level \
                                  and ((self.use_rsi and self.delta_k > self.count_dk) or not self.use_rsi)

        if self.final_decision_buy and self.buys == 0:
            log.info("set new long in=%s" % self.in_triangle)
            if self.decision == "buy":
                self.out_triangle_order = True
            self.buy = True
            return
        tp = self.tp_price != 0 \
             and self.tp_price < 100 * ((self.real_row["close"] - self.order_price) / self.real_row["close"])

        sl = self.sl_price != 0 \
             and self.sl_price < 100 * ((self.order_price - self.real_row["close"]) / self.real_row["close"])

        # -----------------------------------------------------------------------------------
        if not self.in_triangle:  # or self.out_triangle_order:
            # if self.out_triangle_order:
            local_decision = self.decision == "sell" \
                             or (self.levels.fy_up != 0
                                 and self.levels.y < (self.levels.fy_up * self.level_close_buy))
        else:
            local_decision = self.decision_in_triangle_sell \
                             or (self.levels.fy_down != 0
                                 and self.levels.y < (self.levels.fy_down * self.level_close_buy))
        # -----------------------------------------------------------------------------------

        if (local_decision or tp or sl) and not self.final_decision_buy and self.buys != 0:
            self.sell = True
            self.out_triangle_order = False
            log.info("close longe in=%s | d:%s" % (self.in_triangle, self.final_decision_buy))
            if tp:
                log.warning("Take profit!")
            if sl:
                log.warning("Stop lose!")

    def short(self):
        if not self.enable_short:
            return

        if not self.in_triangle:
            local_decision = self.decision == "sell"
        else:
            local_decision = self.decision_in_triangle_sell

        self.final_decision_sell = (local_decision or (self.not_triangle_hard and self.decision == "sell")) \
                                   and self.levels.down_angle < self.down_angle \
                                   and self.levels.downl > self.min_length_level \
                                   and ((self.use_rsi and self.delta_k < -self.count_dk) or not self.use_rsi)

        if self.final_decision_sell and self.sells == 0:
            if self.decision == "sell":
                self.out_triangle_order = True
            log.info("set new short in=%s" % self.in_triangle)
            self.sell = True
            return
        tp = self.tp_price != 0 \
             and self.tp_price < 100 * ((self.order_price - self.real_row["close"]) / self.real_row["close"])
        sl = self.sl_price != 0 \
             and self.sl_price < 100 * ((self.real_row["close"] - self.order_price) / self.real_row["close"])

        # -----------------------------------------------------------------------------------
        # if not self.in_triangle:  # or self.out_triangle_order:
        if self.out_triangle_order:
            local_decision = self.decision == "buy" \
                             or (self.levels.fy_down != 0
                                 and self.levels.y > (self.levels.fy_down * self.level_close_sell))
        else:
            local_decision = self.decision_in_triangle_buy \
                             or (self.levels.fy_up != 0
                                 and self.levels.y > (self.levels.fy_up * self.level_close_sell))
        # -----------------------------------------------------------------------------------

        if (local_decision or tp or sl) and not self.final_decision_sell and self.sells != 0:
            self.out_triangle_order = False
            self.buy = True
            log.info("close short in=%s" % self.in_triangle)
            if tp:
                log.warning("Take profit!")
            if sl:
                log.warning("Stop lose!")
