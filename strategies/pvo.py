import logging.config
import os
from datetime import datetime

from sqlalchemy.orm import sessionmaker

from entity import testdata
from mappers.bitmexmapper import BitmexMapper
from strategies.strat import Strat
from utils import configloader
from utils.candlecreator import CandleCreator
from utils.chargecreator import ChargeCreator
from utils.indicators import add_pvo, stochastic_rsi
from utils.ordercreator import OrderCreator

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)
logging.getLogger("bitmex.market_maker.bitmex").setLevel(logging.WARNING)


class PVO(Strat):
    # constant
    FEE = 0.002
    STRATEGY_ID = 3

    order_price = 0
    tp_sell = False
    sl_sell = False

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)
        self.config = config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)
        self.symbol = config["symbol"]
        self.period = config["period"]
        self.pvo_max_buy = config["pvo_max_buy"]
        self.pvo_max_sell = config["pvo_max_sell"]
        self.pvo_min_buy = config["pvo_min_buy"]
        self.pvo_min_sell = config["pvo_min_sell"]
        self.test = int(config["test"])
        self.server = config["server"]
        self.loop_delay = int(config["loop_delay"])
        self.enable_long = config["enable_long"]
        self.enable_short = config["enable_short"]
        self.tp_price = config["tp_price"]
        self.sl_price = config["sl_price"]
        self.x_stop = config["x_stop"]
        self.max_dk = config["max_dk"]
        self.min_dk = config["min_dk"]
        self.use_rsi = config["use_rsi"]
        self.enable_testdata = config["enable_testdata"]

        self.simple_create = config["simple_create"]
        self.candle_from_db = config["candle_from_db"]
        self.metric_from_db = config["metric_from_db"]
        self.proxy = config["proxy"]

        self.last_tick = datetime.utcnow()
        self.charger = ChargeCreator(self.engine)
        self.orderer = OrderCreator(self.engine)

        self.mapper = BitmexMapper("JKIAo442CYwX5ak5KlYJGwPa",
                                   "83UWnblikD2j-2l5-KBtyvHa4P3ViuqAJdern4XMm2e0bpZr",
                                   base_url="https://www.bitmex.com/api/v1/", skipws=True, proxy=self.proxy)
        self.orderer = OrderCreator(self.engine, skipws=True)
        self.cc = CandleCreator(engine)

    def main_loop(self, candles):
        if self.use_rsi:
            candles = stochastic_rsi(candles)
        candles = add_pvo(candles)
        self.real_row = candles.iloc[-1]
        self.profit = 0
        self.decision_pvo()
        self.buy_or_sell()

    def print_processing(self):
        s = ""
        s += " | " + str(self.real_row["date"])
        s += " | " + str(self.real_row["close"])
        s += " |vol: %f" % (self.real_row["volume"] / 1000000)
        s += " |PVO: %s" % round(self.real_row["PVOH"], 2)
        if self.use_rsi:
            s += " |K: %s" % round(self.real_row["SLOWK"], 2)
            s += " |D: %s" % round(self.real_row["SLOWD"], 2)
            s += " |deltaK: %s" % round(self.real_row["SLOWK"] - self.real_row["SLOWD"], 2)
        s += " |pdd: %s" % self.price_delta_up
        s += " |pdd: %s" % self.price_delta_down
        s += " |tp: %s" % (self.tp_price != 0 and self.tp_price < self.price_delta_down)
        s += " |sl: %s" % (self.sl_price != 0 and self.sl_price < self.price_delta_up)
        s += " | " + self.status
        s += " (%s, b:%s, s:%s)" % (self.deposit, self.buys, self.sells)
        log.info(s)

    def print_event(self, msg):
        if self.enable_testdata:
            session = self.Session()
            td = testdata.Testdata()
            td.date = datetime.utcnow()
            data = ""
            data += "%s;" % self.real_row["date"]
            data += "%s;" % self.real_row["close"]
            data += "%s;" % round(self.real_row["volume"] / 1000000, 2)
            data += "%s;" % round(self.real_row["PVOH"], 2)
            data += "%s;" % round(self.real_row["SLOWK"] - self.real_row["SLOWD"], 2)
            data += "%s;" % ("buy" if self.buy else "sell")
            data += "%s;" % self.config
            td.data = data
            try:
                session.add(td)
                session.commit()
            except Exception as e:
                session.rollback()
                log.exception("Error save testdata")
            finally:
                session.close()

        log.warning(msg +
                    " date=" + str(self.real_row["date"]) +
                    " price=" + str(self.real_row["close"]) +
                    " | charge_id=" + str(self.charge_id)
                    )

    def decision_pvo(self):
        if self.order_price != 0:
            self.price_delta_up = 100 * ((self.order_price - self.real_row["close"]) / self.real_row["close"])
            self.price_delta_down = 100 * ((self.real_row["close"] - self.order_price) / self.real_row["close"])
        else:
            self.price_delta_up = 0
            self.price_delta_down = 0

        self.long()
        self.short()

    def long(self):
        if not self.enable_long:
            return
        tp = self.tp_price != 0 and self.tp_price < self.price_delta_down
        sl = self.sl_price != 0 and self.sl_price < self.price_delta_up
        sell_decision = (self.real_row["PVOH"] < self.pvo_min_buy or tp or sl)

        if (self.pvo_max_buy * self.x_stop) > self.real_row["PVOH"] > self.pvo_max_buy \
                and ((self.use_rsi and self.max_dk > self.real_row["SLOWK"] - self.real_row["SLOWD"] > self.min_dk)
                     or not self.use_rsi) \
                and (not ((not self.tp_sell and tp) or (not self.sl_sell and sl))) \
                and self.buys == 0:
            self.tp_sell = False
            self.sl_sell = False
            self.order_price = self.real_row["close"]
            self.buy = True

        if sell_decision and self.buys != 0:
            self.order_price = 0
            if tp:
                log.info("TAKE PROFIT!!!")
                self.tp_sell = True
            if sl:
                log.info("STOP LOSE!!!")
                self.sl_sell = True
            self.sell = True

    def short(self):
        if not self.enable_short:
            return

        tp = self.tp_price != 0 and self.tp_price < self.price_delta_up
        sl = self.sl_price != 0 and self.sl_price < self.price_delta_down
        buy_decision = (self.real_row["PVOH"] > self.pvo_max_sell or tp or sl)

        if (self.pvo_min_sell * self.x_stop) < self.real_row["PVOH"] < self.pvo_min_sell \
                and ((self.use_rsi and self.max_dk > self.real_row["SLOWK"] - self.real_row["SLOWD"] > self.min_dk)
                     or not self.use_rsi)\
                and (not ((not self.tp_sell and tp) or (not self.sl_sell and sl))) \
                and self.sells == 0:
            self.order_price = self.real_row["close"]
            self.tp_sell = False
            self.sl_sell = False
            self.sell = True

        if buy_decision and self.sells != 0:
            self.order_price = 0
            if tp:
                log.info("TAKE PROFIT!!!")
                self.tp_sell = True
            if sl:
                log.info("STOP LOSE!!!")
                self.sl_sell = True
            self.buy = True

