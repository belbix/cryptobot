import logging.config
import os
from datetime import datetime

from sqlalchemy.orm import sessionmaker

from entity import testdata
from mappers.bitfinexmapper import BitfinexMapper
from mappers.bitmexmapper import BitmexMapper
from mappers.ibmapper import IBMapper
from strategies.strat import Strat
from utils import configloader
from utils.candlecreator import CandleCreator
from utils.indicators import stochastic_rsi, add_pvo
from utils.ordercreator import OrderCreator

log = logging.getLogger(__name__)
logging.getLogger("requests").setLevel(logging.INFO)
logging.getLogger("utils.candlecreator").setLevel(logging.INFO)
logging.getLogger("bitmex.market_maker.bitmex").setLevel(logging.WARNING)


class SRsi(Strat):
    # constant
    FEE = (0.075 + 0.15) / 100
    # FEE = 0.002
    # FEE = 0
    STRATEGY_ID = 1
    span = 1

    # not loop variable
    real_delta_k = None
    skip = False
    delta_k = 0
    dif = 0
    K = 0
    D = 0
    old_k = 0
    old_d = 0
    old_delta_k = 0
    old_k2 = 0
    old_d2 = 0
    old_delta_k2 = 0
    order_price = 0
    candles_day = None
    row_day = 0
    day_delta_k = 0
    namek = "SLOWK"
    named = "SLOWD"
    price_delta = 0
    real_price_delta = 0
    dynamic_candle = None
    dynamic_delta_kd = 0

    # configurable variable
    kdeltax = None
    kmin = None
    row_number = None
    datedeltax = None
    tp_sell = False
    sl_sell = False

    def __init__(self, engine):
        self.engine = engine
        self.Session = sessionmaker(bind=self.engine)

        self.config = config = configloader.get_config(os.path.basename(__file__).split(".")[0])
        log.info(config)

        self.candle_from_db = config["candle_from_db"]
        self.symbol = config["symbol"]
        self.server = config["server"]
        self.period = config["period"]
        self.test = config["test"]
        self.loop_delay = int(config["loop_delay"])
        self.simple_create = config["simple_create"]
        self.download_candles = config["download_candles"]
        self.metric_from_db = config["metric_from_db"]

        self.real_kdeltax = int(config["real_kdeltax"])
        self.kdeltax = int(config["kdeltax"])
        self.kdeltax_close = config["kdeltax_close"]
        self.kmin = config["kmin"]
        self.kmax = config["kmax"]
        self.row_number = int(config["row_number"])
        self.datedeltax = int(config["datedeltax"])
        self.tp_price = config["tp_price"]
        self.sl_price = config["sl_price"]
        self.use_old_delta_k2 = config["use_old_delta_k2"]
        self.proxy = config["proxy"]
        self.enable_long = config["enable_long"]
        self.enable_short = config["enable_short"]
        self.hard_close = config["hard_close"]
        self.day_validate = config["day_validate"]
        self.kdeltax_day = config["kdeltax_day"]
        self.old_kdeltax = config["old_kdeltax"]
        self.old_k_row_number = config["old_k_row_number"]
        try:
            self.FEE = config["FEE"]
        except Exception as e:
            log.info("Fee not set in config, setup: %s" % self.FEE)
        self.max_dif = config["max_dif"]

        self.enable_dynamic_candle = False
        self.dynamic_kdeltax = 2
        self.dynamic_kdeltax_close = 100

        self.enable_pvo = config["pvo"]
        self.pvo_max_short_open = config["pvo_max_short_open"]
        self.pvo_max_short_close = config["pvo_max_short_close"]

        self.enable_testdata = False

        if self.enable_dynamic_candle:
            self.row_number += 1
        if self.server == "bitmex":
            self.mapper = BitmexMapper("JKIAo442CYwX5ak5KlYJGwPa",
                                       "83UWnblikD2j-2l5-KBtyvHa4P3ViuqAJdern4XMm2e0bpZr",
                                       base_url="https://www.bitmex.com/api/v1/", skipws=True, proxy=self.proxy)
        elif self.server == "bitfinex":
            self.mapper = BitfinexMapper("", "")
        elif self.server == "ib":
            self.mapper = IBMapper("", "")

        self.last_tick = datetime.utcnow()

        self.cc = CandleCreator(engine)
        self.orderer = OrderCreator(self.engine, skipws=True)

    def main_loop(self, candles):
        if self.enable_dynamic_candle:
            candles = stochastic_rsi(candles.append(candles.iloc[-1]))

        if self.enable_dynamic_candle or not self.metric_from_db:
            candles = stochastic_rsi(candles)
            self.namek = "SLOWK"
            self.named = "SLOWD"
        else:
            self.namek = "slowk"
            self.named = "slowd"

        if self.enable_pvo:
            candles = add_pvo(candles)

        self.profit = 0
        try:
            if not self.validate(candles):
                return
        except Exception as e:
            # log.info(candles)
            raise e
        self.decision()
        self.buy_or_sell()

    def print_event(self, msg):
        log.warning(msg +
                    " date=" + str(self.real_row["date"]) +
                    " price=" + str(self.real_row["close"]) +
                    " price_delta_up_max=" + str(self.price_delta_down_max) +
                    " price_delta_down_max=" + str(self.price_delta_up_max) +
                    " | charge_id=" + str(self.charge_id) +
                    " | old_k=" + str(round(self.old_k, 2)) +
                    " | old_d=" + str(round(self.old_d, 2)) +
                    " | old_delta_k2=" + str(round(self.old_delta_k2, 2)) +
                    " | old_delta_k=" + str(round(self.old_delta_k, 2))
                    )

        if self.enable_testdata:
            session = self.Session()
            try:
                td = testdata.Testdata()
                td.date = datetime.utcnow()
                data = ""
                data += "%s;" % self.real_row["date"]
                data += "%s;" % self.real_row["close"]
                data += "%s;" % round(self.real_row["volume"] / 1000000, 2)
                data += "%s;" % round(self.real_row[self.namek] - self.real_row[self.named], 2)
                # data += "%s;" % self.real_price_delta
                # data += "%s;" % self.price_delta
                if self.enable_dynamic_candle:
                    data += "%s;" % self.dynamic_delta_kd
                if self.enable_pvo:
                    data += "%s;" % round(self.real_row["PVOH"], 2)
                data += "%s;" % ("buy" if self.buy else "sell")
                data += "%s;" % self.config
                td.data = data
                session.add(td)
                session.commit()
            except Exception as e:
                log.exception("Error save testdata")
            finally:
                session.close()

    def print_processing(self):
        if self.real_row is None:
            return
        # log.info(self.real_row)
        s = ""
        s += "%s" % self.real_row["date"]
        s += " | %s" % self.real_row["close"]
        s += " | %s" % round(self.dif, 2)
        s += " |k: %s" % round(self.real_row[self.namek], 2)
        s += " |d: %s" % round(self.real_row[self.named], 2)
        if self.enable_dynamic_candle:
            s += " |ddk: %s" % self.dynamic_delta_kd
        s += " |dk: %s" % self.real_delta_k
        s += " |dk1: %s" % self.delta_k
        s += " |dk2: %s" % self.old_delta_k
        # s += " |rpd: %s" % self.real_price_delta
        # s += " |pd: %s" % self.price_delta
        # s += " |dpd: %s" % self.otskok
        if self.enable_pvo:
            s += " |pvo: %s" % round(self.real_row["PVOH"], 2)
        s += " |up_max: %s" % round(self.price_delta_up_max, 2)
        s += " |down_max: %s" % round(self.price_delta_down_max, 2)

        s += " | %s" % self.status + "(%s, %s, %s)" % (self.deposit, self.buys, self.sells)
        log.info(s)

    def validate(self, candles):
        if candles is None:
            return False
        if self.enable_dynamic_candle:
            self.dynamic_candle = candles.iloc[-1]
            self.dynamic_delta_kd = round(self.dynamic_candle[self.namek] - self.dynamic_candle[self.named], 2)

            self.real_row = candles.iloc[-2]
        else:
            self.real_row = candles.iloc[-1]
        self.row = candles.iloc[-self.row_number]
        datedelta = (datetime.utcnow() - self.row["date"]).total_seconds()
        if datedelta < self.datedeltax:
            self.status = "skip first *datedelta* seconds"
            return False
        self.dif = ((abs(self.real_row["close"] - self.row["low"])) / self.real_row["close"]) * 100
        self.K = self.row[self.namek]
        self.D = self.row[self.named]
        self.delta_k = round(self.K - self.D, 2)
        # if self.delta_k == 0: # DELTA K!!!!!!!!!!!!!!!!!!!
        #     return False

        self.real_delta_k = round(self.real_row[self.namek] - self.real_row[self.named], 2)

        self.old_k = candles.iloc[-(self.row_number + self.old_k_row_number)][self.namek]
        self.old_d = candles.iloc[-(self.row_number + self.old_k_row_number)][self.named]
        self.old_delta_k = round(self.old_k - self.old_d, 2)

        self.old_k2 = candles.iloc[-(self.row_number + self.old_k_row_number + 1)][self.namek]
        self.old_d2 = candles.iloc[-(self.row_number + self.old_k_row_number + 1)][self.named]
        self.old_delta_k2 = self.old_k2 - self.old_d2
        self.price_delta = round(((self.row["close"] - self.row["open"]) / self.row["open"]) * 100, 2)
        self.real_price_delta = round(((self.real_row["close"] - self.real_row["open"]) / self.real_row["open"]) * 100,
                                      2)
        self.otskok = abs(self.price_delta) - abs(self.real_price_delta)

        if self.day_validate and \
                (self.candles_day is None
                 or self.row_day["date"].day != self.real_row["date"].day):
            self.candles_day = self.mapper.get_candles("1d", self.symbol, hist=True, limit=70,
                                                       end=str(self.real_row["date"]))
            self.candles_day = stochastic_rsi(self.candles_day)
            # log.info(self.candles_day)
            self.row_day = self.candles_day.iloc[-1]
            self.day_delta_k = round(self.row_day[self.namek] - self.row_day[self.named], 2)

        return True

    def decision(self):

        if self.day_validate:
            if self.day_delta_k > self.kdeltax_day:
                self.enable_long = True
            else:
                self.enable_long = False

            if self.day_delta_k < self.kdeltax_day:
                self.enable_short = True
            else:
                self.enable_short = False
        if self.order_price != 0:
            self.price_delta_up = 100 * ((self.real_row["close"] - self.order_price) / self.real_row["close"])
            self.price_delta_down = 100 * ((self.order_price - self.real_row["close"]) / self.real_row["close"])
        else:
            self.price_delta_down = 0
            self.price_delta_up = 0

        self.long()
        self.short()
        if self.buy or self.sell:
            self.order_price = self.real_row["close"]

    def long(self):
        if not self.enable_long:
            return

        tp = self.tp_price != 0 and self.tp_price < self.price_delta_up
        sl = self.sl_price != 0 and self.sl_price < self.price_delta_down

        if self.enable_dynamic_candle:
            open_dynamic = self.dynamic_delta_kd > self.dynamic_kdeltax
        else:
            open_dynamic = False

        # if self.enable_pvo:
        #     pvo_open = self.real_row["PVOH"] < self.pvo_max_short_open
        #     pvo_close = self.real_row["PVOH"] > self.pvo_max_short_close
        # else:
        #     pvo_open = True
        #     pvo_close = False

        decision = self.delta_k > self.kdeltax \
                   and self.dif < self.max_dif \
                   and (self.real_delta_k > self.real_kdeltax or open_dynamic) \
                   and self.real_row[self.namek] <= self.kmin \
                   and (self.old_delta_k < self.old_kdeltax
                        or (self.use_old_delta_k2 and self.old_delta_k2 < self.old_kdeltax))

        close_decision = (self.hard_close and decision) or \
                         (not self.hard_close and (self.delta_k < -self.kdeltax_close
                                                   or self.real_delta_k < -self.kdeltax_close
                                                   or (self.enable_dynamic_candle
                                                       and self.dynamic_delta_kd < self.dynamic_kdeltax_close)
                                                   or tp or sl))

        if decision and self.buys == 0:  # -self.kdeltax:
            log.info("set new long "
                     "dif = %s "
                     "%s > %s "
                     "and %s > %s "
                     "and %s == 0 "
                     "and  %s <= %s "
                     "and  (%s < 0 or %s < 0)" % (self.dif, self.delta_k, self.kdeltax,
                                                  self.real_delta_k, -self.real_kdeltax,
                                                  self.buys,
                                                  self.K, self.kmin,
                                                  self.old_delta_k,
                                                  self.old_delta_k2))
            self.order_price = self.real_row["close"]
            self.tp_sell = False
            self.sl_sell = False
            self.buy = True
            return

        if close_decision and self.buys != 0:
            self.sell = True
            self.order_price = 0
            log.info("close longe "
                     "%s < %s or %s < %s or %s or %s" % (
                         self.delta_k, -self.kdeltax, self.real_delta_k, -self.real_kdeltax, tp, sl)
                     )
            if tp:
                log.info("TAKE PROFIT!!!")
                self.tp_sell = True
            if sl:
                log.info("STOP LOSE!!!")
                self.sl_sell = True

    def short(self):
        if not self.enable_short:
            return

        tp = self.tp_price != 0 and self.tp_price < self.price_delta_down
        sl = self.sl_price != 0 and self.sl_price < self.price_delta_up

        if self.enable_dynamic_candle:
            open_dynamic = self.dynamic_delta_kd < -self.dynamic_kdeltax
        else:
            open_dynamic = False

        if self.enable_pvo:
            pvo_open = self.real_row["PVOH"] < self.pvo_max_short_open
            pvo_close = self.real_row["PVOH"] > self.pvo_max_short_close
        else:
            pvo_open = True
            pvo_close = False

        decision = self.delta_k < -self.kdeltax \
                   and (self.real_delta_k < -self.real_kdeltax or open_dynamic) \
                   and self.real_row[self.namek] >= self.kmax \
                   and pvo_open \
                   and (not ((not self.tp_sell and tp) or (not self.sl_sell and sl))) \
                   and (self.old_delta_k > -self.old_kdeltax or
                        (self.use_old_delta_k2 and self.old_delta_k2 > -self.old_kdeltax))

        close_decision = (self.hard_close and decision) or \
                         (not self.hard_close
                          and (self.delta_k > self.kdeltax_close
                               or self.real_delta_k > self.kdeltax_close
                               or pvo_close
                               or (self.enable_dynamic_candle and self.dynamic_delta_kd > self.dynamic_kdeltax_close)
                               or tp or sl)
                          )

        if decision and self.sells == 0:
            log.info("set new short "
                     "%s < %s "
                     "and %s < %s "
                     "and %s == 0 "
                     "and  %s >= %s "
                     "and  %s > 0| %s > 0" % (self.delta_k, self.kdeltax,
                                              self.real_delta_k, -self.real_kdeltax,
                                              self.sells,
                                              self.K, self.kmax,
                                              self.old_delta_k,
                                              self.old_delta_k2))
            self.tp_sell = False
            self.sl_sell = False
            self.sell = True
            self.order_price = self.real_row["close"]
            return

        if close_decision and self.sells != 0:
            self.buy = True
            log.info("close short "
                     "%s > %s or %s > %s or %s or %s" % (
                         self.delta_k, self.kdeltax, self.real_delta_k, self.kdeltax, tp, sl)
                     )
            self.order_price = 0
            if tp:
                log.info("TAKE PROFIT!!!")
                self.tp_sell = True
            if sl:
                log.info("STOP LOSE!!!")
                self.sl_sell = True
