import argparse
import logging
import os
import time
from datetime import datetime, timedelta

from utils import configloader
from utils import emailsender

logging.config.dictConfig(configloader.get_config_log())
log = logging.getLogger(__name__)
parser = argparse.ArgumentParser(description='Check log last line.')
parser.add_argument('name', help='Name for identify')
args = parser.parse_args()
log.info("Start: %s" % args)

while True:
    f = open("logs/crypto.log")
    try:
        last_line = f.readlines()[-1]
        arr = last_line.split(" ")
        last_date = datetime.strptime(arr[0] + arr[1], "%Y-%m-%d%H:%M:%S,%f")
        # log.info("last date: %s" % (last_date - datetime.now()))
        if datetime.now() - last_date > timedelta(minutes=10):
            msg = "So old activity in %s | %s | %s | %s | %s" \
                  % (
                      os.path.dirname(os.path.realpath(__file__)),
                      arr[3],
                      last_date,
                      datetime.now(),
                      (datetime.now() - last_date)
                  )
            es = emailsender.EmailSender()
            log.info(msg)
            es.send_email(msg)
    except:
        log.exception("error activitychecker")

    f.close()
    time.sleep(60 * 20)
